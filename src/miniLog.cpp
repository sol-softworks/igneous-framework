// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "miniLog.h"

#include <map>
#include <chrono>
#include <vector>

#include <cstdio>

static std::map<LogType,std::string> typeString = {
	{LogType::DEBUG,"DEBUG"},
	{LogType::INFO,"INFO"},
	{LogType::WARN,"WARN"},
	{LogType::ERR,"ERROR"},
	{LogType::FATAL,"FATAL"}};

static std::chrono::time_point startTime = std::chrono::system_clock::now();

static const char* logPrepend = "[%.8f] [%s]: ";

static float timestamp(){
	std::chrono::duration<float> ts = std::chrono::system_clock::now() - startTime;
	return ts.count();
}

/*grumble grumble*/
void ConsoleLog(LogType type, std::string msg){
	auto fmt = FormatMsg(type,msg);
	printf("%s",fmt.c_str());
}

void FileLog(LogType type, std::string file, std::string msg){
	if(auto out = std::fopen(file.c_str(),"a")){
		auto fmt = FormatMsg(type,msg);
		fprintf(out, "%s", fmt.c_str());
	}
}

std::string FormatMsg(LogType type, std::string msg){
	float ts = timestamp();
	int sz = snprintf(nullptr,0,logPrepend,ts,typeString[type].c_str());
	std::vector<char> buf(sz+1);
	snprintf(&buf[0],buf.size(),logPrepend,ts,typeString[type].c_str());

	//Ignore null-terminator produced by snprintf.
	std::string formatted = std::string(buf.begin(), --buf.end()) + msg;
	return formatted;
}
