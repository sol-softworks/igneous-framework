// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "Pipelines/pipeline_configuration.h"

#include "pipeline.h"
#include "surface.h"

#include "Conduit/shader.h"
#include "Obsidian/bindings.h"

#include "miniLog.h"

namespace Igneous{

  void DynamicStateConfiguration::AddDynamicState(vk::DynamicState state){
    this->dynamicStates.push_back(state);
  }

  vk::PipelineDynamicStateCreateInfo DynamicStateConfiguration::CreateInfo(){
    return vk::PipelineDynamicStateCreateInfo{
      {},
      static_cast<uint32_t>(this->dynamicStates.size()),this->dynamicStates.data()
    };
  }

  void ColorBlendState::AddAttachment(vk::Bool32 enabled,
    vk::BlendFactor sourceColorBlend,
    vk::BlendFactor destColorBlend,
    vk::BlendOp colorBlendOp,
    vk::BlendFactor sourceAlphaBlend,
    vk::BlendFactor destAlphaBlend,
    vk::BlendOp alphaBlendOp,
    vk::ColorComponentFlags writeMask
  ){
    this->colorAttaches.emplace_back(enabled,
      sourceColorBlend,destColorBlend,colorBlendOp,
      sourceAlphaBlend,destAlphaBlend,alphaBlendOp,
      writeMask
    );
  }

  vk::PipelineColorBlendStateCreateInfo ColorBlendState::CreateInfo(){
    return vk::PipelineColorBlendStateCreateInfo{
      {},
      this->logicEnable, this->logicOp,
      static_cast<uint32_t>(this->colorAttaches.size()),this->colorAttaches.data()
    };
  }

  PrimitiveConfiguration::~PrimitiveConfiguration(){}

  vk::PipelineInputAssemblyStateCreateInfo PrimitiveConfiguration::CreateInfo(){
    return vk::PipelineInputAssemblyStateCreateInfo{
      {},
      this->topology,
      this->primitiveRestartEnable
    };
  }

  Point::Point(){
      this->topology = vk::PrimitiveTopology::ePointList;
      //Primitive Restart not supported with List-type topologies
      this->primitiveRestartEnable = false;
    }

  Line::Line(Line::Ordering layout) : _layout(layout){
    this->withAdjacency = false;
    this->primitiveRestartEnable = true;
  }

  vk::PipelineInputAssemblyStateCreateInfo Line::CreateInfo(){
    switch(this->_layout){
      case Line::Ordering::List:
        if(this->withAdjacency) this->topology = vk::PrimitiveTopology::eLineListWithAdjacency;
        else this->topology = vk::PrimitiveTopology::eLineList;
        //Primitive Restart not supported with List-type topologies
        this->primitiveRestartEnable = false;
        break;
      case Line::Ordering::Strip:
        if(this->withAdjacency) this->topology = vk::PrimitiveTopology::eLineStripWithAdjacency;
        else this->topology = vk::PrimitiveTopology::eLineStrip;
        break;
    }
    return PrimitiveConfiguration::CreateInfo();
  }

  Triangle::Triangle(Triangle::Ordering layout) : _layout(layout){
    this->withAdjacency = false;
    this->primitiveRestartEnable = true;
  }

  vk::PipelineInputAssemblyStateCreateInfo Triangle::CreateInfo(){
    switch(this->_layout){
      case Triangle::Ordering::List:
        if(this->withAdjacency) this->topology = vk::PrimitiveTopology::eTriangleListWithAdjacency;
        else this->topology = vk::PrimitiveTopology::eTriangleList;
        //Primitive Restart not supported with List-type topologies
        this->primitiveRestartEnable = false;
        break;
      case Triangle::Ordering::Strip:
        if(this->withAdjacency) this->topology = vk::PrimitiveTopology::eTriangleStripWithAdjacency;
        else this->topology = vk::PrimitiveTopology::eTriangleStrip;
        break;
      case Triangle::Ordering::Fan:
        this->topology = vk::PrimitiveTopology::eTriangleFan;
        break;
    }
    return PrimitiveConfiguration::CreateInfo();
  }

  Patch::Patch(){
    this->topology = vk::PrimitiveTopology::ePatchList;
    //Primitive Restart not supported with List-type topologies
    this->primitiveRestartEnable = false;
  }

  vk::PipelineTessellationStateCreateInfo TessellationConfiguration::CreateInfo(){
    return vk::PipelineTessellationStateCreateInfo{
      {},
      this->patchControlPoints
    };
  }

  vk::PipelineRasterizationStateCreateInfo RasterizationConfiguration::CreateRasterInfo(){
    if(this->cullFrontFace) this->cullFlags |= vk::CullModeFlagBits::eFront;
    if(this->cullBackFace) this->cullFlags |= vk::CullModeFlagBits::eBack;

    if(this->frontFaceIsCCW) this->frontFaceWinding = vk::FrontFace::eCounterClockwise;
    else this->frontFaceWinding = vk::FrontFace::eClockwise;
    return vk::PipelineRasterizationStateCreateInfo{
      {},
      this->enableDepthClamp,
      this->enableRasterizerDiscard,
      this->mode,
      this->cullFlags,
      this->frontFaceWinding,
      this->enableDepthBias,
      this->depthBiasConstantFactor,
      this->depthBiasClamp,
      this->depthBiasSlopeFactor,
      this->lineWidth
    };
  }

  vk::PipelineMultisampleStateCreateInfo RasterizationConfiguration::CreateMSInfo(){
    return vk::PipelineMultisampleStateCreateInfo{
      {},
      this->sampleCount,
      this->enableSampleShading,
      this->minSampleShading,
      this->pMask,
      this->enableAlphaToCoverage,
      this->enableAlphaToOne
    };
  }

  void PipelineConfiguration::_CompileShaders(vk::Device const &dev){
    for(auto &elem : this->stages){
      this->shaders.push_back(sharedShaders.at(elem.first).Build(dev));
      this->shaderStages.emplace_back(
        vk::PipelineShaderStageCreateFlags(),
        sharedShaders.at(elem.first).entry_points.at(elem.second),
        this->shaders.back(),
        elem.second.c_str(),
        nullptr);
    }
  }

  void PipelineConfiguration::_CleanupShaders(vk::Device const &dev){
    for(auto &shader : this->shaders){
      Shader::Destroy(dev,shader);
    }
  }

  void PipelineConfiguration::_ConfigureLayout(
    vk::Device const &dev,
    vk::PipelineLayout &layout
  ){
    std::vector<vk::DescriptorSetLayout> dslList = {};
		for(auto &desc : this->layout_metas){
			dslList.push_back(desc);
		}
		//Construct pc_block temporary for layout creation
		std::vector<vk::PushConstantRange> pcs;
    for(auto &blockDef : this->pc_block_defs){
      //pcs.push_back(std::move(blockDef.second.Define()));
      pcs.push_back(blockDef.second.Define());
    }

		vk::PipelineLayoutCreateInfo lInfo = {
			{},
			static_cast<uint32_t>(dslList.size()), dslList.data(),
			static_cast<uint32_t>(pcs.size()), pcs.data(),
		};

		if(layout) dev.destroyPipelineLayout(layout,Igneous::allocators);
		ConsoleLog(LogType::INFO,"[VK] Creating Pipeline Layout\n");
		layout = dev.createPipelineLayout(lInfo,Igneous::allocators);
  }

  void PipelineConfiguration::_ConfigurePushConstants(PushConstantLayout &layout){
    if(this->pc_block_defs.empty()){
      for(auto &elem : this->stages){
        sharedShaders.at(elem.first).AddPushConstants(this->pc_block_defs);
      }
    }
    //I know this looks terrible. And it is.
    for(auto &block : this->pc_block_defs){
      switch(block.second.type.basetype){
        case spirv_cross::SPIRType::Int:
          if(block.second.count == 1) layout.AddRange<int32_t>(block.first,block.second.stages,block.second.offset);
          else layout.AddArrayRange<int32_t>(block.first,block.second.stages,block.second.offset,block.second.count);
          break;
        case spirv_cross::SPIRType::UInt:
          if(block.second.count == 1) layout.AddRange<uint32_t>(block.first,block.second.stages,block.second.offset);
          else layout.AddArrayRange<uint32_t>(block.first,block.second.stages,block.second.offset,block.second.count);
          break;
        case spirv_cross::SPIRType::Int64:
          if(block.second.count == 1) layout.AddRange<int64_t>(block.first,block.second.stages,block.second.offset);
          else layout.AddArrayRange<int64_t>(block.first,block.second.stages,block.second.offset,block.second.count);
          break;
        case spirv_cross::SPIRType::UInt64:
          if(block.second.count == 1) layout.AddRange<uint64_t>(block.first,block.second.stages,block.second.offset);
          else layout.AddArrayRange<uint64_t>(block.first,block.second.stages,block.second.offset,block.second.count);
          break;
        case spirv_cross::SPIRType::Float:
          if(block.second.count == 1) layout.AddRange<float>(block.first,block.second.stages,block.second.offset);
          else layout.AddArrayRange<float>(block.first,block.second.stages,block.second.offset,block.second.count);
          break;
        case spirv_cross::SPIRType::Double:
          if(block.second.count == 1) layout.AddRange<double>(block.first,block.second.stages,block.second.offset);
          else layout.AddArrayRange<double>(block.first,block.second.stages,block.second.offset,block.second.count);
          break;
        default:
          ConsoleLog(LogType::ERR,"[IG:PIPE] - Unimplemented Push Constant block type encountered!");
          break;
      }
    }
  }

  void PipelineConfiguration::_ConfigureBindings(
    Device &dev
  ){
    if(this->shaderBindings.size() == 0){
      for(auto &stage : this->stages){
        sharedShaders.at(stage.first).AddBindingDefs(this->shaderBindings);
      }
    }
    ConsoleLog(LogType::DEBUG,"[VK] Shader Bindings: %u\n",this->shaderBindings.size());
    for(auto layoutCount = (--this->shaderBindings.end())->layoutIndex+1;layoutCount>0;--layoutCount){
      this->layout_metas.emplace_back(dev);
    }
    for(auto &bind : this->shaderBindings){
      auto &meta = this->layout_metas.at(bind.layoutIndex);
      ///@todo DefineBinding no longer needs to be a template, as the "convenience"
      ///     factor has become a hindrance.
      if(bind.type == CombinedSampleImageBinding::Type){
        meta.DefineBinding<Igneous::CombinedSampleImageBinding>(
  				std::make_pair(bind.bindingIndex,bind.name),
  				bind.stages,
  				bind.dimensionality
  			);
      }
      else if(bind.type == SampledImageBinding::Type){
        meta.DefineBinding<Igneous::SampledImageBinding>(
  				std::make_pair(bind.bindingIndex,bind.name),
  				bind.stages,
  				bind.dimensionality
  			);
      }
      else if(bind.type == SamplerBinding::Type){
        meta.DefineBinding<Igneous::SamplerBinding>(
  				std::make_pair(bind.bindingIndex,bind.name),
  				bind.stages,
  				bind.dimensionality
  			);
      }
      else if(bind.type == BufferBinding::Type){
        meta.DefineBinding<Igneous::BufferBinding>(
  				std::make_pair(bind.bindingIndex,bind.name),
  				bind.stages,
  				bind.dimensionality
  			);
      }
    }
    for(auto &meta : this->layout_metas){
      meta.CompileLayout();
    }
  }

  void PipelineConfiguration::_AssignLayouts(
    std::vector<DescriptorLayoutMeta> &layout_meta
  ){
    if(layout_meta.size() > 0) return;
    ConsoleLog(LogType::DEBUG,"[VK] Adding %u layouts to pipeline\n",this->layout_metas.size());
    for(auto &layout : this->layout_metas){
      layout_meta.push_back(layout);
    }
  }

  vk::Pipeline ComputePipelineConfiguration::CreatePipeline(
    Device &dev,
    vk::PipelineLayout &layout,
    PushConstantLayout &pc_block
  ){
    this->_CompileShaders(dev);

    this->_ConfigurePushConstants(pc_block);
    this->_ConfigureLayout(dev,layout);

    auto pipeConf = vk::ComputePipelineCreateInfo{
      {},
      this->shaderStages.at(0),
      layout,
      nullptr,0
    };

    auto pipeline = static_cast<vk::Device>(dev).createComputePipeline(
      GetPipelineCache(dev),
      pipeConf,
      Igneous::allocators
    );

    this->_CleanupShaders(dev);

    return pipeline;
  }

  void GraphicsPipelineConfiguration::SetViewportConfiguration(
    uint32_t viewportCount,
    uint32_t scissorCount
  ){
    this->viewports = viewportCount;
    this->scissors = scissorCount;
  }

  vk::Pipeline GraphicsPipelineConfiguration::CreatePipeline(
    Device &dev,
    vk::PipelineLayout &layout,
    PushConstantLayout &pc_block
  ){
    this->_CompileShaders(dev);

    this->_ConfigurePushConstants(pc_block);
    this->_ConfigureBindings(dev);

    this->_ConfigureLayout(dev,layout);

    this->vertex = this->vertexInput.CreateInfo();
    this->vp = vk::PipelineViewportStateCreateInfo{
      {},
      this->viewports,nullptr,
      this->scissors,nullptr
    };
    this->color = this->colorBlend.CreateInfo();
    this->ds = this->dynamicStates.CreateInfo();
    this->inputAssembly = this->primitiveConfig->CreateInfo();
    this->tess = this->tesselation.CreateInfo();
    this->rasterConfig = this->rasterization.CreateRasterInfo();
    this->multisampling = this->rasterization.CreateMSInfo();
    auto pipeConf = vk::GraphicsPipelineCreateInfo{
      {},
      static_cast<uint32_t>(this->shaderStages.size()),this->shaderStages.data(),
      &this->vertex,
      &this->inputAssembly,
      &this->tess,
      &this->vp,
      &this->rasterConfig,
      &this->multisampling,
      &this->depthStencil,
      &this->color,
      &this->ds,
      layout,
      *this->rpm,0,
      nullptr,0
    };

    auto pipeline = static_cast<vk::Device>(dev).createGraphicsPipeline(
			GetPipelineCache(dev),
			pipeConf,
			Igneous::allocators
		);

    this->_CleanupShaders(dev);

    return pipeline;
  }

  vk::IndexType GraphicsPipelineConfiguration::GetIndexType(){
    if(this->index16) return vk::IndexType::eUint16;
    else return vk::IndexType::eUint32;
  }

  vk::PipelineVertexInputStateCreateInfo VertexInputState::CreateInfo(){
    return vk::PipelineVertexInputStateCreateInfo{
      {},
      static_cast<uint32_t>(this->bindings.size()),this->bindings.data(),
      static_cast<uint32_t>(this->attributes.size()),this->attributes.data(),
    };
  }
};
