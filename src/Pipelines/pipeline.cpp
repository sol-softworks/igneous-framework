// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "Pipelines/pipeline.h"

#include "surface.h"
#include "texture.h"

#include "miniLog.h"

namespace Igneous{

	//*grumble grumble*
	//Defined in resource translation unit
	extern vk::AllocationCallbacks* allocators;

	static vk::ImageSubresourceRange imageRange = {vk::ImageAspectFlagBits::eColor,0,1,0,1};

	//Pipeline Lifecycle Management
	Pipeline::Pipeline(
		Device &dev,
		vk::PipelineBindPoint bindPoint
	) :
		pipelineType(bindPoint),
		_d(dev),
		pc_block(this->layout),
		_subID(0)
	{
		ConsoleLog(LogType::INFO,"[VK] Generating Graphics Pipeline\n");
	}

	Pipeline::~Pipeline(){
		///@todo Save pipeline cache state on pipeline destruction, to expedite
		///				construction on future pipeline creation (including persistent
		///				storage)
		static_cast<vk::Device>(this->_d).destroyPipelineCache(this->cache,Igneous::allocators);
		static_cast<vk::Device>(this->_d).destroyPipelineLayout(this->layout,Igneous::allocators);
		static_cast<vk::Device>(this->_d).destroyPipeline(this->pipeline,Igneous::allocators);
	}

	//Pipeline Construction / Configuration
	void Pipeline::CompilePipeline(
		PipelineConfiguration &config
	){

		ConsoleLog(LogType::INFO,"[VK] Compiling Graphics Pipeline\n");

		//Create Pipeline from configuration struct
		this->pipeline = config.CreatePipeline(
			this->_d,
			this->layout,
			this->pc_block
		);

		config._AssignLayouts(this->layouts);
	}

	void Pipeline::PreConfigurePipeline(){
		ConsoleLog(LogType::INFO,"[IG:PIPE] Default PreConfigure\n");
	}

	std::shared_ptr<Material> Pipeline::CreateMaterial(uint32_t baseLayout){
		return std::make_shared<Material>(this->layouts[baseLayout]);
	}

	void Pipeline::FrameStart(){
		this->executionTask = this->_d.StartTask();
		if(auto task = this->executionTask.lock()){
			auto buffer = static_cast<vk::CommandBuffer>(*task);

			this->pc_block.SetCommandBuffer(buffer);

			//Bind pipeline / desc sets
			buffer.bindPipeline(
				this->pipelineType,
				this->pipeline);
		}
	}

	void Pipeline::FrameEnd(){
		if(auto t = this->executionTask.lock()){
			this->pc_block.ClearCommandBuffer();
			t->Submit();
		}
	}

	void Pipeline::SetMaterials(
		std::vector<std::weak_ptr<Material>> const &materials
	){
		if(auto t = this->executionTask.lock()){
			auto buffer = static_cast<vk::CommandBuffer>(*t);
			auto ds = std::vector<vk::DescriptorSet>();
			for(auto &mat : materials){
				ds.push_back(mat.lock()->PrepareDescriptorSet());
			}
			///@todo Render all meshes that use the same set of materials in a single
			///				bind.
			buffer.bindDescriptorSets(
				this->pipelineType,
				this->layout,
				0,
				ds,
				nullptr			//FIXME: Dynamic Offsets array needs to actually exist
				);
		}
	}
}
