// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include "Pipelines/computePipeline.h"

namespace Igneous{

  ComputePipeline::ComputePipeline(
    Device &dev
  ) :
    Pipeline(dev,vk::PipelineBindPoint::eCompute)
  {
    ConsoleLog(LogType::INFO,"[VK] Generating Compute Pipeline\n");
  }

  void ComputePipeline::FrameStart(){
    Pipeline::FrameStart();
  }

  void ComputePipeline::FrameEnd(){
    Pipeline::FrameEnd();
  }

  void ComputePipeline::Dispatch(
    uint32_t groupX,
    uint32_t groupY,
    uint32_t groupZ
  ){
    if(auto task = this->executionTask.lock()){
      auto buffer = static_cast<vk::CommandBuffer>(*task);
      buffer.dispatch(groupX,groupY,groupZ);
    }
  }
}
