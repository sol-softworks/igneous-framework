// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include "Pipelines/graphicsPipeline.h"

namespace Igneous{

  GraphicsPipeline::GraphicsPipeline(
    Device &dev
  ) :
    Pipeline(dev,vk::PipelineBindPoint::eGraphics)
  {
    ConsoleLog(LogType::INFO,"[VK] Generating Graphics Pipeline\n");
  }

  void GraphicsPipeline::FrameStart(
    RenderBuffer &framebuffer
  ){
    if(!framebuffer){
      ConsoleLog(LogType::INFO,"[IG:PIPE] - Invalid Framebuffer!\n");
      return;
    }
    Pipeline::FrameStart();
    if(auto task = this->executionTask.lock()){
			auto buffer = static_cast<vk::CommandBuffer>(*task);
      auto rp = framebuffer.StartPass();
      buffer.beginRenderPass(rp,vk::SubpassContents::eInline);
    }
  }

  void GraphicsPipeline::FrameEnd(){
    if(auto t = this->executionTask.lock()){
      auto buffer = static_cast<vk::CommandBuffer>(*t);
      buffer.endRenderPass();
    }

    Pipeline::FrameEnd();
  }

  void GraphicsPipeline::ConfigureViewport(
    ViewportConfiguration &config
  ){
    if(auto task = this->executionTask.lock()){
			auto buffer = static_cast<vk::CommandBuffer>(*task);
      config.SetScissors(buffer);
      config.SetViewports(buffer);
    }
  }

  void GraphicsPipeline::RecordDraw(BaseDrawCommand &cmd){
    if(auto t = this->executionTask.lock()){
      auto buffer = static_cast<vk::CommandBuffer>(*t);
      cmd.Draw(buffer);
    }
  }

  void GraphicsPipeline::AssignBuffers(){}

  void GraphicsPipeline::AssignBuffers(
    VulkanBuffer &VBO,
    VulkanBuffer &IBO,
    GraphicsPipelineConfiguration *config
  ){
    if(auto t = this->executionTask.lock()){
      auto buffer = static_cast<vk::CommandBuffer>(*t);

      vk::DeviceSize vOff = {0};

      buffer.bindVertexBuffers(
        0,
        1,VBO,&vOff);
      buffer.bindIndexBuffer(
        IBO,
        0,config->GetIndexType());
    }
  }
}
