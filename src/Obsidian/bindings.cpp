// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include "Conduit/dsmeta.h"

namespace Igneous{

  BaseBinding::BaseBinding(
    vk::DescriptorSetLayoutBinding &_binding
  ) : binding(_binding){}

  BaseBinding::~BaseBinding(){}

  vk::WriteDescriptorSet BaseBinding::CreateWriteInfo(){
    vk::WriteDescriptorSet wdInfo = {
      //Handled by external DS update
      //this->ds,
      nullptr,
      this->binding.binding,
      0,
      //Count populated by derived binding
      0,
      this->binding.descriptorType,
      //Handled by derived bindings
      //diInfo,dbInfo,view
      nullptr,nullptr,nullptr
    };

    return wdInfo;
  }

  BaseBinding::operator vk::DescriptorSetLayoutBinding() const{
    return this->binding;
  }

  BaseBinding::operator vk::DescriptorType() const{
    return this->binding.descriptorType;
  }


  ImageBindingData::ImageBindingData(
    size_t _size,
    BindUpdates &_updater,
    size_t bindIndex
  ) :
    _updates(_updater),
    _bindIndex(bindIndex),
    size(_size)
  {
    this->views.resize(this->size);
    this->samplers.resize(this->size);
  }

  BufferBindingData::BufferBindingData(
      size_t _size,
      BindUpdates &_updater,
      size_t bindIndex
    ) :
      _updates(_updater),
      _bindIndex(bindIndex),
      size(_size)
    {
    this->buffers.resize(this->size);
  }

  const vk::DescriptorType CombinedSampleImageBinding::Type;
  const vk::DescriptorType SampledImageBinding::Type;
  const vk::DescriptorType SamplerBinding::Type;
  const vk::DescriptorType BufferBinding::Type;
}
