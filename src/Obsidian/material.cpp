// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "Obsidian/material.h"

#include "miniLog.h"

namespace Igneous{

	Material::Material(DescriptorLayoutMeta &setLayout) : dsm(setLayout.CreateSet()){
		//Construct properties based on DSM bindings
		auto dsm_lock = dsm.lock();
		for(auto &bind : dsm_lock->bindings){
			std::unique_ptr<MaterialProperty> prop;
			//Determine Property class from descriptor type
			//		Will eventually include Property types for all descriptor types.
			switch(static_cast<vk::DescriptorType>(*bind.second)){
				//All three cases use a TextureProperty, so fall-through is necessary
				case vk::DescriptorType::eSampler:
					[[fallthrough]];
				case vk::DescriptorType::eCombinedImageSampler:
					[[fallthrough]];
				case vk::DescriptorType::eSampledImage:
					//prop = std::make_unique<TextureProperty>(bind.first,bind.second);
					prop = std::make_unique<TextureProperty>(bind.first,bind.second);
					break;
				case vk::DescriptorType::eUniformBuffer:
					///@todo  Dynamic buffer resizing is possible in Igneous, but shouldn't
					///				be mandatory for statically-sized buffers.
					//prop = std::make_unique<BufferProperty>(bind.first,this->dsm.lock());
					prop = std::make_unique<BufferProperty>(bind.first,bind.second);
					break;
				//Any types not defined currently use BaseDescriptorSetBinding
				default:
					prop = std::make_unique<MaterialProperty>(bind.first,bind.second);
					break;
			}
			this->properties.try_emplace(bind.first,std::move(prop));
		}
	}

	Material::~Material(){
		this->properties.clear();
	}

	vk::DescriptorSet Material::PrepareDescriptorSet(){
		if(auto ds = this->dsm.lock()){
			ds->Update();
		}
		return *(this->dsm.lock());
	}

	TextureProperty* Material::GetTexture(std::string id){
		return this->GetProperty<TextureProperty>(id);
	}

	BufferProperty* Material::GetBuffer(std::string id){
		return this->GetProperty<BufferProperty>(id);
	}
}
