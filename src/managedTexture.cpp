// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "managedTexture.h"

namespace Igneous{

  ManagedTexture::ManagedTexture(
    Device &dev,
    vk::ImageViewCreateInfo &ivci
  ) : BaseTexture(dev,ivci),
    _imgMem(nullptr)
  {}

  ManagedTexture::~ManagedTexture(){
    static_cast<vk::Device>(this->dev).destroyImage(this->_img,Igneous::allocators);
    this->dev.free(this->_imgMem);
  }

  void ManagedTexture::_rebuild(vk::ImageCreateInfo &ici){
    if(this->_img){
      //Free existing entities
      static_cast<vk::Device>(this->dev).destroyImage(this->_img,Igneous::allocators);
      this->dev.free(this->_imgMem);
    }

    this->_img = static_cast<vk::Device>(this->dev).createImage(ici,Igneous::allocators);
    this->_imgMem = this->dev.ialloc(this->_img,{vk::MemoryPropertyFlagBits::eDeviceLocal});
    this->_rebuildViews();
  }
}
