// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include "samplerMeta.h"

namespace Igneous{

  //*grumble grumble*
	//Defined in resource translation unit
	extern vk::AllocationCallbacks* allocators;

  SamplerMeta::SamplerMeta(TilingConfig tilecfg, AnisotropyConfig anisocfg, TexelFilter texcfg, LODConfig lodcfg){
    this->_sci = {
      {},
      //Texel Filter Configuration
      texcfg.magnify, texcfg.minify,
      vk::SamplerMipmapMode::eLinear,		//eNearest, eLinear (Configurable?)
      //Addressing Configuration
      tilecfg.u, tilecfg.v, tilecfg.w,
      0.0f,
      //Anisotropic Filtering Configuration
      anisocfg.enable,anisocfg.max,
      0,vk::CompareOp::eAlways,
      //LoD Configuration
      lodcfg.min,lodcfg.max,
      //Texture Border Configuration
      vk::BorderColor::eFloatTransparentBlack,
      //Unnormalized Coordinates (0: Normalized)
      0
    };
		this->_dirty.reset();
  }

  SamplerMeta::~SamplerMeta(){}

	void SamplerMeta::_rebuild(){
		if(this->_dirty){
			this->_sci.magFilter = this->filters.magnify;
			this->_sci.minFilter = this->filters.minify;
			this->_sci.addressModeU = this->tiling.u;
			this->_sci.addressModeV = this->tiling.v;
			this->_sci.addressModeW = this->tiling.w;
			this->_sci.anisotropyEnable = this->anisotropy.enable;
			this->_sci.maxAnisotropy = this->anisotropy.max;
			this->_sci.minLod = this->lod.min;
			this->_sci.maxLod = this->lod.max;

			for(auto &inst : this->_instances){
				static_cast<vk::Device>(inst.first.get()).destroySampler(*inst.second,Igneous::allocators);
				*inst.second = static_cast<vk::Device>(inst.first.get()).createSampler(this->_sci,Igneous::allocators);
			}

			this->_dirty.reset();
		}
	}

  std::shared_ptr<vk::Sampler> SamplerMeta::GetSampler(Device& owner){
		this->_rebuild();
		//Why create a new shared_ptr, if one already exists?
		if(!this->_instances.contains(owner)){
			auto samp = std::shared_ptr<vk::Sampler>(
				new vk::Sampler(
					static_cast<vk::Device>(owner).createSampler(this->_sci,Igneous::allocators)
				),
				owner._manager
			);
			this->_instances.try_emplace(owner,std::move(samp));
		}
		//Either way, we know there *should* be an instance for this device.
		return this->_instances.at(owner);
	}

  SamplerMeta::TilingConfig SamplerMeta::TilingConfig::TiledUV(){
    return TilingConfig{vk::SamplerAddressMode::eRepeat,vk::SamplerAddressMode::eRepeat,vk::SamplerAddressMode::eClampToBorder};
  }

  SamplerMeta::TilingConfig SamplerMeta::TilingConfig::TiledUVW(){
    return TilingConfig{vk::SamplerAddressMode::eRepeat,vk::SamplerAddressMode::eRepeat,vk::SamplerAddressMode::eRepeat};
  }

  SamplerMeta::TilingConfig SamplerMeta::TilingConfig::MirroredUV(){
    return TilingConfig{vk::SamplerAddressMode::eMirroredRepeat,vk::SamplerAddressMode::eMirroredRepeat,vk::SamplerAddressMode::eClampToBorder};
  }

  SamplerMeta::TilingConfig SamplerMeta::TilingConfig::MirroredUVW(){
    return TilingConfig{vk::SamplerAddressMode::eMirroredRepeat,vk::SamplerAddressMode::eMirroredRepeat,vk::SamplerAddressMode::eMirroredRepeat};
  }

  SamplerMeta::TilingConfig SamplerMeta::TilingConfig::ClampedUV(){
    return TilingConfig{vk::SamplerAddressMode::eClampToBorder,vk::SamplerAddressMode::eClampToBorder,vk::SamplerAddressMode::eClampToBorder};
  }

  SamplerMeta::TilingConfig SamplerMeta::TilingConfig::ClampedUVW(){
    return TilingConfig{vk::SamplerAddressMode::eClampToBorder,vk::SamplerAddressMode::eClampToBorder,vk::SamplerAddressMode::eClampToBorder};
  }

  SamplerMeta::TexelFilter SamplerMeta::TexelFilter::Nearest(){
    return TexelFilter{vk::Filter::eNearest,vk::Filter::eNearest};
  }

  SamplerMeta::TexelFilter SamplerMeta::TexelFilter::Linear(){
    return TexelFilter{vk::Filter::eLinear,vk::Filter::eLinear};
  }

  SamplerMeta::TexelFilter SamplerMeta::TexelFilter::CubicIMG(){
    return TexelFilter{vk::Filter::eCubicEXT,vk::Filter::eCubicEXT};
  }
}
