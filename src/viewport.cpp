// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include "viewport.h"

namespace Igneous{
	//Viewport Configuration
	void ViewportConfiguration::AddViewport(vk::Extent2D position, vk::Extent2D dimensions, vk::Extent2D depth){
		this->viewports.emplace_back(
			position.width,position.height,
			dimensions.width,dimensions.height,
			depth.width,depth.height
		);
	}

	void ViewportConfiguration::AddScissor(vk::Offset2D offset, vk::Extent2D extents){
		this->scissors.emplace_back(offset,extents);
	}

	void ViewportConfiguration::RecreateViewport(size_t vpIndex, vk::Extent2D pos, vk::Extent2D dims, vk::Extent2D depth){
		this->viewports[vpIndex].setX(pos.width).setY(pos.height)
			.setWidth(dims.width).setHeight(dims.height)
			.setMinDepth(depth.width).setMaxDepth(depth.height);
	}

	void ViewportConfiguration::SetViewports(vk::CommandBuffer &buffer){
		buffer.setViewport(0,static_cast<uint32_t>(this->viewports.size()),this->viewports.data());
	}

	void ViewportConfiguration::RecreateScissor(size_t scIndex, vk::Offset2D offset, vk::Extent2D extents){
		this->scissors[scIndex].offset = offset;
		this->scissors[scIndex].extent = extents;
	}

	void ViewportConfiguration::SetScissors(vk::CommandBuffer &buffer){
		buffer.setScissor(0,static_cast<uint32_t>(this->scissors.size()),this->scissors.data());
	}

	vk::PipelineViewportStateCreateInfo ViewportConfiguration::CreateInfo(){
		return vk::PipelineViewportStateCreateInfo{
			{},
			static_cast<uint32_t>(this->viewports.size()),this->viewports.data(),
			static_cast<uint32_t>(this->scissors.size()),this->scissors.data()
		};
	}
}
