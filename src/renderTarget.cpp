// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "renderTarget.h"

#include "managedTexture.h"
#include "externalTexture.h"

namespace Igneous{

  //*grumble grumble*
  //Defined in resource translation unit
  extern vk::AllocationCallbacks* allocators;

  //Revisit this. Probably will need to be more modular.
  static vk::ImageSubresourceRange imageRange = {vk::ImageAspectFlagBits::eColor,0,1,0,1};

  RenderTarget::RenderTarget(
    Device &_d,
    vk::Format format,
    vk::Extent2D _exts
  ) : dev(_d),
    extents(_exts)
  {
    this->_ivci = vk::ImageViewCreateInfo{
      {},
      nullptr,
      vk::ImageViewType::e2D,
      format,
      {
        vk::ComponentSwizzle::eR,
        vk::ComponentSwizzle::eG,
        vk::ComponentSwizzle::eB,
        vk::ComponentSwizzle::eA,
      },
      imageRange
    };
    this->targetTexture = std::make_shared<ManagedTexture>(
      dev,
      this->_ivci
    );
  }

  RenderTarget::RenderTarget(
    Device &_d,
    vk::Format format,
    vk::Extent2D _exts,
    vk::Image &rawImage
  ) : dev(_d),
    extents(_exts)
  {
    this->_ivci = vk::ImageViewCreateInfo{
      {},
      nullptr,
      vk::ImageViewType::e2D,
      format,
      {
        vk::ComponentSwizzle::eR,
        vk::ComponentSwizzle::eG,
        vk::ComponentSwizzle::eB,
        vk::ComponentSwizzle::eA,
      },
      imageRange
    };
    this->targetTexture = std::make_shared<ExternalTexture>(
      dev,
      this->_ivci,
      rawImage
    );
  }

  RenderTarget& RenderTarget::SetImage(
    vk::Image &img,
    vk::Extent2D exts
  ){
    this->extents = exts;
    if(auto etp = std::dynamic_pointer_cast<ExternalTexture>(this->targetTexture)){
      etp->_setImage(img);
    }
    return *this;
  }

  vk::Rect2D RenderTarget::GetSubregion(
    vk::Offset2D &regionOffset,
    vk::Extent2D &regionExtents
  ) const{

    //If the specified offset would place the subregion outside the render target area,
    //    the only viable options are:
    //      1) Ignore the offset. Not ideal, because there's probably a reason why
    //          the offset was requested.
    //      2) Attempt to create a valid subregion, deducing where the region would be
    //          considered valid. Not exactly difficult, but can lead to counterintuitive
    //          behaviour.
    //      3) Return an empty subregion. Does not make the underlying issue obvious, but
    //          it prevents an invalid region specification from destroying otherwise valid
    //          render data.
    //
    //The warning should hopefully help explain why an RT isn't showing the expected
    //    results.
    if(regionOffset.x > this->extents.width | regionOffset.y > this->extents.height){
      ConsoleLog(LogType::WARN,"[IG:RT] Requested subregion offset has a dimension which exceeds the \
extents of the actual render target. This will result in a null render area.\n");
      return vk::Rect2D{};
    }

    vk::Rect2D subregion{regionOffset,regionExtents};
    //Ensure the subregion extents fit within the RT area, after accounting for
    //    the offset.
    if(regionOffset.x + regionExtents.width > this->extents.width){
      ConsoleLog(LogType::WARN,"[IG:RT] Requested subregion width exceeds the extents of the \
actual render target. Attempting to clamp values. Render operations may not behave as expected.\n");
      subregion.extent.width = this->extents.width - regionOffset.x;
    }
    if(regionOffset.y + regionExtents.height > this->extents.height){
      ConsoleLog(LogType::WARN,"[IG:RT] Requested subregion height exceeds the extents of the \
actual render target. Attempting to clamp values. Render operations may not behave as expected.\n");
      subregion.extent.height = this->extents.height - regionOffset.y;
    }
    return subregion;
  }

  RenderTarget::operator vk::Rect2D() const{
    return vk::Rect2D{vk::Offset2D(),this->extents};
  }

  vk::Extent2D RenderTarget::GetExtents() const{
    return this->extents;
  }

  //STUB: RT instances should mirror Texture{x}D functionality in this regard
  //  (Per-device targetTexture)
  BaseTexture& RenderTarget::GetTexture(Device &owner){
    return *this->targetTexture;
  }


  RenderBuffer::RenderBuffer(
    std::shared_ptr<RenderPassMeta> rpmeta
  ) : rpm(rpmeta){
    this->rpm->InitializeAttachments(this->fbAttachments,this->attNameIndex);
  }

  RenderBuffer& RenderBuffer::SetAttachment(
    std::string id,
    BaseTexture &attachmentSource
  ){
    auto index = this->attNameIndex.find(id);
    if(index != this->attNameIndex.end()){
      auto &attach = this->fbAttachments.at(index->second);
      attach.first = attachmentSource.CreateView();
    }
    return *this;
  }

  RenderBuffer& RenderBuffer::SetClearValue(
    std::string id,
    vk::ClearValue value
  ){
    auto index = this->attNameIndex.find(id);
    if(index != this->attNameIndex.end()){
      auto &attach = this->fbAttachments.at(index->second);
      attach.second = value;
    }
    return *this;
  }

  RenderBuffer& RenderBuffer::SetRenderArea(vk::Rect2D const &area){
    this->renderArea = area;
    return *this;
  }

  RenderBuffer& RenderBuffer::SetExtents(vk::Extent2D const &extents){
    this->renderArea.setExtent(extents);
    return *this;
  }

  RenderBuffer& RenderBuffer::SetExtents(uint32_t const width, uint32_t const height){
    this->SetExtents(vk::Extent2D{width,height});
    return *this;
  }

  RenderBuffer& RenderBuffer::SetRenderOffset(vk::Offset2D const &offset){
    this->renderArea.setOffset(offset);
    return *this;
  }

  void RenderBuffer::Build(Device &dev){
    vk::FramebufferCreateInfo fci = {
      {},
      *this->rpm,
      0,{},
      this->renderArea.extent.width,this->renderArea.extent.height,
      1
    };
    std::vector<vk::ImageView> attachments;
    this->_rpBlanking.clear();
    for(size_t index = 0; index < this->fbAttachments.size(); ++index){
      auto attach = this->fbAttachments.find(index);
      //We know the element SHOULD exist, but there could be a skipped index.
      if(attach != this->fbAttachments.end()){
        attachments.push_back(*attach->second.first);
        this->_rpBlanking.push_back(attach->second.second);
      }
    }
    fci.setAttachmentCount(static_cast<uint32_t>(attachments.size()));
    fci.setPAttachments(attachments.data());

    this->framebuffer = std::shared_ptr<vk::Framebuffer>(
      new vk::Framebuffer(
        static_cast<vk::Device>(dev).createFramebuffer(fci,Igneous::allocators)
      ),
      dev._manager
    );
  }

  vk::RenderPassBeginInfo RenderBuffer::StartPass(){
    return vk::RenderPassBeginInfo{
			*this->rpm,
			*this->framebuffer,
			this->renderArea,
			static_cast<uint32_t>(this->_rpBlanking.size()),this->_rpBlanking.data()
		};
  }

  RenderBuffer::operator bool() const{
    return *this->framebuffer;
  }
}
