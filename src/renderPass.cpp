// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include "renderPass.h"

#include "miniLog.h"

namespace Igneous{

  //*grumble grumble*
  //Defined in resource translation unit
  extern vk::AllocationCallbacks* allocators;

  //RenderPass Metadata
  RenderPassMeta::RenderPassMeta(
    vk::RenderPass &pass,
    std::vector<vk::AttachmentDescription> &attachment_definitions,
    std::map<std::string,uint32_t> &id_mapping
  ) :
    rp(pass),
    _attach_map(id_mapping),
    _attach_defs(attachment_definitions)
  {}

  void RenderPassMeta::InitializeAttachments(
    std::map<uint32_t,std::pair<BaseTexture::view,vk::ClearValue>> &atts,
    std::map<std::string,uint32_t> &id_mapping
  ){
    ConsoleLog(LogType::DEBUG,"[IG::RPM] - Initializing Instance Attachments\n");
    for(auto &attDef : this->_attach_map){
      ConsoleLog(LogType::DEBUG,"[IG::RPM]   + %s\n",attDef.first.c_str());
      atts.try_emplace(attDef.second, std::make_pair(BaseTexture::view(),vk::ClearValue{}));
      id_mapping.try_emplace(attDef.first,attDef.second);
    }
    ConsoleLog(LogType::DEBUG,"[IG::RPM] - Finished Initializing Instance Attachments\n");
  }

  RenderPassMeta::operator vk::RenderPass() const{
    return this->rp;
  }

  //RenderPass Generator Methods
  void RenderPassGenerator::AddAttachment(
    std::string id,
    vk::Format attachmentFormat,
    uint8_t sampleCount,
    AttachmentOperationConfiguration colorConfig,
    AttachmentOperationConfiguration stencilConfig,
    vk::ImageLayout initialLayout,
    vk::ImageLayout finalLayout
  ){
    auto attach = vk::AttachmentDescription{
      {},
      attachmentFormat,
      vk::SampleCountFlagBits::e1,
      vk::AttachmentLoadOp::eDontCare,
      vk::AttachmentStoreOp::eDontCare,
      vk::AttachmentLoadOp::eDontCare,
      vk::AttachmentStoreOp::eDontCare,
      initialLayout,
      finalLayout
    };
    auto _sampleCount = vk::SampleCountFlagBits::e1;
    // *blech*
    //TODO: Round invalid sample requests to next valid tier (5->8, 24->32, etc)?
    //			Alternatively, treat input as a power of two, and limit to range
    //			0->6
    switch(sampleCount){
      case 1:
        break;
      case 2:
        _sampleCount = vk::SampleCountFlagBits::e2;
        break;
      case 4:
        _sampleCount = vk::SampleCountFlagBits::e4;
        break;
      case 8:
        _sampleCount = vk::SampleCountFlagBits::e8;
        break;
      case 16:
        _sampleCount = vk::SampleCountFlagBits::e16;
        break;
      case 32:
        _sampleCount = vk::SampleCountFlagBits::e32;
        break;
      case 64:
        _sampleCount = vk::SampleCountFlagBits::e64;
        break;
      default:
        ConsoleLog(LogType::ERR,R"([VK] Invalid sample count %d specified for RP attachment:\
          Defaulting to single sample\n)",
          sampleCount);
    }
    attach.setSamples(_sampleCount);

    //Color Load/Write Operation Configuration
    if(colorConfig.loadEnabled){
      if(colorConfig.loadPreserve) attach.setLoadOp(vk::AttachmentLoadOp::eLoad);
      else attach.setLoadOp(vk::AttachmentLoadOp::eClear);
    }
    if(colorConfig.writeEnabled){
      attach.setStoreOp(vk::AttachmentStoreOp::eStore);
    }

    //Stencil Operation Configuration
    if(stencilConfig.loadEnabled){
      if(stencilConfig.loadPreserve) attach.setStencilLoadOp(vk::AttachmentLoadOp::eLoad);
      else attach.setStencilLoadOp(vk::AttachmentLoadOp::eClear);
    }
    if(stencilConfig.writeEnabled){
      attach.setStencilStoreOp(vk::AttachmentStoreOp::eStore);
    }
    auto index = this->attachments.size();
    this->attachments.push_back(attach);
    this->id_mapping.try_emplace(id,index);
  }

  uint32_t RenderPassGenerator::AddSubpass(
    const std::vector<std::string> &inputAttachments,
    const std::vector<std::string> &colorAttachments,
    const std::vector<std::string> &resolveAttachments,
    const std::vector<std::string> &preserveAttachments,
    bool hasDepthStencil,
    const std::string depthStencilID
  ){
    vk::AttachmentReference _depthStencil;
    std::vector<vk::AttachmentReference> _inputs;
    std::vector<vk::AttachmentReference> _colors;
    std::vector<vk::AttachmentReference> _resolves;
    std::vector<uint32_t> _preserves;

    std::map<std::string,std::vector<vk::AttachmentReference>> references = {
      {"Input",{}},
      {"Color",{}},
      {"Resolve",{}}
    };

    for(auto &arSet : references){
      auto vecSet = inputAttachments;
      if(arSet.first == "Color") vecSet = colorAttachments;
      else if(arSet.first == "Resolve") vecSet = resolveAttachments;
      for(auto attachIndex : vecSet){
        auto index = this->id_mapping.find(attachIndex);
        if(index != this->id_mapping.end()){
          auto attach = this->attachments.at(index->second);
          auto ref = vk::AttachmentReference{
            index->second,
            attach.finalLayout
          };
          if(ref.layout == vk::ImageLayout::ePresentSrcKHR){
            ref.layout = vk::ImageLayout::eColorAttachmentOptimal;
          }
          arSet.second.push_back(ref);
        }
      }
    }

    for(auto attachIndex : preserveAttachments){
      auto index = this->id_mapping.find(attachIndex);
      if(index != this->id_mapping.end()){
        _preserves.push_back(index->second);
      }
    }

    if(hasDepthStencil){
      auto index = this->id_mapping.find(depthStencilID);
      if(index != this->id_mapping.end()){
        auto attach = this->attachments.at(index->second);
        _depthStencil = vk::AttachmentReference{
          index->second,
          attach.finalLayout
        };
      }
    }

    auto index = static_cast<uint32_t>(this->subpasses.size());
    this->subpasses.emplace_back(
      references["Input"],
      references["Color"],
      references["Resolve"],
      _preserves,
      _depthStencil
    );
    return index;
  }

  ///@todo Dependency creation should be handled by subpass instances, as it can
  ///       eliminate the requirement to know explicit indices for subpasses.
  ///       Doing so will require subpasses to know their creation index.
  void RenderPassGenerator::AddSubpassDependency(
    uint32_t sourceSubpass,
    uint32_t destinationSubpass,
    vk::PipelineStageFlags sourceStageMask,
    vk::PipelineStageFlags destinationStageMask,
    vk::AccessFlags sourceAccess,
    vk::AccessFlags destinationAccess,
    vk::DependencyFlags dependencyFlags
  ){
    vk::SubpassDependency dep = {
      sourceSubpass,
      destinationSubpass,
      sourceStageMask,
      destinationStageMask,
      sourceAccess,
      destinationAccess,
      dependencyFlags
    };
    this->spDepends.push_back(dep);
  }

  std::shared_ptr<RenderPassMeta> RenderPassGenerator::Generate(Device &device){
    ConsoleLog(LogType::DEBUG,"[VKRP]   + Building RP\n");
    std::vector<vk::SubpassDescription> spDesc(this->subpasses.begin(),this->subpasses.end());
    vk::RenderPassCreateInfo info = {
      {},
      static_cast<uint32_t>(this->attachments.size()),this->attachments.data(),
      static_cast<uint32_t>(spDesc.size()),spDesc.data(),
      //TODO: Actually handle subpass dependencies
      //static_cast<uint32_t>(this->spDepends.size()),this->spDepends.data()
      0,nullptr
    };
    auto rp = static_cast<vk::Device>(device).createRenderPass(info,Igneous::allocators);
    if(!rp) ConsoleLog(LogType::ERR,"[VKRP]   + Failed to Build RP\n");
    else ConsoleLog(LogType::DEBUG,"[VKRP]   + Finished Building RP\n");
    return std::shared_ptr<RenderPassMeta>(
      new RenderPassMeta(
        rp,
        this->attachments,
        this->id_mapping
      ),
      device._manager
    );
  }

  RenderPassGenerator::Subpass::Subpass(
    const std::vector<vk::AttachmentReference> &inputAttachments,
    const std::vector<vk::AttachmentReference> &colorAttachments,
    const std::vector<vk::AttachmentReference> &resolveAttachments,
    const std::vector<uint32_t> &preserveAttachments,
    const vk::AttachmentReference &depthStencil
  ){
    //Copy data references
    this->_inputs = std::move(inputAttachments);
    this->_colors = std::move(colorAttachments);
    this->_resolves = std::move(resolveAttachments);
    this->_stencil = std::move(depthStencil);
    this->_preserves = std::move(preserveAttachments);
    if(this->_stencil.layout != vk::ImageLayout::eUndefined) ConsoleLog(LogType::DEBUG,"[VKRS]   + Subpass Contains Depth Stencil\n");
  }

  RenderPassGenerator::Subpass::operator vk::SubpassDescription() const{
    vk::SubpassDescription ret = {
      {},
      vk::PipelineBindPoint::eGraphics,
      static_cast<uint32_t>(this->_inputs.size()),this->_inputs.data(),
      static_cast<uint32_t>(this->_colors.size()),this->_colors.data(),this->_resolves.data(),
      nullptr,
      static_cast<uint32_t>(this->_preserves.size()),this->_preserves.data()
    };
    //ARTEFACT: Depth stencil is configured separately, as we need to
    //		verify whether the stored stencil is actually valid.
    //		If layout is ImageLayout::eUndefined, it's an invalid stencil.
    if(this->_stencil.layout != vk::ImageLayout::eUndefined) ret.setPDepthStencilAttachment(&this->_stencil);
    return ret;
  }
}
