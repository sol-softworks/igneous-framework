// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "Conduit/dsmeta.h"

#include "miniLog.h"

namespace Igneous{

  //*grumble grumble*
	//Defined in resource translation unit
	extern vk::AllocationCallbacks* allocators;

  DescriptorLayoutMeta::DescriptorLayoutMeta(Device &dev) : _dev(dev){
		this->layout_bindings.size();
  }

  DescriptorLayoutMeta::~DescriptorLayoutMeta(){
    static_cast<vk::Device>(this->_dev).destroyDescriptorSetLayout(this->layout,Igneous::allocators);
  }

  void DescriptorLayoutMeta::CompileLayout(){
    auto bindView = std::vector<vk::DescriptorSetLayoutBinding>();
    bindView.reserve(this->layout_bindings.size());
    for(auto &bind : this->layout_bindings){
      bindView.push_back(bind.second);
    }
    vk::DescriptorSetLayoutCreateInfo lInfo = {
      {},
      static_cast<uint32_t>(bindView.size()),bindView.data()
    };
    this->layout = static_cast<vk::Device>(this->_dev).createDescriptorSetLayout(lInfo,Igneous::allocators);
  }

  std::weak_ptr<DescriptorSetMeta> DescriptorLayoutMeta::CreateSet(){
    auto newDS = std::make_shared<DescriptorSetMeta>(this->_dev,*this);
		for(auto &bind : this->layout_bindings){
			std::shared_ptr<BaseBinding> binding;
			//Determine Property class from descriptor type
			//		Will eventually include Property types for all descriptor types.
			switch(bind.second.descriptorType){
				//All three cases use a TextureProperty, so fall-through is necessary
				case vk::DescriptorType::eSampler:
					newDS->AddBinding<SamplerBinding>(bind.first.second,bind.second);
					break;
				case vk::DescriptorType::eCombinedImageSampler:
					newDS->AddBinding<CombinedSampleImageBinding>(bind.first.second,bind.second);
					break;
				case vk::DescriptorType::eSampledImage:
					newDS->AddBinding<SampledImageBinding>(bind.first.second,bind.second);
					break;
				case vk::DescriptorType::eUniformBuffer:
					///@todo  Dynamic buffer resizing is possible in Igneous, but shouldn't
					///				be mandatory for statically-sized buffers.
					newDS->AddBinding<BufferBinding>(bind.first.second,bind.second);
					break;
				//Any types not defined currently use BaseDescriptorSetBinding
				default:
					newDS->AddBinding(bind.first.second,bind.second);
					break;
			}

		}
    auto inst = this->_instances.emplace(std::move(newDS));
    return *inst.first;
  }

	//Descriptor Set Configuration / Updating
	void DescriptorLayoutMeta::AddLayoutBinding(std::string bindID, std::tuple<vk::DescriptorType,vk::ShaderStageFlagBits,uint32_t> binding){
		auto thisBindIndex = static_cast<uint32_t>(this->layout_bindings.size());
		this->layout_bindings.try_emplace(
			std::pair{thisBindIndex,bindID},
			vk::DescriptorSetLayoutBinding{
				thisBindIndex,
				std::get<0>(binding),
				std::get<2>(binding),
				std::get<1>(binding),
				nullptr
			}
		);
	}

	DescriptorLayoutMeta::operator vk::DescriptorSetLayout() const{
		return this->layout;
	}

  DescriptorSetMeta::DescriptorSetMeta(
    Device &dev,
    DescriptorLayoutMeta &dsl
  ) : _dev(dev),
      ds(dev.AddDescriptorSet(dsl))
  {}

  DescriptorSetMeta::~DescriptorSetMeta(){
    this->_dev.FreeDescriptors(this->ds);
  }

	void DescriptorSetMeta::Update(){
		if(this->_bindUpdater){
			std::vector<vk::WriteDescriptorSet> writeInfos;
			for(auto &bind : this->bindings){
				if(this->_bindUpdater.isSet(bind.second->binding.binding)){
					auto _bInfo = bind.second->CreateWriteInfo();
					_bInfo.dstSet = this->ds;
					writeInfos.push_back(_bInfo);
				}
			}
			static_cast<vk::Device>(this->_dev).updateDescriptorSets(writeInfos.size(),writeInfos.data(),0,nullptr);
			this->_bindUpdater.reset();
		}
	}

  DescriptorSetMeta::operator vk::DescriptorSet() const{
    return this->ds;
  }
}
