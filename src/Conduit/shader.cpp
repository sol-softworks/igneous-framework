// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include <fstream>

#include "spirv_reflect.hpp"
#include "spirv_common.hpp"

#include "Conduit/shader.h"
#include "miniLog.h"

#include "Conduit/pc_elements.h"

namespace Igneous{

  //*grumble grumble*
  //Defined in resource translation unit
  extern vk::AllocationCallbacks* allocators;

  const std::map<spv::ExecutionModel,vk::ShaderStageFlagBits> stageFlag{
    {spv::ExecutionModel::ExecutionModelVertex,vk::ShaderStageFlagBits::eVertex},
    {spv::ExecutionModel::ExecutionModelFragment,vk::ShaderStageFlagBits::eFragment},
    {spv::ExecutionModel::ExecutionModelGLCompute,vk::ShaderStageFlagBits::eCompute},
    {spv::ExecutionModel::ExecutionModelGeometry,vk::ShaderStageFlagBits::eGeometry},
    {spv::ExecutionModel::ExecutionModelTessellationControl,vk::ShaderStageFlagBits::eTessellationControl},
    {spv::ExecutionModel::ExecutionModelTessellationEvaluation,vk::ShaderStageFlagBits::eTessellationEvaluation}
    //Ray Tracing ignored, as there isn't a standard interface in Vulkan (yet).
  };

  vk::PushConstantRange Shader::_PCBlockDef::Define(){
    return vk::PushConstantRange{
      this->stages,
      static_cast<uint32_t>(this->offset),
      static_cast<uint32_t>(this->size)
    };
  }

  std::map<std::string,Shader> sharedShaders;

  Shader::Shader(
    std::vector<uint32_t> &shaderCode
  ) :
    shader(std::move(shaderCode))
  {
    ConsoleLog(LogType::DEBUG,"[IG:SHADER] Performing Reflection on shader\n");
    spirv_cross::CompilerReflection cc(this->shader);
    auto res = cc.get_shader_resources();

    const std::map<vk::DescriptorType,spirv_cross::SmallVector<spirv_cross::Resource>> bindTypes{
      {vk::DescriptorType::eSampler, res.separate_samplers},
      {vk::DescriptorType::eCombinedImageSampler, res.sampled_images},
      {vk::DescriptorType::eSampledImage, res.separate_images},
      {vk::DescriptorType::eStorageImage, res.storage_images},
      {vk::DescriptorType::eUniformBuffer, res.uniform_buffers},
      {vk::DescriptorType::eStorageBuffer, res.storage_buffers}
    };

    vk::ShaderStageFlags accessFlags;

    auto entryPoints = cc.get_entry_points_and_stages();
    ConsoleLog(LogType::DEBUG,"[IG:SHADER] - Processing Entrypoints\n");
    for(auto &ep : entryPoints){
      ConsoleLog(LogType::DEBUG,"[IG:SHADER]   + %s:%s\n",
        ep.name.c_str(), to_string(stageFlag.at(ep.execution_model)).c_str());
      this->entry_points.try_emplace(
        ep.name,
        stageFlag.at(ep.execution_model)
      );
      //@todo Improve usage determination process, to more appropriately limit
      //      descriptor resource access.
      accessFlags |= stageFlag.at(ep.execution_model);
    }

    for(auto &type : bindTypes){
      ConsoleLog(LogType::DEBUG,"[IG:SHADER] - Processing Type: %s\n",to_string(type.first).c_str());
      for(auto &elem : type.second){
        ConsoleLog(LogType::DEBUG,"[IG:SHADER]   + Element: %s\n",elem.name.c_str());
        auto elemType = cc.get_type(elem.type_id);
        uint32_t elemSize = 1;
        for(auto &dim : elemType.array) elemSize *= dim;
        auto bindID = std::make_pair(
          cc.get_decoration(elem.id,spv::DecorationDescriptorSet),
          cc.get_decoration(elem.id,spv::DecorationBinding)
        );
        auto ref = this->shaderBindings.find(bindID);
        if(ref == this->shaderBindings.end()){
          this->shaderBindings.emplace(
            SetBinding{
              cc.get_decoration(elem.id,spv::DecorationDescriptorSet),
              cc.get_decoration(elem.id,spv::DecorationBinding),
              elem.name,
              type.first,
              elemSize,
              accessFlags
            }
          );
        }
        else{
          //Recreate the node, but append our own flags to the existing flags
          auto node = this->shaderBindings.extract(ref);
          node.value() |= accessFlags;
          this->shaderBindings.insert(std::move(node));
        }
      }
    }

    //Technically, Vulkan only allows one push constant block to exist in a
    //    pipeline.
    for(auto &pc : res.push_constant_buffers){
      auto elem_type = cc.get_type(pc.base_type_id);
      //Should be the case, but verify.
      if(elem_type.basetype == spirv_cross::SPIRType::Struct){
        ConsoleLog(LogType::DEBUG,"[IG:SHADER] - Processing Push Constants\n");
        auto count = elem_type.member_types.size();
        ConsoleLog(LogType::DEBUG,"[IG:SHADER]   + %d blocks\n",count);
        for(size_t i=0; i<count; ++i){
          auto &name = cc.get_member_name(elem_type.self,i);
          auto &blockType = cc.get_type(elem_type.member_types[i]);
          if(blockType.basetype == spirv_cross::SPIRType::Struct){
            ConsoleLog(LogType::WARN,"[IG:SHADER]   + Detected struct-type element in push constant layout! \
Conduit does not generate definitions for struct members in push constants. Ignoring member %s",name.c_str());
            continue;
          }
          ConsoleLog(LogType::DEBUG,"[IG:SHADER]     * %s\n",name.c_str());
          auto blockSize = cc.get_declared_struct_member_size(elem_type,i);
          ConsoleLog(LogType::DEBUG,"[IG:SHADER]     * Block Size: %u\n",blockSize);
          auto offset = cc.type_struct_member_offset(elem_type,i);
          ConsoleLog(LogType::DEBUG,"[IG:SHADER]     * Offset: %u\n",offset);

          //Scalar alignment deduction
          uint32_t stride = 0;

          uint32_t count = 1;
          if(!blockType.array.empty()){
            ConsoleLog(LogType::DEBUG,"[IG:SHADER]     * Array Type\n");
            //Array stride
            stride = cc.type_struct_member_array_stride(elem_type,i);
            count = blockSize / stride;
          }
          else if(blockType.columns > 1){
            ConsoleLog(LogType::DEBUG,"[IG:SHADER]     * Matrix Type\n");
            //Matrix column stride
            stride = cc.type_struct_member_matrix_stride(elem_type,i);
            count = blockSize / stride;
          }
          else if(blockType.vecsize > 1){
            ConsoleLog(LogType::DEBUG,"[IG:SHADER]     * Vector Type\n");
            count = blockType.vecsize;
            stride = blockSize / blockType.vecsize;
          }
          else stride = blockSize;
          ConsoleLog(LogType::DEBUG,"[IG:SHADER]     * Stride: %u\n",stride);

          this->pc_blocks.try_emplace(
            name,
            _PCBlockDef{
              blockType,
              blockSize,
              count,
              offset,
              accessFlags
            }
          );
        }
      }
    }
  }

  Shader::~Shader(){}

  void Shader::AddPushConstants(std::map<std::string,_PCBlockDef> &pc_defs){
    for(auto &block : this->pc_blocks){
      auto result = pc_defs.try_emplace(
        block.first,
        block.second
      );
      if(!result.second){
        pc_defs.at(block.first).stages |= block.second.stages;
      }
    }
  }

  void Shader::AddBindingDefs(std::set<SetBinding,BindingComp> &bindSet){
    ConsoleLog(LogType::DEBUG,"[IG:SHADER] Binding Count: %u\n",this->shaderBindings.size());
    for(auto &binding : this->shaderBindings){
      auto bind = bindSet.find(binding);
      if(bind != bindSet.end()){
        auto node = bindSet.extract(bind);
        node.value() |= binding.stages;
        bindSet.insert(std::move(node));
      }
      else{
        bindSet.emplace(
          SetBinding{
            binding.layoutIndex,
            binding.bindingIndex,
            binding.name,
            binding.type,
            binding.dimensionality,
            binding.stages
          }
        );
      }
    }
  }

  vk::ShaderModule Shader::Build(vk::Device device){
    vk::ShaderModuleCreateInfo info = {
      {},
      static_cast<uint32_t>(this->shader.size())*4, //Size, in bytes.
      this->shader.data()
    };
    return device.createShaderModule(info,Igneous::allocators);
  }

  void Shader::Destroy(vk::Device device, vk::ShaderModule module){
    device.destroyShaderModule(module,Igneous::allocators);
  }

  void Shader::Define(
    std::filesystem::path shaderPath,
    std::string name
  ){
    ConsoleLog(LogType::DEBUG,"[IG:SHADER] Defining Shader\n");
    //std::basic_ifstream<uint16_t> file(filename,std::ios::ate | std::ios::binary);
    std::ifstream file(shaderPath,std::ios::binary);
    if(!file.is_open()){
      ConsoleLog(LogType::ERR,"[VK] - Unable to read SPIR-V source!\n");
      if(file.fail()){
        ConsoleLog(LogType::ERR,"[VK]   - Unable to open file %s\n",shaderPath.c_str());
      }
      return;
    }
    file.setf(std::ios::hex);
    auto fSize = std::filesystem::file_size(shaderPath);
    auto code = std::vector<uint32_t>(fSize/4);
    auto fData = std::vector<char>(fSize);
    file.read(fData.data(),fSize);
    file.close();
    for(auto i=0; i<fSize; i+=4){
      //To avoid sign extension during the cast, mask the lower
      //		byte (which contains the actual value) for each
      //		component.
      //
      //Could be avoided by implementing CharTraits for a uint32_t
      //		filestream, but laziness and apathy.
      code[i/4] = {
        (static_cast<uint32_t>(fData[i]) & 0xFF) |
        ((static_cast<uint32_t>(fData[i+1]) & 0xFF) << 8) |
        ((static_cast<uint32_t>(fData[i+2]) & 0xFF) << 16) |
        ((static_cast<uint32_t>(fData[i+3]) & 0xFF) << 24)
      };
    }
    //Verify the provided shader is SPIR-V bytecode
    //  (Magic Number: 0x07230203)
    if(code[0] != 0x07230203){
      ConsoleLog(LogType::ERR,"[IG:SHADER] - Path does not point to SPIR-V bytecode!\n");
      ConsoleLog(LogType::ERR,"[IG:SHADER] - Aborting Shader construction\n");
      return;
    }
    if(name == ""){
      if(shaderPath.extension() == ".spv"){
        name = shaderPath.stem();
      }
      else name = shaderPath.filename();
      ConsoleLog(LogType::DEBUG,"[IG:SHADER] Deduced name %s\n",name.c_str());
    }
    sharedShaders.try_emplace(name,Shader(code));
    ConsoleLog(LogType::DEBUG,"[IG:SHADER] Shader %s definition established.\n",name.c_str());
  }
}
