// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "resource.h"

#include "surface.h"

#include "miniLog.h"

#include <fstream>

namespace Igneous{

	//Internal Members
	std::vector<PD> physicalDevices = {};
	static std::vector<vk::PhysicalDevice> physical = {};

	vk::Instance inst;

	std::map<std::reference_wrapper<const vk::Device>,vk::PipelineCache,std::less<vk::Device>> cacheStorage;

	//TODO: Any tangible benefit in defining custom allocators? Apart from logging?
	//		Internal{Allocation,Free} are logging, but Alloc/Realloc/Free are a bit
	//		more involved.
	vk::AllocationCallbacks *allocators = nullptr;

	//Validation Layers
	#ifdef DEBUG_BUILD
	const std::array<const char*,1> validationLayers = {
		"VK_LAYER_LUNARG_standard_validation"
	};
	#else
	const std::array<const char*,0> validationLayers = {};
	#endif


	static vk::DispatchLoaderDynamic dynload;


	//Internal Utility Methods
	static uint32_t getMemoryType(vk::PhysicalDeviceMemoryProperties memProps, uint32_t type, vk::MemoryPropertyFlags props){
		for(uint32_t i=0; i<memProps.memoryTypeCount; ++i){
			if(type & (1<<i) && (memProps.memoryTypes[i].propertyFlags & props) == props){
				return i;
			}
		}
		return 0xffffffff;
	}


	//Vulkan API Management
	//TODO: Move into separate file?
	void InitAPI(std::vector<const char*> vkExtensionList){
		ConsoleLog(LogType::INFO,"[VK] - Creating Vulkan Instance\n");
		if(!inst){
			inst = vk::createInstance({{},nullptr,validationLayers.size(),validationLayers.data(),static_cast<uint32_t>(vkExtensionList.size()),vkExtensionList.data()},Igneous::allocators);
		}
		ConsoleLog(LogType::INFO,"[VK] - Enumerating Vulkan Physical Devices\n");
		if(physical.size() == 0){
			physical = inst.enumeratePhysicalDevices();
			ConsoleLog(LogType::INFO,"[VK]   + %d devices\n",physical.size());
			for(auto &pd : physical){
				physicalDevices.emplace_back(pd);
			}
		}
	}

	void ReleaseAPI(){
		ConsoleLog(LogType::INFO,"[VK] Releasing Vulkan Objects\n");
		physicalDevices.clear();
		inst.destroy(Igneous::allocators);
	}

	PD& GetPhysicalDevice(size_t index){
		return physicalDevices.at(index);
	}

	vk::PipelineCache GetPipelineCache(vk::Device const &dev){
		return cacheStorage.at(dev);
	}


	//Physical Device Lifecycle Management
	PD::PD(
		vk::PhysicalDevice &pd
	) :
		phys(pd),
		queueProps(pd.getQueueFamilyProperties()[0]),
		features(pd.getFeatures())
	{
		//Configure Required features
		//
		//TODO: User customization of features
		this->features.shaderClipDistance = 1;
		this->features.shaderCullDistance = 1;
		this->features.samplerAnisotropy = 1;
	}

	PD::~PD(){
		//Hmm.
	}

	PD::PD(PD &&b) :
		phys(std::move(b.phys)),
		queueProps(std::move(b.queueProps)),
		features(std::move(b.features))
	{
		this->logicalDevices = std::move(b.logicalDevices);
	}

	//PD->LD Management
	std::unique_ptr<Device> PD::AddDevice(std::vector<vk::DeviceQueueCreateInfo> dqciArray, std::vector<const char*> devExt){
		vk::DeviceCreateInfo dcInfo = {
				{},
				static_cast<uint32_t>(dqciArray.size()),dqciArray.data(),
				static_cast<uint32_t>(validationLayers.size()),validationLayers.data(),
				static_cast<uint32_t>(devExt.size()),devExt.data(),
				&this->features
			};
		auto ld = this->phys.createDevice(dcInfo,Igneous::allocators);
		auto &ld_ptr = this->logicalDevices.emplace_back(std::make_unique<vk::Device>(ld));

		return std::make_unique<Device>(this->phys,*ld_ptr);
	}


	//Logical Device Lifecycle Management
  //
  //TODO: Major overhaul of logical device construction (more configurable)
	Device::Device(
		vk::PhysicalDevice &pd,
		vk::Device &dev
	) :
		phys(pd),
		logicalDevice(dev),
		taskList(logicalDevice,0,0,10),
		memProps(pd.getMemoryProperties()),
		_manager(this->logicalDevice)
	{
		//Create Descriptor Pool
		std::array<vk::DescriptorPoolSize,11> poolSizes = {
			vk::DescriptorPoolSize(vk::DescriptorType::eSampler, 1000),
			{vk::DescriptorType::eCombinedImageSampler, 1000},
			{vk::DescriptorType::eSampledImage, 1000},
			{vk::DescriptorType::eStorageImage, 1000},
			{vk::DescriptorType::eUniformTexelBuffer, 1000},
			{vk::DescriptorType::eStorageTexelBuffer, 1000},
			{vk::DescriptorType::eUniformBuffer, 1000},
			{vk::DescriptorType::eStorageBuffer, 1000},
			{vk::DescriptorType::eUniformBufferDynamic, 1000},
			{vk::DescriptorType::eStorageBufferDynamic, 1000},
			{vk::DescriptorType::eInputAttachment, 1000},
		};
		this->descPool = this->logicalDevice.createDescriptorPool(
			{
				{vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet},
				1000*poolSizes.size(),
				poolSizes.size(),
				poolSizes.data()
			},
			Igneous::allocators);

		//Create Device Semaphores
		for(int i=0; i<this->semaphore.size(); ++i){
			this->semaphore[i] = this->logicalDevice.createSemaphore({},
				Igneous::allocators);
		}

		//Pre-generate pipeline cache.
		///@todo Load persistent cache state, if one exists for the physical device
		cacheStorage.emplace(
			this->logicalDevice,
			this->logicalDevice.createPipelineCache(
				{
					{},
					0,nullptr
				},
				Igneous::allocators
			)
		);

		//this->setData = std::set<std::pair<vk::DescriptorSet,vk::DescriptorSetLayout>>(10);
		//this->setData = std::set<std::pair<vk::DescriptorSet,vk::DescriptorSetLayout>>();
		//10 buckets, avg 10 load -> 1000 elements.
		//	"Conveniently" matches the size of the descriptor pool
		//this->setData.max_load_factor(10);
	}

	Device::~Device(){
		ConsoleLog(LogType::INFO,"[VK] Destroying Logical Device\n");
		//Free allocated memory (images, buffers, etc.)
		this->memoryAllocations.clear();

		this->taskList._destroy();

		//this->ResetDescriptorPool();
		this->logicalDevice.destroyDescriptorPool(this->descPool,Igneous::allocators);
		for(int i=0; i<this->semaphore.size(); ++i){
			this->logicalDevice.destroySemaphore(this->semaphore[i],Igneous::allocators);
		}

		//Destroy Pipeline Cache for this device
		this->logicalDevice.destroyPipelineCache(
			cacheStorage.at(this->logicalDevice),
			Igneous::allocators
		);

		this->logicalDevice.destroy(Igneous::allocators);
	}

	Device::Device(
		Device &&b
	) :
		phys(b.phys),
		logicalDevice(b.logicalDevice),
		taskList(std::move(b.taskList)),
		memProps(std::move(b.memProps)),
		_manager(std::move(b._manager))
	{
		this->descPool = std::move(b.descPool);
		this->semaphore = std::move(b.semaphore);
	}

	//Logical Device Memory Management
	Device::mem_ptr Device::malloc(uint32_t size, uint32_t typeIndex){
		auto mem = this->logicalDevice.allocateMemory({size,typeIndex},Igneous::allocators);
		auto ret = this->memoryAllocations.insert_after(
			this->memoryAllocations.before_begin(),
			std::unique_ptr<vk::DeviceMemory,ResourceManager>(
				new vk::DeviceMemory(std::move(mem)),
				this->_manager
			)
		);
		return ret->get();
	}

	//TODO: Find a way to remove the need to specify memory property flags
	Device::mem_ptr Device::balloc(vk::Buffer &buf, vk::MemoryPropertyFlags props){
		vk::MemoryRequirements memReq = this->logicalDevice.getBufferMemoryRequirements(buf);
		auto blk = this->malloc(memReq.size,getMemoryType(this->memProps,memReq.memoryTypeBits,props));
		this->logicalDevice.bindBufferMemory(buf,*blk,0);
		return blk;
	}

	//TODO: Find a way to remove the need to specify memory property flags
	Device::mem_ptr Device::ialloc(vk::Image &img, vk::MemoryPropertyFlags props){
		vk::MemoryRequirements memReq = this->logicalDevice.getImageMemoryRequirements(img);
		auto blk = this->malloc(memReq.size,getMemoryType(this->memProps,memReq.memoryTypeBits,props));
		this->logicalDevice.bindImageMemory(img,*blk,0);
		return blk;
	}

	void Device::free(Device::mem_ptr memory){
		//Underlying free is delegated to the resource manager.
		this->memoryAllocations.remove_if(
			[&](std::unique_ptr<vk::DeviceMemory,ResourceManager> &mem){
				return mem.get() == memory;
			}
		);
	}

	void Device::memcpy(Device::mem_ptr memory, void* buffer, size_t size){
		auto buf = this->logicalDevice.mapMemory(*memory,0,size,{});
		::memcpy(buf,buffer,size);
		this->logicalDevice.unmapMemory(*memory);
	}

	void Device::memcpy(void* buffer, Device::mem_ptr memory, size_t size){
		auto buf = this->logicalDevice.mapMemory(*memory,0,size,{});
		::memcpy(buffer,buf,size);
		this->logicalDevice.unmapMemory(*memory);
	}


	//Logical Device Command Management
	//Shims... Why did there have to be shims
	Device::TaskQueue::_Task Device::StartTask(){
		auto taskPtr = this->taskList.StartTask();
		if(auto task = taskPtr.lock()){
			static_cast<vk::CommandBuffer>(*task).reset({});
			//This is why tasks are not (currently) re-submitable.
			//TODO: Create a separate process for reusable tasks?
			static_cast<vk::CommandBuffer>(*task).begin({vk::CommandBufferUsageFlagBits::eOneTimeSubmit});
		}
		return taskPtr;
	}

	//So many shims...
	void Device::ExecuteTasks(std::vector<vk::PipelineStageFlags> stageFlags, std::vector<vk::Semaphore> waitSem, std::vector<vk::Semaphore> sigSem){
		std::vector<vk::Fence> fences;

		//TODO: Iterate over all task lists
		//for(auto &tq : this->taskList)
		fences.push_back(this->taskList._fence);

		//Reset Fences before TQ submission
		//this->logicalDevice.resetFences(static_cast<uint32_t>(fences.size()),fences.data());
    while(this->logicalDevice.waitForFences(static_cast<uint32_t>(fences.size()),fences.data(),1,100) != vk::Result::eSuccess);
		this->logicalDevice.resetFences(1,&this->taskList._fence);
		this->taskList.SubmitTasks(stageFlags,waitSem,sigSem);
	}

	bool Device::CheckTaskQueueStatus(){
		//TODO: Specify task list to check status
		return this->taskList.QueueFinished();
	}

	void Device::SynchronizeQueues(){
		std::vector<vk::Fence> fences;

		//TODO: Iterate over all task lists
		//for(auto &tq : this->taskList)
		fences.push_back(this->taskList._fence);
		//Wait for all TaskQueues to signal completion
		//Thar be dragons
		//TODO: Check whether vk::Result::eErrorDeviceLost has been raised.
		while(this->logicalDevice.waitForFences(static_cast<uint32_t>(fences.size()),fences.data(),1,100) != vk::Result::eSuccess);
		this->taskList.QueueFinished();
		this->taskList.ResetQueue();
	}


	//Logical Device Task Queue Manager
	Device::TaskQueue::TaskQueue(vk::Device &dev, uint32_t queueFamily, uint32_t queueIndex, uint32_t initBufferCount) :
	_d(dev){
		this->_pool = dev.createCommandPool({
				{vk::CommandPoolCreateFlagBits::eResetCommandBuffer},
				queueFamily
			},
			Igneous::allocators);
		this->queue = this->_d.getQueue(queueFamily,queueIndex);

		this->_fence = this->_d.createFence({vk::FenceCreateFlagBits::eSignaled},
				Igneous::allocators);

		auto cbs = dev.allocateCommandBuffers({this->_pool,vk::CommandBufferLevel::ePrimary,initBufferCount});
		for(auto &buf : cbs){
			this->tasks.emplace_back(std::make_shared<Task>(std::move(buf)));
		}
	}

	Device::TaskQueue::~TaskQueue(){

	}

	void Device::TaskQueue::_destroy(){
		while(this->_d.waitForFences(1,&this->_fence,1,100) != vk::Result::eSuccess);
		this->queue.waitIdle();
		for(auto &task : this->tasks){
			this->_d.freeCommandBuffers(this->_pool,1,&task->buffer);
			task.reset();
		}
		this->_d.destroyFence(this->_fence,Igneous::allocators);
		this->tasks.clear();
		this->_d.destroyCommandPool(this->_pool,Igneous::allocators);
	}

	bool Device::TaskQueue::QueueFinished(){
		if(this->_d.getFenceStatus(this->_fence) == vk::Result::eSuccess){
			//Set all submitted Tasks to "Finished" state
			for(auto &t : this->tasks){
				if(t->state == 3) t->state = 4;
			}
			return true;
		}
		else return false;
	}

	void Device::TaskQueue::ResetQueue(){
		//Reset Tasks
		this->ResetTasks();
	}

	Device::TaskQueue::_Task Device::TaskQueue::StartTask(){
		//Find first available Task
		for(auto &t : this->tasks){
			if(t->state == 0){
				t->state = 1;
				auto ret = _Task{t};
				return ret;
			}
		}
		//No available Tasks located, so allocate new tasks
		//		Pre-allocate in batches, to amortize costs
		auto cbs = this->_d.allocateCommandBuffers({this->_pool,vk::CommandBufferLevel::ePrimary,10});
		for(auto &buf : cbs){
			this->tasks.emplace_back(std::make_shared<Task>(std::move(buf)));
		}
		//Tail recursion FTW
		return this->StartTask();
	}

	void Device::TaskQueue::ResetTasks(){
		if(this->_d.getFenceStatus(this->_fence) != vk::Result::eSuccess) return;
		for(auto &t : this->tasks){
			if(t->state == 4) t->state = 0;
		}
	}

	void Device::TaskQueue::SubmitTasks(std::vector<vk::PipelineStageFlags> stageFlags, std::vector<vk::Semaphore> waitSem, std::vector<vk::Semaphore> sigSem){
		auto tl = std::vector<vk::CommandBuffer>{};
		for(auto &task : this->tasks){
			//TODO: ... What a terrible design
			if(task->state == 2){
				task->state = 3;
				tl.push_back(*task);
			}
		}
		auto subs = vk::SubmitInfo{
			static_cast<uint32_t>(waitSem.size()),waitSem.data(),
			stageFlags.data(),
			static_cast<uint32_t>(tl.size()),tl.data(),
			static_cast<uint32_t>(sigSem.size()),sigSem.data()
		};
		this->queue.submit(1,&subs,this->_fence);
	}

	//Rendering-specific, but needs to be exposed somewhere...
	void Device::TaskQueue::PresentKHR(std::vector<vk::Semaphore> waitSem, vk::SwapchainKHR &chain, uint32_t imageIndex){
		this->queue.presentKHR(vk::PresentInfoKHR{
			static_cast<uint32_t>(waitSem.size()),waitSem.data(),
			1,&chain,&imageIndex,nullptr
		});
	}

	Device::TaskQueue::Task::Task(vk::CommandBuffer buf) : buffer(buf), state(0){}

	Device::TaskQueue::Task::~Task(){
		//Move freeCommandBuffer into destructor?
	}

	void Device::TaskQueue::Task::Submit(){
		//TODO: ... What a terrible design
		if(this->state == 1){
			this->buffer.end();
			this->state = 2;
		}
	}

	Device::TaskQueue::Task::operator vk::CommandBuffer() const{
		return this->buffer;
	}

	Device::TaskQueue::Task::operator VkCommandBuffer() const{
		return static_cast<VkCommandBuffer>(this->buffer);
	}


	//Logical Device Descriptor Set Management
	///@brief	Allocate Descriptor Set to internal storage
	///
	///@details	Defines a new Descriptor Set Layout, and allocates space for a
	///				Descriptor Set that utilizes the layout.
	/*
	Device::dsIter Device::AddDescriptorLayout(std::vector<vk::DescriptorSetLayoutBinding> bindings){
		//Create Descriptor Set Layout
		vk::DescriptorSetLayoutCreateInfo lInfo = {
			{},
			static_cast<uint32_t>(bindings.size()),bindings.data()
		};
		auto layout = this->logicalDevice.createDescriptorSetLayout(lInfo,Igneous::allocators);

		//Allocate Descriptor Set
		vk::DescriptorSetAllocateInfo aInfo = {
			this->descPool,
			1,&layout
		};
		auto newSets = this->logicalDevice.allocateDescriptorSets(aInfo);
		return this->setData.emplace(newSets[0],std::move(layout)).first;
	}
	*/

	vk::DescriptorSet Device::AddDescriptorSet(vk::DescriptorSetLayout layout){
		vk::DescriptorSetAllocateInfo aInfo = {
			this->descPool,
			1,&layout
		};
		return this->logicalDevice.allocateDescriptorSets(aInfo)[0];
	}

	//TODO: Find a convenient way to enable multiple DS allocations
	/*
	void Device::AllocateDescriptors(){
		//Only allocate new descriptors sets
		size_t size = std::distance(this->lastAddedLayout,this->setData.second.end());
		vk::DescriptorSetAllocateInfo aInfo = {
			this->descPool,
			static_cast<uint32_t>(size),
			this->lastAddedLayout
		};
		auto newSets = this->logicalDevice.allocateDescriptorSets(aInfo);
		this->setData.first.insert(this->setData.first.end(),newSets.begin(),newSets.end());

		this->lastAddedLayout = this->setData.second.end();
	}
	*/

	///@todo DescriptorSets should be managed by DescriptorLayoutMeta, which should
	///				consequently manage a personal DescriptorPool
	void Device::FreeDescriptors(vk::DescriptorSet &oldSet){
		this->logicalDevice.freeDescriptorSets(this->descPool,1,&oldSet);
	}

	/*
	void Device::ResetDescriptorPool(){
		for(auto iter = this->setData.begin(); iter != this->setData.end(); ++iter){
			this->FreeDescriptors(iter);
		}
		this->setData.clear();
	}
	*/


	//Logical Device Convenience Casts
	Device::operator vk::Device() const{
		return this->logicalDevice;
	}

	Device::operator VkDevice() const{
		return static_cast<VkDevice>(this->logicalDevice);
	}
}
