// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "baseTexture.h"
#include "buffer.h"

namespace Igneous{

  //*grumble grumble*
	//Defined in resource translation unit
	extern vk::AllocationCallbacks* allocators;

  BaseTexture::BaseTexture(
		Device &dev,
		vk::ImageViewCreateInfo &ivci
	) :
		_ivci(ivci),
		dev(dev)
	{}

		BaseTexture::~BaseTexture(){}

	void BaseTexture::_rebuildViews(){
		auto _newinfo = this->_ivci;
		_newinfo.image = this->_img;

		//Prune view list of previously-deleted entries.
		for(auto iter = this->_views.before_begin(); iter != this->_views.end();){
			auto prev = iter++;
			//"Special" EoL implementation, as iter will reach end before terminator.
			//		Loop construct could be simplified to (auto iter...;;), but the safety
			//		of a guaranteed exit is worth the minimal overhead of checking.
			if(iter == this->_views.end()) break;
			if(iter->expired()){
				this->_views.erase_after(prev);
				iter = prev;
			}
			else{
				static_cast<vk::Device>(this->dev).destroyImageView(*(iter->lock()),Igneous::allocators);
				*iter->lock() = static_cast<vk::Device>(this->dev).createImageView(_newinfo,Igneous::allocators);
			}
		}
	}

  void BaseTexture::_setData(vk::Extent3D &extent, std::vector<uint32_t> &pixelData){
    auto buff = VulkanBuffer(
      this->dev,
      pixelData.size()*sizeof(uint32_t),
      {vk::BufferUsageFlagBits::eTransferSrc},
      {vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent});
    buff.Assign(pixelData);

    //Copy data to image buffer
    auto taskPtr = this->dev.StartTask();
    if(auto task = taskPtr.lock()){
      auto buf = static_cast<vk::CommandBuffer>(*task);
			//Current data is irrelevant, as we're overwriting it anyway.
			//If this becomes the standard process for manipulating texel data, it
			//		would be better to perform a more appropriate transition. It would
			//		also be best to change the second call (post-copy) to transition back
			//		to the original layout.
      this->_transLayout(buf,vk::ImageLayout::eUndefined,vk::ImageLayout::eTransferDstOptimal);
      std::array<vk::BufferImageCopy,1> region = {
        vk::BufferImageCopy(0,0,0,
          {vk::ImageAspectFlagBits::eColor,0,0,1},
          {0,0,0},
          extent
        )
      };
      buf.copyBufferToImage(
        buff,
        this->_img,
        vk::ImageLayout::eTransferDstOptimal,
        region
        );
      //this->_transLayout(buf,vk::ImageLayout::eTransferDstOptimal,vk::ImageLayout::eShaderReadOnlyOptimal);
			this->_transLayout(buf,vk::ImageLayout::eTransferDstOptimal,vk::ImageLayout::eGeneral);
      task->Submit();
    }
    this->dev.ExecuteTasks({},{},{});
		this->dev.SynchronizeQueues();
  }

	void BaseTexture::_debugReadData(vk::Extent3D const &range, vk::Offset3D const &offset){
		auto buff = VulkanBuffer(
      this->dev,
      range.height*range.width*range.depth*sizeof(uint32_t),
      {vk::BufferUsageFlagBits::eTransferDst},
      {vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent});

			auto taskPtr = this->dev.StartTask();
			if(auto task = taskPtr.lock()){
				auto buf = static_cast<vk::CommandBuffer>(*task);
				std::array<vk::BufferImageCopy,1> region = {
					vk::BufferImageCopy(0,0,0,
						{vk::ImageAspectFlagBits::eColor,0,0,1},
	          offset,
	          range
					)
				};
				buf.copyImageToBuffer(
					this->_img,
					vk::ImageLayout::eGeneral,
					buff,
					region
				);
				task->Submit();
			}
			this->dev.ExecuteTasks({},{},{});
			this->dev.SynchronizeQueues();

			//buff._debugReadValue(range, offset);
			buff._debugReadRange(0,range.height*range.width*range.depth);
	}

  void BaseTexture::_transLayout(
		vk::CommandBuffer &buf,
		vk::ImageLayout oldLayout,
		vk::ImageLayout newLayout){
    vk::ImageMemoryBarrier barrier = {
      {},{},
      oldLayout, newLayout,
      VK_QUEUE_FAMILY_IGNORED,VK_QUEUE_FAMILY_IGNORED,
      this->_img,
      {vk::ImageAspectFlagBits::eColor,0,1,0,1}
    };
    std::array<vk::PipelineStageFlags,2> stages;

    //There is a plan to make layout transfers less... terrible.
    //		Right now, what we have here is an affront to sane
    //		development practices.
    //
    //TODO: Put eyes back in sockets. Make this atrocity more robust.
    if(oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eTransferDstOptimal){
      barrier.setDstAccessMask(vk::AccessFlagBits::eTransferWrite);
      stages[0] = vk::PipelineStageFlagBits::eTopOfPipe;
      stages[1] = vk::PipelineStageFlagBits::eTransfer;
    }
    //else if(oldLayout == vk::ImageLayout::eTransferDstOptimal && newLayout == vk::ImageLayout::eShaderReadOnlyOptimal){
		else if(oldLayout == vk::ImageLayout::eTransferDstOptimal && newLayout == vk::ImageLayout::eGeneral){
      barrier.setSrcAccessMask(vk::AccessFlagBits::eTransferWrite).setDstAccessMask(vk::AccessFlagBits::eShaderRead);
      stages[0] = vk::PipelineStageFlagBits::eTransfer;
      stages[1] = vk::PipelineStageFlagBits::eFragmentShader;
    }
    else if(oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal){
      barrier.setDstAccessMask({vk::AccessFlagBits::eDepthStencilAttachmentRead | vk::AccessFlagBits::eDepthStencilAttachmentWrite});
      stages[0] = vk::PipelineStageFlagBits::eTopOfPipe;
      stages[1] = vk::PipelineStageFlagBits::eEarlyFragmentTests;
    }

    if(newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal){
      //TODO: Verify Stencil in _baseFormat
      barrier.subresourceRange.setAspectMask({vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil});
    }
    else{
      barrier.subresourceRange.setAspectMask({vk::ImageAspectFlagBits::eColor});
    }
    buf.pipelineBarrier(
      stages[0],stages[1],{},
      0,nullptr,
      0,nullptr,
      1,&barrier
      );
  }

	BaseTexture::view BaseTexture::CreateView(){

		//Technically, the IVCI reference could be modified directly.
		//		Once Igneous becomes a multi-threaded Hydra, though, this would become
		//		virtually inevitable. Why mutex resources, when you can copy-on-write?
		auto _newinfo = this->_ivci;
		_newinfo.image = this->_img;
		auto ret = BaseTexture::view(
			new vk::ImageView(
				static_cast<vk::Device>(this->dev).createImageView(_newinfo,Igneous::allocators)
			),
			this->dev._manager);
		this->_views.emplace_front(ret);
		return ret;
	}
}
