// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include "drawcmd.h"

namespace Igneous{

  VertexDrawCommand::VertexDrawCommand(
    uint32_t vCount,
    uint32_t vOffset
  ) : vertexCount(vCount), vertexOffset(vOffset)
  {}

  void VertexDrawCommand::Draw(vk::CommandBuffer &buffer){
    this->DrawInstanced(buffer,1,0);
  }

  void VertexDrawCommand::DrawInstanced(vk::CommandBuffer &buffer, uint32_t instances, uint32_t firstInstanceID){
    buffer.draw(this->vertexCount,instances,this->vertexOffset,firstInstanceID);
  }

  IndexedDrawCommand::IndexedDrawCommand(
    uint32_t iCount,
    int32_t vOffset,
    uint32_t iOffset
  ) : indexCount(iCount), vertexOffset(vOffset), indexOffset(iOffset)
  {}

  void IndexedDrawCommand::Draw(vk::CommandBuffer &buffer){
    this->DrawInstanced(buffer,1,0);
  }

  void IndexedDrawCommand::DrawInstanced(vk::CommandBuffer &buffer, uint32_t instances, uint32_t firstInstanceID){
    buffer.drawIndexed(this->indexCount,instances,this->indexOffset,this->vertexOffset,firstInstanceID);
  }

  IndirectDrawCommand::IndirectDrawCommand(
    Device &dev
  ) : commandSets({dev,sizeof(vk::DrawIndirectCommand),{vk::BufferUsageFlagBits::eIndirectBuffer | vk::BufferUsageFlagBits::eTransferDst},{vk::MemoryPropertyFlagBits::eDeviceLocal}})
  {

  }

  void IndirectDrawCommand::Draw(vk::CommandBuffer &buffer){
    this->DrawInstanced(buffer,1,0);
  }

  void IndirectDrawCommand::DrawInstanced(vk::CommandBuffer &buffer, uint32_t instances, uint32_t offset){
    buffer.drawIndirect(this->commandSets,offset,instances,sizeof(vk::DrawIndirectCommand));
  }

  IndirectIndexedDrawCommand::IndirectIndexedDrawCommand(
    Device &dev
  ) : commandSets({dev,sizeof(vk::DrawIndexedIndirectCommand),{vk::BufferUsageFlagBits::eIndirectBuffer | vk::BufferUsageFlagBits::eTransferDst},{vk::MemoryPropertyFlagBits::eDeviceLocal}})
  {

  }

  void IndirectIndexedDrawCommand::Draw(vk::CommandBuffer &buffer){
    this->DrawInstanced(buffer,1,0);
  }

  void IndirectIndexedDrawCommand::DrawInstanced(vk::CommandBuffer &buffer, uint32_t instances, uint32_t offset){
    buffer.drawIndexedIndirect(this->commandSets,offset,instances,sizeof(vk::DrawIndirectCommand));
  }
}
