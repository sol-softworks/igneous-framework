// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "surface.h"

#include "texture.h"
#include "pipeline.h"

#include "miniLog.h"

namespace Igneous{

	//*grumble grumble*
	//Defined in resource translation unit
	extern vk::Instance inst;
	extern vk::AllocationCallbacks* allocators;

	//Surface Lifecycle Management
	WindowSurface::WindowSurface(Device &dev, std::function<void(void*,vk::SurfaceKHR&)> surfaceGenerator, void* genData = nullptr) : _dev(dev){
		ConsoleLog(LogType::INFO,"[VK] Generating Vulkan Surface\n");
		surfaceGenerator(genData,this->surface);

		//Initialize format and colorSpace
		//TODO: Make element configurable
		this->format = vk::Format::eB8G8R8A8Unorm;
		this->colorSpace = vk::ColorSpaceKHR::eSrgbNonlinear;

		//Capabilities verification
		this->capabilities = this->_dev.phys.getSurfaceCapabilitiesKHR(this->surface);
		//I promise this will ACTUALLY be used in the future.
		this->_dev.phys.getSurfaceSupportKHR(0,this->surface);
		auto formats = this->_dev.phys.getSurfaceFormatsKHR(this->surface);

		vk::SurfaceFormatKHR tmp;
		auto iter = std::find_if(formats.begin(),formats.end(),[&](const vk::SurfaceFormatKHR &fmt){return fmt.format == this->format;});
		if(iter == formats.end() && formats[0].format != vk::Format::eUndefined) tmp = formats[0];
		else tmp = *iter;
		this->format = tmp.format;

		this->swapchainSize = std::clamp<uint32_t>(this->capabilities.minImageCount+2,0,this->capabilities.maxImageCount);

		this->viewports.AddViewport({0,0},{0,0},{0,1});
		this->viewports.AddScissor({0,0},{0,0});

		ConsoleLog(LogType::DEBUG,"[VK] - Capabilities Dump:\n");
		ConsoleLog(LogType::DEBUG,"[VK]   + minImageCount: %u\n",static_cast<uint32_t>(this->capabilities.minImageCount));
		ConsoleLog(LogType::DEBUG,"[VK]   + maxImageCount: %u\n",static_cast<uint32_t>(this->capabilities.maxImageCount));
		ConsoleLog(LogType::DEBUG,"[VK] - Selected swapchain size: %u\n",static_cast<uint32_t>(this->swapchainSize));
	}

	WindowSurface::~WindowSurface(){
		ConsoleLog(LogType::INFO,"[VKRF] Destroying Renderer\n");
		ConsoleLog(LogType::INFO,"[VKRF] - Destroying Swapchain\n");
		static_cast<vk::Device>(this->_dev).destroySwapchainKHR(this->swapchain,Igneous::allocators);
		ConsoleLog(LogType::INFO,"[VKRF] - Destroying SurfaceKHR\n");
		Igneous::inst.destroySurfaceKHR(this->surface,Igneous::allocators);
		ConsoleLog(LogType::INFO,"[VKRF] Finished Destroying Renderer\n");
	}

	void WindowSurface::AddSurfaceAttachment(RenderPassGenerator &rpg){
		rpg.AddAttachment(
			"surface_final",
			this->format,
			1,
			{true,false,true},
			{false,false,false},
			vk::ImageLayout::eUndefined,
			vk::ImageLayout::ePresentSrcKHR
		);
	}

	void WindowSurface::ConfigureViewports(
		std::shared_ptr<GraphicsPipeline> &pipeline
	){
		pipeline->ConfigureViewport(this->viewports);
	}

	void WindowSurface::resizeFramebuffer(int width, int height){
		ConsoleLog(LogType::INFO,"[VK] - Resizing Framebuffers\n");
		VkSwapchainKHR oldSwap = this->swapchain;
		static_cast<vk::Device>(this->_dev).waitIdle();

		//Create New Swapchain
		//(this->capabilities.currentExtent.width == 0xffffffff) ? this->fbDims = vk::Extent2D(width,height) : this->fbDims = this->capabilities.currentExtent;
		this->fbDims = vk::Extent2D(width,height);
		this->viewports.RecreateViewport(0,{0,0},this->fbDims,{0,1});
		this->viewports.RecreateScissor(0,{0,0},this->fbDims);

		vk::SwapchainCreateInfoKHR info = {
				{},
				this->surface,
				this->swapchainSize,
				this->format,
				this->colorSpace,
				this->fbDims,
				1,
				vk::ImageUsageFlagBits::eColorAttachment,
				vk::SharingMode::eExclusive,
				0,
				nullptr,
				vk::SurfaceTransformFlagBitsKHR::eIdentity,
				vk::CompositeAlphaFlagBitsKHR::eOpaque,
				vk::PresentModeKHR::eFifo,
				1,
				oldSwap
			};
		this->swapchain = static_cast<vk::Device>(this->_dev).createSwapchainKHR(
			info,
			Igneous::allocators);
		{
			auto tmp = static_cast<vk::Device>(this->_dev).getSwapchainImagesKHR(this->swapchain);
			auto needsCreate = (this->backbuffer.size() == 0);
			if(needsCreate) this->backbuffer.reserve(tmp.size());
			size_t i = 0;
			for(auto &img : tmp){
				if(needsCreate){
					auto &elem = this->backbuffer.emplace_back(
						this->_dev,
						this->format,
						vk::Extent2D{
							static_cast<uint32_t>(width),
							static_cast<uint32_t>(height)
						},
						img
					);
					auto &rb = this->framebuffers.emplace_back(this->rpm);
					rb.SetAttachment("surface_final",elem.GetTexture(this->_dev)).
					SetClearValue("surface_final",vk::ClearColorValue(std::array<float,4>{0.4471f,0.5647f,0.6039f,0.5f})).
					SetRenderArea(static_cast<vk::Rect2D>(elem));
				}
				else{
					auto &elem = this->backbuffer[i].SetImage(
						img,
						vk::Extent2D{
							static_cast<uint32_t>(width),
							static_cast<uint32_t>(height)
						}
					);
					this->framebuffers[i].
						SetAttachment("surface_final",elem.GetTexture(this->_dev)).
						SetRenderArea(elem).
						Build(this->_dev);
					++i;
				}
			}
			auto test = this->framebuffers.at(0);
			test.SetClearValue("surface_final",vk::ClearColorValue(std::array<float,4>{0.4471f,0.5647f,0.6039f,0.5f}));
		}

		if(oldSwap) static_cast<vk::Device>(this->_dev).destroySwapchainKHR(oldSwap,Igneous::allocators);

		ConsoleLog(LogType::INFO,"[VK] Finished Resizing Framebuffers\n");
	}

	void WindowSurface::SetBufferAttachmentUniform(
		std::string id,
		BaseTexture &attachSource
	){
		for(auto &fb : this->framebuffers){
			fb.SetAttachment(id,attachSource);
		}
	}

	void WindowSurface::SetBufferAttachment(
		uint32_t bufferIndex,
		std::string id,
		BaseTexture &attachSource
	){
		this->framebuffers[bufferIndex].SetAttachment(id,attachSource);
	}

	void WindowSurface::SetAttachmentClearValueUniform(
		std::string id,
		vk::ClearValue value
	){
		for(auto &fb : this->framebuffers){
			fb.SetClearValue(id,value);
		}
	}

	void WindowSurface::SetAttachmentClearValue(
		uint32_t bufferIndex,
		std::string id,
		vk::ClearValue value
	){
		this->framebuffers[bufferIndex].SetClearValue(id,value);
	}

	void WindowSurface::SetRenderPass(std::shared_ptr<RenderPassMeta> rpmeta){
		this->rpm = rpmeta;
	}

	//Surface Operations
	std::optional<RenderBuffer> WindowSurface::Prepare(){
		try{
			this->activeSwapchainImage = static_cast<vk::Device>(this->_dev).acquireNextImageKHR(this->swapchain,UINT64_MAX,this->_dev.semaphore[0],nullptr).value;
			this->_dev.SynchronizeQueues();

			return this->framebuffers[this->activeSwapchainImage];
		}
		catch(vk::OutOfDateKHRError &e){
			ConsoleLog(LogType::INFO,"[VK] - Invalid swapchain! Has the framebuffer been resized?\n");
			return {};
		}
	}

	void WindowSurface::Present(){
		try{
			this->_dev.ExecuteTasks({vk::PipelineStageFlagBits::eColorAttachmentOutput},{this->_dev.semaphore[0]},{});
			this->_dev.taskList.PresentKHR({},this->swapchain,this->activeSwapchainImage);
		}
		catch(vk::OutOfDateKHRError &e){
			ConsoleLog(LogType::INFO,"[VK] - Invalid swapchain! Has the framebuffer been resized?\n");
		}
	}

	void WindowSurface::Synchronize(){
		static_cast<vk::Device>(this->_dev).waitIdle();
	}
}
