#Igneous Framework SConstruct Project File
import os
import multiprocessing
import project

env = Environment(	CXX = 'clang++',
					CXXFLAGS = [
						'-std=c++2a',
						#'-O3',
						'-Wno-undef'
						],
					CPPPATH = [
						'include',
						'extern',
						'extern/glm',
						'extern/assimp/include',
						'extern/assimp/build/include',
						'extern/glfw/include',
						'extern/spirv-cross'
						],
					LIBPATH = [
						'extern',
						'extern/assimp/build/code',
						'extern/glfw/build/src',
						'extern/spirv-cross'
						],
					RPATH = [
						'../../extern/glfw/build/src'
						]
				 )

class Project(object):

	def __init__(self,projName,major,minor,patch,build):
		self.name = projName
		self.maj = major
		self.min = minor
		self.pt = patch
		self.rev = build

	def SemVerStr(self):
		return '.'.join([str(self.maj),str(self.min),str(self.pt),str(self.rev)])

proj = Project(
	projName="Igneous Framework",
	major=project.major,
	minor=project.minor,
	patch=project.patch,
	build=project.build
	)

#Static Analysis Overrides
#
#   Used by scan-build for static scans
env["CC"] = os.getenv("CC") or env["CC"]
env["CXX"] = os.getenv("CXX") or env["CXX"]
env["ENV"].update(x for x in os.environ.items() if x[0].startswith("CCC_"))

if int(ARGUMENTS.get('debug',0)):
	env.Append(CXXFLAGS = ['-DDEBUG_BUILD','-g'])

#Determine set of enabled warnings
#
#Warnings are specified as a comma-delimited list
warnset = str(ARGUMENTS.get('warnings',''))
wlist = warnset.split(',')
for warn in wlist:
	if warn is not '':
		env.Append(CXXFLAGS = '-W'+warn)

#Configure default job count, based on available cpus / threads
#
#Overriden by -j or --jobs command line parameter
thread_count = multiprocessing.cpu_count()+1
SetOption('num_jobs',thread_count)

SConscript('lib.scons',exports='env',variant_dir='build/lib',duplicate=0)

if int(ARGUMENTS.get('examples',0)):
	env.Append(LIBPATH = ['build/lib'])
	env.Append(CPPPATH = ['examples','examples/shared'])
	SConscript('examples.scons',exports='env',variant_dir='build/examples',duplicate=0)


#Documentation Target
if int(ARGUMENTS.get('doc',0)):
	#docBuild = Environment(tools = ["default", "doxygen"])
	docBuild = Environment(tools = ["default", "doxyfile", "doxygen"])
	dfile = docBuild.Doxyfile(
		target="doxygen/Doxyfile",
		PROJECT_NUMBER=proj.SemVerStr()
	)
	"""
	... Doxygen builder is currently borked, as it's causing shlex to try and
	tokenize bytes, rather than full tokens. Considering how the scons-doxygen
	builder hasn't been updated since 2013, this will probably require some effort
	to resolve. Until then, we can at least work towards automating Doxyfile
	updates.

	External builds aren't using scons to generate documentation: this only
	impacts local documentation builds.
	"""
	#doc = docBuild.Doxygen(dfile)
	#doc = docBuild.Doxygen("doxygen/Doxyfile")
	#docBuild.NoClean(doc)
