// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "simple.h"

#include <array>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <cmath>

#include <typeinfo>

#include <chrono>

#include "miniLog.h"

#include "Obsidian/bindings.h"

#include "Conduit/shader.h"
#include "texture.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/trigonometric.hpp"
#include "glm/gtc/constants.hpp"
#include "glm/gtx/hash.hpp"

//AssImp mesh loading - To be abstracted
#include <assimp/Importer.hpp>
#include <assimp/DefaultLogger.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace SS{
	struct Vertex{
		glm::vec3 pos;
		uint32_t color;
		glm::vec2 uv;

		bool operator==(const Vertex &other) const{
			return this->pos == other.pos &&
			this->color == other.color &&
			this->uv == other.uv;
		}
	};
}

namespace std{
	template<> struct hash<SS::Vertex>{
		size_t operator()(SS::Vertex const& vertex) const{
			return ((hash<glm::vec3>()(vertex.pos) ^
			(hash<uint32_t>()(vertex.color) << 1)) >> 1) ^
			(hash<glm::vec2>()(vertex.uv) << 1);
		}
	};
}

namespace SS{

	static std::vector<Vertex> testVertices;
	static std::vector<uint32_t> testIndices;

	bool _configured = false;
	std::unique_ptr<Igneous::GraphicsPipelineConfiguration> _pipeConfig;

	uint32_t _configuredInstances = 0;

	Simple3D::Simple3D(){
		if(!_configured){
      Configure();
      _pipeConfig = CreateConfiguration();
      _pipeConfig->SetViewportConfiguration(1,1);

			_configured = true;
    }
		++_configuredInstances;

		auto _pipe = this->_pipeline_map.try_emplace("flatShade",std::make_shared<Igneous::GraphicsPipeline>(*this->dev)).first->second;

		//Configure RenderPassGenerator with renderer-specific attachments
		ConfigureRenderPass(this->_rpg);

		//Generate Render Pass instance
		this->_rpmeta = this->_rpg.Generate(*this->dev);

		//Assign render pass to surface for framebuffer creation
    this->_vkr->SetRenderPass(this->_rpmeta);
    _pipeConfig->rpm = this->_rpmeta;

		ConsoleLog(LogType::INFO,"[Sim3D] - Compiling Pipeline\n");
    _pipe->CompilePipeline(*_pipeConfig);

		//Create image resource sampler
		this->sampler = std::make_shared<Igneous::SamplerMeta>(
      Igneous::SamplerMeta::TilingConfig::ClampedUVW(),
      Igneous::SamplerMeta::AnisotropyConfig{1,16},
      Igneous::SamplerMeta::TexelFilter::Linear(),
      Igneous::SamplerMeta::LODConfig{-10,10}
    );

		//Create Obsidian Material instance
		this->cube_material = _pipe->CreateMaterial(0);

		//Load texture from file
		this->logo_img = logo_img = UI::LoadPNG("textures/logo_dev_A1.png");

		//Assign Material references
		this->cube_material->GetTexture("textures")->SetTexture(*logo_img,0);
		this->cube_material->GetTexture("samp")->SetSampler(sampler);
		this->cube_material->GetBuffer("UniformBuffer")->Create<UBO>(0);

		//AssImp mesh loading
		ConsoleLog(LogType::DEBUG,"[Sim3D] TESTING: Creating Importer\n");
		Assimp::Importer importer;
		//Remove Line / Point Primitives
		importer.SetPropertyInteger(AI_CONFIG_PP_SBP_REMOVE,aiPrimitiveType_POINT | aiPrimitiveType_LINE);
		Assimp::DefaultLogger::create("AssimpLog.txt",Assimp::Logger::NORMAL);
		ConsoleLog(LogType::DEBUG,"[Sim3D] - Importing test model\n");
		const aiScene *scene = importer.ReadFile("models/cube.blend",
			aiProcess_CalcTangentSpace |
			aiProcess_Triangulate |
			aiProcess_JoinIdenticalVertices |
			aiProcess_FindDegenerates |
			aiProcess_FixInfacingNormals |
			aiProcess_SortByPType |
			aiProcess_DropNormals |
			aiProcess_OptimizeMeshes |
			aiProcess_OptimizeGraph
		);

		Assimp::DefaultLogger::kill();
		ConsoleLog(LogType::DEBUG,"[Sim3D] - File Imported\n");

		auto root = scene->mRootNode;
		ConsoleLog(LogType::DEBUG,"[Sim3D] - Children: %d\n",root->mNumChildren);
		ConsoleLog(LogType::DEBUG,"[Sim3D] - Texture Count: %d\n",scene->mNumTextures);
		ConsoleLog(LogType::DEBUG,"[Sim3D] - Material Count: %d\n",scene->mNumMaterials);
		//TESTING: TO BE REMOVED
		for(uint32_t i=0; i<scene->mNumMeshes; ++i){
			ConsoleLog(LogType::DEBUG,"[Sim3D] - Loading Mesh %d\n",i);
			uint32_t meshID = i;
			uint32_t dupes = 0;
			ConsoleLog(LogType::DEBUG,"[Sim3D] +   Mesh %d Vertices: %d\n",meshID,scene->mMeshes[meshID]->mNumVertices);
			//Indices are relative to the mesh, so we need to
			//		add an offset (current number of vertices) when
			//		pushing indices
			auto offset = static_cast<uint32_t>(testVertices.size());
			for(uint32_t j=0; j<scene->mMeshes[meshID]->mNumVertices; ++j){
				auto vert = scene->mMeshes[meshID]->mVertices[j];
				auto pos = glm::vec3{vert.x,vert.y,vert.z};
				uint32_t _col = 0xFF0000FF;
				auto _uv = glm::vec2{0,0};
				if(scene->mMeshes[meshID]->HasVertexColors(0)){
					auto col = scene->mMeshes[meshID]->mColors[0];
					_col = (uint32_t)(255*col[j].r) << 24 |
								(uint32_t)(255*col[j].g) << 16 |
								(uint32_t)(255*col[j].b) << 8 |
								(uint32_t)(255*col[j].a);
				}
				if(scene->mMeshes[meshID]->HasTextureCoords(0)){
					auto uv = scene->mMeshes[meshID]->mTextureCoords[0];
					_uv = glm::vec2{uv[j].x,uv[j].y};
				}
				auto v = Vertex{pos,_col,_uv};
				testVertices.push_back(v);
			}
			ConsoleLog(LogType::DEBUG,"[Sim3D] +   %d Duplicate Vertices Discarded\n",dupes);

			ConsoleLog(LogType::DEBUG,"[Sim3D] +   Current Index Offset: %d\n",
				offset);
			for(uint32_t j=0; j<scene->mMeshes[meshID]->mNumFaces; ++j){
				if(scene->mMeshes[meshID]->mFaces[j].mNumIndices != 3){
					ConsoleLog(LogType::DEBUG,"[Sim3D] *     Degenerate Primitive Detected! Ignoring.\n");
					continue;
				}
				for(uint32_t k=0; k<scene->mMeshes[meshID]->mFaces[j].mNumIndices; ++k){
					if(j == 0 && k == 0){
						ConsoleLog(LogType::DEBUG,"[Sim3D] +   First Index: %d\n",
						scene->mMeshes[meshID]->mFaces[j].mIndices[k]);
					}
					testIndices.push_back(
						offset + scene->mMeshes[meshID]->mFaces[j].mIndices[k]
					);
				}
			}
		}

		ConsoleLog(LogType::DEBUG,"[Sim3D] - Final Vertex Count: %d\n",
			static_cast<uint32_t>(testVertices.size()));
		ConsoleLog(LogType::DEBUG,"[Sim3D] - Final Index Count: %d\n",
			static_cast<uint32_t>(testIndices.size()));

		//Generate pipeline data resources
		this->VBO = std::make_unique<Igneous::VulkanBuffer>(
			*this->dev,
			vk::DeviceSize{1},
			vk::BufferUsageFlagBits::eVertexBuffer,
			vk::MemoryPropertyFlagBits::eHostVisible
		);
    this->IBO = std::make_unique<Igneous::VulkanBuffer>(
			*this->dev,
			vk::DeviceSize{1},
			vk::BufferUsageFlagBits::eIndexBuffer,
			vk::MemoryPropertyFlagBits::eHostVisible
		);
		this->VBO->Assign(testVertices);
		this->IBO->Assign(testIndices);

		//Generate / configure depth texture resource
		ConsoleLog(LogType::INFO,"[Sim3D] Configuring Depth Texture\n");
		this->depthTexture = Igneous::Texture<2>::Create(
			"Depth Texture",
			{
				static_cast<uint32_t>(this->window.width),
				static_cast<uint32_t>(this->window.height)
			},
			1,1);
		this->depthTexture->SetFormat(vk::Format::eD32SfloatS8Uint);
		this->depthTexture->SetResources({vk::ImageAspectFlagBits::eDepth,0,1,0,1});
		this->depthTexture->SetUsageFlags(vk::ImageUsageFlagBits::eDepthStencilAttachment);
		//this->depthTexture->SetSize({static_cast<uint32_t>(this->window.width),static_cast<uint32_t>(this->window.height)});

		//Transfer depth texture into appropriate internal layout
		auto taskPtr = this->dev->StartTask();
		if(auto task = taskPtr.lock()){
			auto buf = static_cast<vk::CommandBuffer>(*task);
			this->depthTexture->SetLayout(
				buf,
				vk::ImageLayout::eDepthStencilAttachmentOptimal
			);
			task->Submit();
		}

		//Execute depth texture task
		this->dev->ExecuteTasks({},{},{});

		//this->_vkr->SetAttachmentClearValueUniform("depth",vk::ClearDepthStencilValue{1.0f,0});

		ConsoleLog(LogType::INFO,"[Sim3D] Finished Initialization\n");
	}

	Simple3D::~Simple3D(){
		if(--_configuredInstances == 0){
			_configured = false;
			_pipeConfig.reset();
		}
	}

	void Simple3D::framebufferResizeCallback(
    uint32_t width,
    uint32_t height
  ){
		//Resize depth texture to fit full viewport dimensions
		this->depthTexture->SetSize({width,height});
		//Rebuild depth texture, and retrieve reference
		auto &dt = this->depthTexture->GetTexture(*this->dev);
		//Assign rebuilt texture to all framebuffers
		this->_vkr->SetBufferAttachmentUniform(
			"depth",
			dt
		);

		//WORKAROUND: Setting Uniform only works once the framebuffer attachments have
		//						been fully configured.
		//Set depth stencil clear value for all framebuffers
		this->_vkr->SetAttachmentClearValueUniform("depth",vk::ClearDepthStencilValue{1.0f,0});
	}

	void Simple3D::ConfigureDraws(){
		//Push MVP
		static auto time = std::chrono::high_resolution_clock::now();
		auto frameTime = std::chrono::high_resolution_clock::now();
		float delta = std::chrono::duration<float,std::chrono::seconds::period>(frameTime-time).count();
		if(delta > 32){
			time = frameTime;
			delta -= 32;
		}
		SS::UBO plane;
		auto r = 5;
		auto st = 0.0f;
		auto ct = 0.0f;
		auto th = delta*22.5f;
		//if(th >= 180.0f){th = 360.0f - th;}
		if(th >= 180.0f){th -= 360.0f;}
		st = glm::sin(glm::radians(th)+glm::pi<float>());
		ct = glm::cos(glm::radians(th)+glm::pi<float>());
		auto orbit = glm::vec3{r*st,0,r*ct};
		//auto orbit = glm::vec3{r,0,3};

		//Create Model / View / Projection Matrices
		plane.view = glm::lookAt(orbit,glm::vec3(0,0,0),glm::vec3(0,0,1));
		plane.model = glm::rotate(glm::mat4(1),glm::radians(45.0f),glm::vec3(0,0,1));
		plane.proj = glm::perspective(glm::radians(90.0f),1920.0f/1080.0f,0.1f,20.0f);
		plane.proj[1][1] *= -1;

		//Assign MVP Matrices to material uniform buffer
		this->cube_material->properties["UniformBuffer"]->as<Igneous::BufferProperty>()->Assign(plane);
	}

	void Simple3D::Render(Igneous::RenderBuffer &frame){
		auto pipeline = dynamic_pointer_cast<Igneous::GraphicsPipeline>(this->_pipeline_map["flatShade"]);

		//Indicate start of render pass
    pipeline->FrameStart(frame);

    //Configure rendering viewport / scissors
    this->_vkr->ConfigureViewports(pipeline);

		//Assign material for resource access in shaders
		pipeline->SetMaterials({this->cube_material});

		//Assign VBO / IBO for mesh rendering
    pipeline->AssignBuffers(*this->VBO,*this->IBO,_pipeConfig.get());

		//Assign push constant values
		auto imgIndex = pipeline->GetPushConstant<int32_t>("imgIndex");
		imgIndex = 0;
		imgIndex.Commit();

		//Generate / Execute draw command
		Igneous::IndexedDrawCommand cmd(this->IBO->size/4,0,0);
		pipeline->RecordDraw(cmd);

		//Indicate end of render pass
		pipeline->FrameEnd();
	}

	void Simple3D::Configure(){
		//Load shader SPIR-V bytecode into shared memory
		Igneous::Shader::Define("GLSL/simple.vert.spv");
		Igneous::Shader::Define("GLSL/simple.frag.spv");
	}

	void Simple3D::ConfigureRenderPass(Igneous::RenderPassGenerator &rpgen){
		rpgen.AddAttachment(
			"depth",
			vk::Format::eD32SfloatS8Uint,
			1,
			{true,false,false},
			{false,false,false},
			vk::ImageLayout::eUndefined,
			vk::ImageLayout::eDepthStencilAttachmentOptimal
		);

		rpgen.AddSubpass(
			{},
			{"surface_final"},
			{},
			{},
			true,
			"depth"
		);
	}

	std::unique_ptr<Igneous::GraphicsPipelineConfiguration> Simple3D::CreateConfiguration(){
		auto config = std::make_unique<Igneous::GraphicsPipelineConfiguration>();
		config->vertexInput.AddBinding<Vertex>();
		config->vertexInput.SetAttributes<glm::vec3,uint32_t,glm::vec2>(0,{
			vk::Format::eR32G32B32Sfloat,
			vk::Format::eR8G8B8A8Unorm,
			vk::Format::eR32G32Sfloat
		});

		//Pipeline State Configuration
		config->SetPrimitive<Igneous::Triangle>(Igneous::Triangle::Ordering::List);
		config->depthStencil.depthTestEnable = true;
		config->depthStencil.depthWriteEnable = true;
		config->depthStencil.depthCompareOp = vk::CompareOp::eLess;
		config->rasterization.cullBackFace = true;
		config->rasterization.frontFaceIsCCW = true;
		config->rasterization.lineWidth = 1;
		config->rasterization.sampleCount = vk::SampleCountFlagBits::e1;

		config->index16 = false;

		//Dynamic State Configuration
		config->dynamicStates.AddDynamicState(vk::DynamicState::eViewport);
		config->dynamicStates.AddDynamicState(vk::DynamicState::eScissor);

		//Color Blending Configuration
		config->colorBlend.AddAttachment(
			1,
			vk::BlendFactor::eSrcAlpha, vk::BlendFactor::eOneMinusSrcAlpha, vk::BlendOp::eAdd,
			vk::BlendFactor::eOneMinusSrcAlpha, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
			{vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA}
		);

		config->stages = {{"simple.vert","main"},{"simple.frag","main"}};

		return config;
	}
}
