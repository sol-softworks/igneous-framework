// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
///		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Simple3D Renderer
//
//	Description: Demonstration Vulkan Renderer for 3D meshes. Used to demonstrate
//				loading of meshes, and application of textures to external models.

#ifndef SS_SIMPLE3D
#define SS_SIMPLE3D

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Pipelines/graphicsPipeline.h"

#include "vulkan/vulkan.hpp"

#include "mainwindow.h"

namespace SS{

	struct UBO{
		glm::mat4 model;
		glm::mat4 view;
		glm::mat4 proj;
	};

	class Simple3D : public UI::MainWindow{
	private:
		std::shared_ptr<Igneous::Texture<2>> logo_img;
		std::shared_ptr<Igneous::SamplerMeta> sampler;

		std::shared_ptr<Igneous::Texture<2>> depthTexture;

		std::shared_ptr<Igneous::Material> cube_material;

	protected:
		virtual void framebufferResizeCallback(uint32_t width, uint32_t height) override;

	public:
		Simple3D();
		~Simple3D();

		virtual void ConfigureDraws() override;
		virtual void Render(Igneous::RenderBuffer &buffer) override;

		static void Configure();
		static void ConfigureRenderPass(Igneous::RenderPassGenerator &rpgen);
		static std::unique_ptr<Igneous::GraphicsPipelineConfiguration> CreateConfiguration();
	};
}

#endif
