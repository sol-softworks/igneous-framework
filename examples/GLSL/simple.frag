#version 450 core
layout(location = 0) out vec4 fColor;

layout(set=0, binding=1) uniform sampler samp;
layout(set=0, binding=2) uniform texture2D textures[8];

layout(location = 0) in struct{
    vec4 Color;
    vec2 UV;
} In;

layout(push_constant) uniform PER_OBJECT{
	layout(offset = 0) int imgIndex;
} pc;

void main()
{
    fColor = In.Color * texture(sampler2D(textures[pc.imgIndex],samp), In.UV.st);
    //fColor = In.Color;
    //fColor = vec4(In.UV.st,0,1);
}
