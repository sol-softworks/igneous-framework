#version 450 core
layout(location = 0) out vec4 fColor;

layout(set=0, binding=0) uniform sampler2D samp;

layout(location = 0) in struct{
    vec4 Color;
    vec2 UV;
} In;

void main()
{
    fColor = texture(samp, In.UV.st);
}
