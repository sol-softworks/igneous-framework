#version 450 core
layout(set = 0, binding = 0) uniform UniformBuffer{
	mat4 model;
	mat4 view;
	mat4 proj;
} ubo;

layout(location = 0) in vec3 aPos;
layout(location = 1) in vec4 aColor;
layout(location = 2) in vec2 aUV;

out gl_PerVertex{
    vec4 gl_Position;
};

layout(location = 0) out struct{
    vec4 Color;
    vec2 UV;
} Out;

void main()
{
    Out.Color = aColor;
    Out.UV = aUV;
    gl_Position = ubo.proj*ubo.view*ubo.model*vec4(aPos, 1);
    //gl_Position = vec4(aPos, 1);
}
