#!/bin/bash
#
# Shader Compilation utility script

#Simple3D Shaders
glslangValidator -o simple.frag.spv -V simple.frag
glslangValidator -o simple.vert.spv -V simple.vert

#Screenspace UI Shaders
glslangValidator -o ssui.frag.spv -V ssui.frag
glslangValidator -o ssui.vert.spv -V ssui.vert

#Texture Shaders
glslangValidator -o texture.frag.spv -V texture.frag
glslangValidator -o texture.vert.spv -V texture.vert

