#version 450 core
layout(location = 0) out vec4 fColor;

layout(set=0, binding=0) uniform sampler samp;
layout(set=0, binding=1) uniform texture2D textures[3];

layout(push_constant) uniform PER_OBJECT
{
    layout(offset = 16) int imgIndex;
}pc;

layout(location = 0) in struct{
    vec4 Color;
    vec2 UV;
} In;

void main()
{
    fColor = In.Color * texture(sampler2D(textures[pc.imgIndex],samp), In.UV.st);
}
