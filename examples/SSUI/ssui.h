// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
///		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: SSUI Renderer
//
//	Description: Demonstration screenspace user interface renderer based on ImGui.
//				 Highly minimalistic implementation that simply provides an interface
//				 between ImGui's draw calls and the Igneous infrastructure.
//
//				 Draw operations derived from ImGui reference Vulkan pipeline.

#ifndef SS_IMGUI_PIPELINE
#define SS_IMGUI_PIPELINE

#include <vector>
#include <utility>

#include "vulkan/vulkan.hpp"

#include "resource.h"
#include "Pipelines/graphicsPipeline.h"

#include "mainwindow.h"

namespace SS::UI{

	//TESTING: Unified pipeline for text and images
	class ScreenspaceRenderer : public MainWindow{
	private:
		std::shared_ptr<Igneous::Texture<2>> logo_img;
		std::shared_ptr<Igneous::Texture<2>> banner_img;
		std::shared_ptr<Igneous::Texture<2>> font_img;
		std::shared_ptr<Igneous::SamplerMeta> sampler;

		Igneous::util::dynarray<float> uScale;
		Igneous::util::dynarray<float> uTrans;

		std::shared_ptr<Igneous::Material> ssui_material;

	protected:
		virtual void framebufferResizeCallback(uint32_t width, uint32_t height) override;

	public:
		ScreenspaceRenderer();
		~ScreenspaceRenderer();

		virtual void ConfigureDraws() override;
		virtual void Render(Igneous::RenderBuffer &buffer) override;
		virtual void EndFrame() override;

		static void Configure();
		static void ConfigureRenderPass(Igneous::RenderPassGenerator &rpgen);
		static std::unique_ptr<Igneous::GraphicsPipelineConfiguration> CreateConfiguration();
	};
}

#endif
