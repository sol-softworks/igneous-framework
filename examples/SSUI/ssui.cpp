// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "ssui.h"

#include "Obsidian/bindings.h"

#include "Conduit/shader.h"

#include "imgui/imgui.h"

#include "miniLog.h"

namespace SS::UI{

	bool _configured;
	std::unique_ptr<Igneous::GraphicsPipelineConfiguration> _pipeConfig;

	uint32_t _configuredInstances = 0;

	ScreenspaceRenderer::ScreenspaceRenderer() :
		uScale(2),
		uTrans(2)
	{
		if(!_configured){
      Configure();
      _pipeConfig = CreateConfiguration();
      _pipeConfig->SetViewportConfiguration(1,1);

			//Configure RenderPassGenerator with renderer-specific attachments
      ConfigureRenderPass(this->_rpg);
    }

		++_configuredInstances;

		auto _pipe = this->_pipeline_map.try_emplace("flatShade",std::make_shared<Igneous::GraphicsPipeline>(*this->dev)).first->second;

    //Generate Render Pass instance
		this->_rpmeta = this->_rpg.Generate(*this->dev);

		//Assign render pass to surface for framebuffer creation
    this->_vkr->SetRenderPass(this->_rpmeta);
    _pipeConfig->rpm = this->_rpmeta;

		ConsoleLog(LogType::INFO,"[SSUI] - Compiling Pipeline\n");
    _pipe->CompilePipeline(*_pipeConfig);

		//Create image resource sampler
		this->sampler = std::make_shared<Igneous::SamplerMeta>(
      Igneous::SamplerMeta::TilingConfig::ClampedUVW(),
      Igneous::SamplerMeta::AnisotropyConfig{1,16},
      Igneous::SamplerMeta::TexelFilter::Linear(),
      Igneous::SamplerMeta::LODConfig{-10,10}
    );

		//Create Obsidian Material instance
		this->ssui_material = _pipe->CreateMaterial(0);

		//Load texture from file
		ConsoleLog(LogType::INFO,"[SSUI] Instantiating Test Images\n");
		this->banner_img = LoadPNG("textures/banner_dev_A1.png");
		this->logo_img = logo_img = LoadPNG("textures/logo_dev_A1.png");

		ConsoleLog(LogType::INFO,"[SSUI] Initializing ImGui Context\n");
		ImGui::CreateContext();

		ConsoleLog(LogType::INFO,"[SSUI] Instantiating Font Texture\n");
		ImGuiIO& io = ImGui::GetIO();
		//Load texture from internal font. This uses a different process from LoadPNG,
		//		as it uses a texture internal to ImGui.
		//std::vector<unsigned char*> pixels;
		unsigned char* pixels;
		std::array<int,2> fontDims;
		io.Fonts->GetTexDataAsRGBA32(&pixels, &fontDims[0], &fontDims[1]);
		size_t upload_size = fontDims[0]*fontDims[1];
		std::vector<uint32_t> _p;
		_p.reserve(upload_size);
		for(int i=0; i<4*upload_size; i+=4){
			//pixels is an array of u_char*, so each element must
			//		be combined into a single uint32_t
			//
			//Pixels are retrieved as RGBA, but when converted into a
			//		uint32_t, the order must be converted to ABGR
			//Note that only the alpha changes in the default ImGui Font Texture
			auto p = uint32_t{
				static_cast<uint32_t>(pixels[i+3]) << 24 |
				static_cast<uint32_t>(pixels[i+2]) << 16 |
				static_cast<uint32_t>(pixels[i+1]) << 8 |
				static_cast<uint32_t>(pixels[i])
			};
			_p.push_back(p);
		}
		std::array<uint32_t,2> fdims = {static_cast<uint32_t>(fontDims[0]),static_cast<uint32_t>(fontDims[1])};

		//Create texture from font
		this->font_img = Igneous::Texture2D::Create("font",fdims,1,1);
		this->font_img->SetPixels(_p);
		this->font_img->SetUsageFlags({vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled});

		//Assign Material references
		auto tex = this->ssui_material->GetTexture("textures");
		tex->SetTexture(*font_img,0);
		tex->SetTexture(*logo_img,1);
		tex->SetTexture(*banner_img,2);
		this->ssui_material->GetTexture("samp")->SetSampler(sampler);
		io.Fonts->TexID = (ImTextureID)(0);

		//Generate pipeline data resources
		this->VBO = std::make_unique<Igneous::VulkanBuffer>(*this->dev,vk::DeviceSize{1},vk::BufferUsageFlagBits::eVertexBuffer,vk::MemoryPropertyFlagBits::eHostVisible);
    this->IBO = std::make_unique<Igneous::VulkanBuffer>(*this->dev,vk::DeviceSize{1},vk::BufferUsageFlagBits::eIndexBuffer,vk::MemoryPropertyFlagBits::eHostVisible);
	}

	ScreenspaceRenderer::~ScreenspaceRenderer(){
		if(--_configuredInstances == 0){
			_configured = false;
			_pipeConfig.reset();
		}
		ImGui::DestroyContext();
	}

	void ScreenspaceRenderer::framebufferResizeCallback(
		uint32_t width,
		uint32_t height
	){
		//ImDrawData* draw_data = ImGui::GetDrawData();
		//Scale / Translation Update
		//ImVec2 display_pos = draw_data->DisplayPos;
		uScale[0] = 2.0f/width;
		uScale[1] = 2.0f/height;
		//xform[2] = -1.0f-display_pos.x*xform[0];
		//xform[3] = -1.0f-display_pos.y*xform[1];
		uTrans[0] = -1.0f;
		uTrans[1] = -1.0f;
		ImGui::GetIO().DisplaySize = {static_cast<float>(width),static_cast<float>(height)};
	}

	void ScreenspaceRenderer::ConfigureDraws(){
		//Indicate to ImGui that a new frame should start
		ImGui::NewFrame();
		ImGuiIO& io = ImGui::GetIO();
		io.MousePos = {static_cast<float>(this->mousePos[0]),static_cast<float>(this->mousePos[1])};
		//Generate meshes
		{
			//Lower window, containing "debug" information and IF banner
			ImGui::SetNextWindowSize(ImVec2(this->window.width,(0.1f*this->window.height)));
			ImGui::SetNextWindowPos(ImVec2(0,(0.9f*this->window.height)));
			ImGui::Begin("Overlay",nullptr,ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
			ImGui::BeginChild("Overlay Text",ImVec2(0.4f*this->window.width,this->window.height),false);
			//"Debug" information
			ImGui::Text("Overlay Test Text");
			ImGui::Text("Mouse Position: <%.4f,%.4f>",io.MousePos.x,io.MousePos.y);
			ImGui::Text("Framerate: %.4f",io.Framerate);
			ImGui::EndChild();
			ImGui::SameLine();
			//IF Banner
			ImGui::Image((ImTextureID)(2),ImVec2(512,100));
			if(ImGui::IsItemHovered()){
				//Tooltip when cursor over banner
				ImGui::BeginTooltip();
				ImGui::Text("Tooltip Test");
				ImGui::Image((ImTextureID)(1),ImVec2(64,64));
				ImGui::EndTooltip();
			}
			ImGui::End();
		}
		//Render mesh data
		ImGui::Render();

		//Modify pipeline data resources (VBO / IBO)
		ImDrawData* draw_data = ImGui::GetDrawData();
		std::vector<ImDrawVert> vtx_dst = {};
		std::vector<ImDrawIdx> idx_dst = {};
		for (int n=0; n<draw_data->CmdListsCount; ++n){
			const ImDrawList* cmd_list = draw_data->CmdLists[n];
			vtx_dst.insert(vtx_dst.end(),(cmd_list->VtxBuffer.Data),(cmd_list->VtxBuffer.Data+cmd_list->VtxBuffer.Size));
			idx_dst.insert(idx_dst.end(),(cmd_list->IdxBuffer.Data),(cmd_list->IdxBuffer.Data+cmd_list->IdxBuffer.Size));
		}
		this->VBO->Assign(vtx_dst);
		this->IBO->Assign(idx_dst);
	}

	void ScreenspaceRenderer::Render(Igneous::RenderBuffer &frame){
		auto pipeline = dynamic_pointer_cast<Igneous::GraphicsPipeline>(this->_pipeline_map["flatShade"]);

		//Indicate start of render pass
		pipeline->FrameStart(frame);

		//Configure rendering viewport / scissors
		this->_vkr->ConfigureViewports(pipeline);

    //Assign material for resource access in shaders
		pipeline->SetMaterials({this->ssui_material});

		//Assign VBO / IBO for mesh rendering
    pipeline->AssignBuffers(*this->VBO,*this->IBO,_pipeConfig.get());

		//Create reusable resources for draw command generation
		int32_t vtx_offset = 0;
		uint32_t idx_offset = 0;
		Igneous::IndexedDrawCommand cmd{0,0,0};
		ImDrawData* draw_data = ImGui::GetDrawData();

		//Assign scale / translation push constant values
		auto pcus = pipeline->GetPushConstant<Igneous::util::dynarray<float>>("uScale");
		auto pcut = pipeline->GetPushConstant<Igneous::util::dynarray<float>>("uTranslate");
		auto imgIndex = pipeline->GetPushConstant<int>("imgIndex");
		pcus = uScale;
		pcut = uTrans;
		pcus.Commit();
		pcut.Commit();

		for (int n=0; n<draw_data->CmdListsCount; ++n){
			//ConsoleLog(LogType::DEBUG,"[SSUI] Draw Command: %d\n",n);
			const ImDrawList* cmd_list = draw_data->CmdLists[n];
			for (int cmd_i=0; cmd_i<cmd_list->CmdBuffer.Size; ++cmd_i){
				const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
				//If set, assign the TextureId from pcmd as the active texture.
				//	We pass an integral index to the UI shader, because AZDO
				//
				//	(Sure. That's why.)

				//Since the stored ID is a numerical ID, there's no need to check
				//		against nullptr. In fact, it was breaking font rendering.
				//		Don't do that.
				imgIndex = static_cast<int>(reinterpret_cast<intptr_t>(pcmd->TextureId));
				imgIndex.Commit();
				//ConsoleLog(LogType::DEBUG,"[SSUI] - Image Index: %d\n",imgIndex.get());
				// Apply scissor/clipping rectangle
				// FIXME: We could clamp width/height based on clamped min/max values.
				/*
				vk::Rect2D scissor;
				scissor.offset.x = (int32_t)(pcmd->ClipRect.x - display_pos.x) > 0 ? (int32_t)(pcmd->ClipRect.x - display_pos.x) : 0;
				scissor.offset.y = (int32_t)(pcmd->ClipRect.y - display_pos.y) > 0 ? (int32_t)(pcmd->ClipRect.y - display_pos.y) : 0;
				scissor.extent.width = (uint32_t)(pcmd->ClipRect.z - pcmd->ClipRect.x);
				scissor.extent.height = (uint32_t)(pcmd->ClipRect.w - pcmd->ClipRect.y + 1); // FIXME: Why +1 here?
				buffer.setScissor(
					0,1,&scissor);
				*/

				// Draw
				/*
				ConsoleLog(LogType::DEBUG,"[SSUI] - Draw Statistics:\n");
				ConsoleLog(LogType::DEBUG,"[SSUI]   + Index Count: %d\n",pcmd->ElemCount);
				ConsoleLog(LogType::DEBUG,"[SSUI]   + Index Offset: %d\n",idx_offset);
				ConsoleLog(LogType::DEBUG,"[SSUI]   + Vertex Offset: %d\n",vtx_offset);
				*/

				//Generate / Execute draw command
				cmd = Igneous::IndexedDrawCommand{pcmd->ElemCount,vtx_offset,idx_offset};
				pipeline->RecordDraw(cmd);

				//Add number of rendered indices to IBO offset for next mesh
				idx_offset += pcmd->ElemCount;
			}
			//Add number of rendered vertices to VBO offset for next mesh
			vtx_offset += cmd_list->VtxBuffer.Size;
		}

		//Indicate end of render pass
		pipeline->FrameEnd();
	}

	void ScreenspaceRenderer::EndFrame(){
		//Indicate to ImGui that the current frame has ended
		ImGui::EndFrame();
	}

	void ScreenspaceRenderer::Configure(){
		//Load shader SPIR-V bytecode into shared memory
		Igneous::Shader::Define("GLSL/ssui.vert.spv");
		Igneous::Shader::Define("GLSL/ssui.frag.spv");
	}

	void ScreenspaceRenderer::ConfigureRenderPass(Igneous::RenderPassGenerator &rpgen){
		rpgen.AddSubpass(
			{},
			{"surface_final"},
			{},
			{},
			false,
			""
		);
	}

	std::unique_ptr<Igneous::GraphicsPipelineConfiguration> ScreenspaceRenderer::CreateConfiguration(){
		auto config = std::make_unique<Igneous::GraphicsPipelineConfiguration>();
		config->vertexInput.AddBinding<ImDrawVert>();
		config->vertexInput.SetAttributes<ImVec2,ImVec2,ImU32>(0,{
			vk::Format::eR32G32Sfloat,
			vk::Format::eR32G32Sfloat,
			vk::Format::eR8G8B8A8Unorm
		});

		//Pipeline State Configuration
		config->SetPrimitive<Igneous::Triangle>(Igneous::Triangle::Ordering::List);
		config->rasterization.mode = vk::PolygonMode::eFill;
		config->rasterization.frontFaceIsCCW = true;
		config->rasterization.lineWidth = 1;
		config->rasterization.sampleCount = vk::SampleCountFlagBits::e1;

		config->index16 = true;

		//Dynamic State Configuration
		config->dynamicStates.AddDynamicState(vk::DynamicState::eViewport);
		config->dynamicStates.AddDynamicState(vk::DynamicState::eScissor);

		//Color Blending Configuration
		config->colorBlend.AddAttachment(
			1,
			vk::BlendFactor::eSrcAlpha, vk::BlendFactor::eOneMinusSrcAlpha, vk::BlendOp::eAdd,
			vk::BlendFactor::eOneMinusSrcAlpha, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
			{vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA}
		);

		config->stages = {{"ssui.vert","main"},{"ssui.frag","main"}};
		return config;
	}
}
