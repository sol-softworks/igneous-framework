// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
///		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Textured Renderer
//
//	Description: Demonstration single-texture renderer. Mostly used to verify
//         the Igneous Framework's texture system.

#ifndef SS_TEXTURED_PIPELINE
#define SS_TEXTURED_PIPELINE

#include <vector>
#include <utility>

#include "vulkan/vulkan.hpp"

#include "resource.h"
#include "Pipelines/graphicsPipeline.h"

#include "mainwindow.h"

namespace SS::UI{

	//TESTING: Unified pipeline for text and images
	class TextureRenderer : public UI::MainWindow{
	private:
		std::shared_ptr<Igneous::Texture<2>> logo_img;
		std::shared_ptr<Igneous::SamplerMeta> sampler;

		std::shared_ptr<Igneous::Material> plane_material;

	protected:
		virtual void framebufferResizeCallback(uint32_t width, uint32_t height) override;

	public:
		TextureRenderer();
		~TextureRenderer();

		virtual void ConfigureDraws() override;
		virtual void Render(Igneous::RenderBuffer &buffer) override;

		static void Configure();
		static void ConfigureRenderPass(Igneous::RenderPassGenerator &rpgen);
		static std::unique_ptr<Igneous::GraphicsPipelineConfiguration> CreateConfiguration();
	};
}

#endif
