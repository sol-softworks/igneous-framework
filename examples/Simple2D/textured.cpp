// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "textured.h"

#include "Obsidian/bindings.h"

#include "Conduit/shader.h"

#include "miniLog.h"

namespace SS::UI{

  struct DrawVert{
    std::array<float,2> position;
    std::array<float,2> coord;
    uint32_t color;
  };

  const std::vector<DrawVert> testBox = {
    DrawVert{{-0.5f,-0.5f},{1,0},0xFF0000FF},
    {{0.5f,-0.5f},{0,0},0xFF0000FF},
    {{0.5f,0.5f},{0,1},0xFF0000FF},
    {{-0.5f,0.5f},{1,1},0xFF0000FF}
  };

  const std::vector<uint16_t> testIndices = {
    0,1,2,2,3,0
  };

  bool _configured;
  std::unique_ptr<Igneous::GraphicsPipelineConfiguration> _pipeConfig;

  uint32_t _configuredInstances = 0;

	TextureRenderer::TextureRenderer(){
    if(!_configured){
      Configure();
      _pipeConfig = CreateConfiguration();
      _pipeConfig->SetViewportConfiguration(1,1);

      //Configure RenderPassGenerator with renderer-specific attachments
      ConfigureRenderPass(this->_rpg);
    }
    ++_configuredInstances;

    auto _pipe = this->_pipeline_map.try_emplace("flatShade",std::make_shared<Igneous::GraphicsPipeline>(*this->dev)).first->second;

    //Generate Render Pass instance
    this->_rpmeta = this->_rpg.Generate(*this->dev);

    //Assign render pass to surface for framebuffer creation
    this->_vkr->SetRenderPass(this->_rpmeta);
    _pipeConfig->rpm = this->_rpmeta;

    ConsoleLog(LogType::INFO,"[Textured] - Compiling Pipeline\n");
    _pipe->CompilePipeline(*_pipeConfig);

    //Create image resource sampler
    this->sampler = std::make_shared<Igneous::SamplerMeta>(
      Igneous::SamplerMeta::TilingConfig::ClampedUVW(),
      Igneous::SamplerMeta::AnisotropyConfig{1,16},
      Igneous::SamplerMeta::TexelFilter::Linear(),
      Igneous::SamplerMeta::LODConfig{-10,10}
    );

    //Create Obsidian Material instance
    this->plane_material = _pipe->CreateMaterial(0);

    //Load texture from file
    this->logo_img = logo_img = LoadPNG("textures/logo_dev_A1.png");

    //Assign Material references
    auto tex = this->plane_material->GetTexture("samp");
    tex->SetTexture(*logo_img);
    tex->SetSampler(sampler);

    //Generate pipeline data resources
    this->VBO = std::make_unique<Igneous::VulkanBuffer>(*this->dev,vk::DeviceSize{1},vk::BufferUsageFlagBits::eVertexBuffer,vk::MemoryPropertyFlagBits::eHostVisible);
    this->IBO = std::make_unique<Igneous::VulkanBuffer>(*this->dev,vk::DeviceSize{1},vk::BufferUsageFlagBits::eIndexBuffer,vk::MemoryPropertyFlagBits::eHostVisible);
    this->VBO->Assign(testBox);
    this->IBO->Assign(testIndices);
  }

	TextureRenderer::~TextureRenderer(){
    if(--_configuredInstances == 0){
			_configured = false;
			_pipeConfig.reset();
		}
  }

  void TextureRenderer::framebufferResizeCallback(
    uint32_t width,
    uint32_t height
  ){}

  void TextureRenderer::ConfigureDraws(){}

  void TextureRenderer::Render(Igneous::RenderBuffer &frame){
    auto pipeline = dynamic_pointer_cast<Igneous::GraphicsPipeline>(this->_pipeline_map["flatShade"]);

    //Indicate start of render pass
    pipeline->FrameStart(frame);

    //Configure rendering viewport / scissors
    this->_vkr->ConfigureViewports(pipeline);

    //Assign material for resource access in shaders
    pipeline->SetMaterials({this->plane_material});

    //Assign VBO / IBO for mesh rendering
    pipeline->AssignBuffers(*this->VBO,*this->IBO,_pipeConfig.get());

    //Generate / Execute draw command
    Igneous::IndexedDrawCommand cmd(6,0,0);
    pipeline->RecordDraw(cmd);

    //Indicate end of render pass
    pipeline->FrameEnd();
  }

  void TextureRenderer::Configure(){
    //Load shader SPIR-V bytecode into shared memory
    Igneous::Shader::Define("GLSL/texture.vert.spv");
    Igneous::Shader::Define("GLSL/texture.frag.spv");
  }

	void TextureRenderer::ConfigureRenderPass(Igneous::RenderPassGenerator &rpgen){
		rpgen.AddSubpass(
			{},
			{"surface_final"},
			{},
			{},
			false,
			""
		);
	}

  std::unique_ptr<Igneous::GraphicsPipelineConfiguration> TextureRenderer::CreateConfiguration(){
    auto config = std::make_unique<Igneous::GraphicsPipelineConfiguration>();
    config->vertexInput.AddBinding<DrawVert>();
    config->vertexInput.SetAttributes<std::array<float,2>,std::array<float,2>,uint32_t>(0,{
      vk::Format::eR32G32Sfloat,
      vk::Format::eR32G32Sfloat,
      vk::Format::eR8G8B8A8Unorm
    });

    //Pipeline State Configuration
    config->SetPrimitive<Igneous::Triangle>(Igneous::Triangle::Ordering::List);
    config->rasterization.mode = vk::PolygonMode::eFill;
    config->rasterization.frontFaceIsCCW = true;
    config->rasterization.lineWidth = 1;
    config->rasterization.sampleCount = vk::SampleCountFlagBits::e1;

    config->index16 = true;

    //Dynamic State Configuration
    config->dynamicStates.AddDynamicState(vk::DynamicState::eViewport);
    config->dynamicStates.AddDynamicState(vk::DynamicState::eScissor);

    //Color Blending Configuration
    config->colorBlend.AddAttachment(
      1,
      vk::BlendFactor::eSrcAlpha, vk::BlendFactor::eOneMinusSrcAlpha, vk::BlendOp::eAdd,
      vk::BlendFactor::eOneMinusSrcAlpha, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
      {vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA}
    );

    config->stages = {{"texture.vert","main"},{"texture.frag","main"}};

    return config;
  }
}
