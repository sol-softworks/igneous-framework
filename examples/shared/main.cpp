// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
///		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//

#include <iostream>

#include "miniLog.h"

#ifdef EXAMPLE_S3D
#include "Simple3D/simple.h"
typedef SS::Simple3D Renderer;
#endif
#ifdef EXAMPLE_SSUI
#include "SSUI/ssui.h"
typedef SS::UI::ScreenspaceRenderer Renderer;
#endif
#ifdef EXAMPLE_TEXTURED
#include "Simple2D/textured.h"
typedef SS::UI::TextureRenderer Renderer;
#endif

int main(int argc, char** argv){

	{
		Renderer viewport;
		viewport.PostInitialize();
		while(viewport.checkEvents());
		ConsoleLog(LogType::INFO,"[Main] Event loop finish\n");
	}

	return 0;
}
