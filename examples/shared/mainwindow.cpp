// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
///		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
#include "mainwindow.h"

#include "resource.h"
#include "buffer.h"
#include "texture.h"
#include "Obsidian/material.h"
#include "Obsidian/matproperties.h"
#include "pipeline.h"
#include "samplerMeta.h"

#include "drawcmd.h"

#include "miniLog.h"

#include <algorithm>
#include <functional>

#include "lodepng/lodepng.h"

namespace Igneous{
	//*grumble grumble*
	//Defined in resource translation unit
	extern vk::Instance inst;
	extern vk::AllocationCallbacks* allocators;
}

namespace SS::UI{

	std::shared_ptr<Igneous::Texture<2>> LoadPNG(std::string filename){
		std::array<unsigned,2> dims;
		std::vector<unsigned char> img_data;
		lodepng::decode(img_data,dims[0],dims[1],filename);
		size_t upload_size = dims[0] * dims[1];
		std::vector<uint32_t> _p;
		_p.reserve(upload_size);
		for(int i=0; i<4*upload_size; i+=4){
			//img_data is an array of u_char*, so each element must
			//		be combined into a single uint32_t
			//
			//Pixels are retrieved as RGBA
			_p.push_back(uint32_t{
				static_cast<uint32_t>(img_data[i+3]) << 24 |
				static_cast<uint32_t>(img_data[i+2]) << 16 |
				static_cast<uint32_t>(img_data[i+1]) << 8 |
				static_cast<uint32_t>(img_data[i])
			});
		}
		auto ret = Igneous::Texture2D::Create(filename,dims,1,1);
		ret->SetPixels(_p);
		//ColorAttachments are for RenderTargets. Not Textures.
		//ret->SetUsageFlags({vk::ImageUsageFlagBits::eColorAttachment});
		ret->SetUsageFlags({vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled});
		return ret;
	}

	MainWindow::MainWindow(){
		if(glfwInit()){
			glfwWindowHint(GLFW_CLIENT_API,GLFW_NO_API);
			glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER,GLFW_TRUE);
			this->renderTarget = glfwCreateWindow(1920,1080,"Igneous Framework: Sample",NULL,NULL);
			if(glfwVulkanSupported()){
				uint32_t tmp;
				const char** _ext = glfwGetRequiredInstanceExtensions(&tmp);
				ConsoleLog(LogType::INFO,"[Window] Required Extensions: (%d)\n",tmp);
				for(int i=0; i<tmp; ++i) ConsoleLog(LogType::INFO,"[Window] - %s\n",_ext[i]);
				std::vector<const char*> ext(_ext,_ext+tmp);
				ConsoleLog(LogType::INFO,"[Window] Initializing Vulkan API\n");
				Igneous::InitAPI(ext);
				std::function<void(void*,vk::SurfaceKHR&)> call = [](void* windowData, vk::SurfaceKHR &surf) -> void{
					VkSurfaceKHR tmp = static_cast<VkSurfaceKHR>(surf);
					glfwCreateWindowSurface(
						Igneous::inst,
						static_cast<MainWindow*>(windowData)->renderTarget,
						reinterpret_cast<VkAllocationCallbacks*>(Igneous::allocators),
						&tmp);
					surf = tmp;
				};

				//TODO: Enable physical device selection
				ConsoleLog(LogType::INFO,"[VK] Creating Vulkan Devices\n");
				const float priority[] = {1.0f};
				std::array<const char*,1> devExtensions = {"VK_KHR_swapchain"};
				//TODO: Configurable LD creation count
				std::array<vk::DeviceQueueCreateInfo,1> info = {
					vk::DeviceQueueCreateInfo(vk::DeviceQueueCreateFlags(),0,1,priority)
				};
				this->dev = Igneous::GetPhysicalDevice(0).AddDevice({info.begin(),info.end()},{devExtensions.begin(),devExtensions.end()});

				ConsoleLog(LogType::INFO,"[Window] Initializing Vulkan Renderer\n");
				this->_vkr = std::make_unique<Igneous::WindowSurface>(*this->dev,call,(void*)this);

				glfwSetWindowUserPointer(this->renderTarget, this);
				glfwGetFramebufferSize(this->renderTarget,&this->window.width,&this->window.height);
				glfwSetFramebufferSizeCallback(this->renderTarget,
					[](GLFWwindow* window, int w, int h){
					auto win = static_cast<MainWindow*>(glfwGetWindowUserPointer(window));
					win->framebufferResizeCallback(static_cast<uint32_t>(w),static_cast<uint32_t>(h));
					win->_vkr->resizeFramebuffer(w,h);
					win->window = {w,h};
					}
				);

				//Initialize RPGen with surface attachment
				this->_vkr->AddSurfaceAttachment(this->_rpg);
			}
		}
		else{
			this->renderTarget = nullptr;
		}
	}

	MainWindow::~MainWindow(){
		//Cleanup Igneous
		this->_pipeline_map.clear();
		this->VBO.reset();
		this->IBO.reset();

		this->_vkr.reset();
		this->_rpmeta.reset();
		this->dev.reset();

		ConsoleLog(LogType::INFO,"[VKRF] - Destroying Device\n");
		Igneous::ReleaseAPI();

		//Cleanup GLFW
		glfwTerminate();
	}

	//And now: the most disgusting sequencing ever conceived
	bool MainWindow::checkEvents(){
		if(glfwWindowShouldClose(this->renderTarget)) return false;
		glfwPollEvents();

		glfwGetCursorPos(this->renderTarget, &this->mousePos[0], &this->mousePos[1]);

		this->ConfigureDraws();
		if(auto frame = this->_vkr->Prepare()){
			//ConsoleLog(LogType::INFO,"[Window] Frame Start\n");
			this->Render(frame.value());
			this->_vkr->Present();
		}
		//ConsoleLog(LogType::INFO,"[Window] Frame End\n");
		this->_vkr->Synchronize();

		this->EndFrame();
		return true;
	}

	void MainWindow::EndFrame(){}

	//WORKAROUND: Swapchain needs to have been created prior to configuration of
	//						attachments
	void MainWindow::PostInitialize(){
		this->_vkr->resizeFramebuffer(this->window.width,this->window.height);
	}
}
