// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Window API Wrapper
//
//	Description: Handles the creation and management of the example application
//				window. Uses the GLFW API to handle surface creation and user
//				input event processing

#ifndef SS_WINDOW
#define SS_WINDOW

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vulkan/vulkan.hpp>

#include <memory>

#include "surface.h"
#include "texture.h"
#include "resource.h"
#include "Pipelines/pipeline_configuration.h"
#include "Obsidian/material.h"

#include "renderTarget.h"

namespace SS{
	namespace UI{

		std::shared_ptr<Igneous::Texture<2>> LoadPNG(std::string filename);

		class MainWindow{

		private:
			GLFWwindow* renderTarget;

		protected:
			std::unique_ptr<Igneous::Device> dev;

			Igneous::RenderPassGenerator _rpg;
			std::shared_ptr<Igneous::RenderPassMeta> _rpmeta;
			std::unique_ptr<Igneous::WindowSurface> _vkr;

			struct windowDims{
				int width;
				int height;
			} window;

			std::array<double,2> mousePos;

			std::map<std::string,std::shared_ptr<Igneous::Pipeline>> _pipeline_map;

			std::unique_ptr<Igneous::VulkanBuffer> VBO;
			std::unique_ptr<Igneous::VulkanBuffer> IBO;

			virtual void framebufferResizeCallback(
				uint32_t width,
				uint32_t height
			) =0;

		public:
			//API Interface Construction
			MainWindow();
			virtual ~MainWindow();

			//Public API

			virtual bool checkEvents();
			virtual void ConfigureDraws() =0;
			virtual void Render(Igneous::RenderBuffer &frame) =0;
			virtual void EndFrame();

			virtual void PostInitialize();

		};
	}
}

#endif
