![Igneous Framework](banner_dev_A1.png)

Igneous Framework
======

A robust framework for Vulkan applications that simplifies the Vulkan interface, in an effort to provide an expedited development process for graphical and computational applications

Abstract
========

The Vulkan API is capable of providing unparalleled performance for cross-platform applications, at the cost of increased complexity in setting up the infrastructure. Even simple applications can require a few thousand lines of boilerplate configuration to display a textured plane. Much of this effort can be abstracted into generic construction procedures.

The Igneous Framework seeks to provide such an interface, in an attempt to streamline the Vulkan application development process.

Dependencies
============

- SPIRV-Cross: Bytecode reflection library, maintained by the Khronos Group. Used for the automatic pipeline generation system (Conduit), which will also generate Material blueprints for the Material system (Obsidian).

- GLM: OpenGL Mathematics library for GLSL-like semantic types and functionality. Used by Conduit for vector and matrix types in shader resources.

Overview
========

The Igneous Framework consists of multiple components. Each class is designed to provide a generic high-level interface to a cohesive set of Vulkan interfaces.

- Instance creation and destruction is handled automatically by the library initialization and cleanup routines, respectively. All a developer needs to provide for initialization is a list of required Vulkan extensions, which can come from an external windowing API (SDL, GLFW, etc.), or a manually-defined feature set.

- Physical Device enumeration is handled automatically by the library initialization routine. Physical device feature configuration is handled through a VkPhysicalDeviceFeatures (technically, vk::PhysicalDeviceFeatures) instance member struct, whose configuration consists entirely of boolean parameters. Enabling / disabling features requires setting individual feature flags to either 1 or 0, respectively. Features are disabled by default.

- Logical Device creation is handled by a member function on the PD class. Destruction is automatically handled when the associated physical device is no longer in use, which normally occurs during application termination. The Device class is a functional superset of the underlying VkDevice, as it aggregates common functionality into a shared interface.

- Descriptor Sets are implemented as a self-managed set of routines associated with a meta object - DescriptorSetMeta. This class allows for an easier interface to descriptor set layout definition, as it masks (most) of the internals of the process.

- Window Surfaces are created by the developer by passing a surface-generating function to the constructor of the WindowSurface class.

- All aspects relevant to pipeline usage are managed by the Pipeline class hierarchy. There are different pipeline subsets defined for each type of pipeline (Graphics and Compute), with additional pipelines being added as the Vulkan ecosystem progresses.

- The Obsidian Material System provides a (more) convenient interface to descriptor set data definitions. Data manipulation is handled through an associative array of properties, where the key is the property name as defined in the relevant shader sources.

- The Conduit System provides an automatic pipeline generation interface for an immensely simplified pipeline management process. Conduit-generated pipelines use the names and definitions exposed in shader sources to create seamless reference semantics for descriptor set binding and push constant value assignment.

Usage
=====

Most applications can use a fairly simple approach to implementing custom pipelines using Igneous.

For a GLFW-based application, the procedure outlined in the examples/shared/mainwindow.cpp file demonstrates how to initialize and configure the window surface.

##Initialization

Terrible test harness aside, the primary components to initialization are:

- `Igneous::InitAPI(...)`: Initialize the Igneous API (instance creation, physical device enumeration)

- `std::make_unique<Igneous::WindowSurface>(...)`: Create a Window Surface.

Note that it isn't necessary to use a std::unique_ptr to manage the surface: a simple instance will work. We use unique_ptr for internal projects, to leverage automatic memory management, but also because it makes it possible to store surfaces, which are inherently dynamic, as class members.

InitAPI takes a single `std::vector<const char*>` as an argument. This vector contains the stringified list of Vulkan extensions required by the windowing API to create a window surface. If your application requires further extensions, they may be added to this vector prior to initializing Igneous.

The WindowSurface constructor takes two arguments: a functor, and a user-defined data pointer. This approach, while it may seem complex, is simply how Igneous decouples the actual surface creation process. The reason for doing so is that surface creation is usually handled by the windowing API used. GLFW, as an example, provides the `glfwCreateWindowSurface(...)` function, which must be provided the Vulkan instance, allocation callbacks, and surface parameters.

##Custom Renderers

The Igneous Framework allows developers to create custom render systems with arbitrary numbers of pipelines. As the implementor of a renderer, you are given full control over high-level data management, including how pipeline resources (VBO,IBO,Material,etc.) are managed, the execution order of pipelines, and viewport configuration.

Examples for how to implement custom renderers are included in the examples folder, but your own renderer does not need to follow the same structure as those in the examples.

###Configuration
The kPipelineConfiguration structs are used to set various configuration properties used when compiling pipeline instances. Graphics and Compute pipelines each have their own configuration structs, but both are considered equivalent for any methods that require passing a PipelineConfiguration instance.

See the documentation for each configuration struct for more information.

###Render Pass
Render pass configuration is handled through an instance of the RenderPassGenerator class. RPG may be used to add any number of attachments, subpass definitions, and subpass dependencies to a render pass.

It should be noted that any pipeline that uses a WindowSurface's swapchain for final output must invoke WindowSurface::AddSurfaceAttachment with its RPG instance. This will add a "surface_final" attachment representing the surface's swapchain images that will be automatically assigned by the WindowSurface::resizeFramebuffer call.

Documentation
=============

Documentation for the Igneous Framework is currently hosted via GitLab Pages. The latest documentation may be accessed via <https://sol-softworks.gitlab.io/igneous-framework>

Licensing
=========

The Igneous Framework is licensed under the GNU LGPL 3.0, and may be used in any project under the appropriate licensing terms set out by the license. See LICENSE for the full text.
