// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework dynarray utility implementation
//
//	Description: Utility container template for dynamically-allocated arrays of
//         fixed size, whose size cannot be determined at compile time. This is
//         designed as a reduced implementation of the semantics for the old
//         std::dynarray proposal.

#ifndef IGNEOUS_UTIL_DYNARRAY
#define IGNEOUS_UTIL_DYNARRAY

#include <algorithm>
#include <memory>
#include <stdexcept>

namespace Igneous::util{

  ///@brief Statically-sized, dynamically-allocated array
  ///
  ///@details Container template for the allocation of statically-sized arrays
  ///         whose size cannot be determined at compile time. Essentially a
  ///         wrapper around C-style arrays of unknown bound, but with an interface
  ///         similar to that of std::array.
  template<typename elemType>
  struct dynarray{
  private:
    const size_t _size;
    std::unique_ptr<elemType[]> _data;

  public:
    typedef elemType                                value_type;
    typedef value_type*                             pointer;
    typedef const value_type*                       const_pointer;
    typedef value_type&                             reference;
    typedef const value_type&                       const_reference;
    typedef value_type*                             iterator;
    typedef const value_type*                       const_iterator;
    typedef std::size_t                             size_type;
    typedef std::ptrdiff_t                          difference_type;
    typedef std::reverse_iterator<iterator>         reverse_iterator;
    typedef std::reverse_iterator<const_iterator>   const_reverse_iterator;

    ///@brief Array construction
    ///
    ///@details Constructs an instance with the specified size.
    ///
    ///@param[in] arrSize Size of the array
    dynarray(
      const size_t arrSize
    ) :
      _size(arrSize),
      _data(std::make_unique<elemType[]>(_size))
    {}

    ~dynarray(){
      _data.reset();
    }

    ///@brief Copy assignment operator
    ///
    ///@details Performs an element-wise copy from the assigning array.
    ///
    ///@param[in] other Array containing values to be assigned to this array.
    ///
    ///@note If there is a size mismatch between the two arrays, any elements
    ///       beyond the range of the smaller array are unaffected by the assignment.
    dynarray& operator=(const dynarray &other){
      if(this != &other){
        //Perform element-wise copy. If the arrays are of different sizes,
        //    copy as many elements as are available / possible.
        auto oit = other.begin();
        for(auto sit = this->begin();; ++sit,++oit){
          if(sit != this->end() && oit != other.end()){
            *sit = *oit;
          }
          else break;
        }
      }
      return *this;
    }

    ///@brief Move assignment operator
    ///
    ///@details Performs an element-wise move from the assigning array.
    ///
    ///@param[in] other Array containing values to be assigned to this array.
    ///
    ///@note If there is a size mismatch between the two arrays, any elements
    ///       beyond the range of the smaller array are unaffected by the assignment.
    ///
    ///@attention These move semantics are different from what you may expect
    ///         when working with arrays of different size. This is more akin to
    ///         an array splice than a move assignment.
    dynarray& operator=(dynarray &&other) noexcept{
      if(this != &other){
        //Perform element-wise swap. If the arrays are of different sizes,
        //    swap as many elements as possible.
        for(auto sit = this->begin(), oit = other.begin();; ++sit,++oit){
          if(sit != this->end() && oit != other.end()){
            std::iter_swap(sit,oit);
          }
          else break;
        }
      }
      return *this;
    }

    ///@name Operations
    ///@{

    ///@brief Fill array with specified value
    ///
    ///@details Assigns copies of the provided value to every element of the
    ///         container.
    ///
    ///@param[in] __u Value to assign
    void fill(const value_type& __u)
    { std::fill_n(begin(), size(), __u); }

    ///@brief Swap values with other array
    ///
    ///@details Performs an element-wise swap between this and the provided array.
    ///
    ///@param[in] __other Array with which to swap elements
    ///
    ///@note If there is a size mismatch between the two arrays, any elements
    ///       beyond the range of the smaller array are unaffected by the assignment.
    void swap(dynarray& __other)
    { std::swap_ranges(begin(), size(), __other.begin()); }
    ///@}


    ///@name Iterators
    ///@{
    //
    ///@brief Iterator to container beginning
    constexpr iterator begin() noexcept
    { return iterator(data()); }

    ///@copydoc begin()
    constexpr const_iterator begin() const noexcept
    { return const_iterator(data()); }

    ///@brief Iterator to container end
    constexpr iterator end() noexcept
    { return iterator(data() + _size); }

    ///@copydoc end()
    constexpr const_iterator end() const noexcept
    { return const_iterator(data() + _size); }

    ///@brief Reverse Iterator to container beginning
    constexpr iterator rbegin() noexcept
    { return reverse_iterator(data()); }

    ///@copydoc rbegin()
    constexpr const_iterator rbegin() const noexcept
    { return const_reverse_iterator(data()); }

    ///@brief Reverse Iterator to container end
    constexpr iterator rend() noexcept
    { return reverse_iterator(data() + _size); }

    ///@copydoc rend()
    constexpr const_iterator rend() const noexcept
    { return const_reverse_iterator(data() + _size); }

    ///@copydoc begin()
    constexpr const_iterator cbegin() const noexcept
    { return const_iterator(data()); }

    ///@copydoc end()
    constexpr const_iterator cend() const noexcept
    { return const_iterator(data() + _size); }

    ///@copydoc rbegin()
    constexpr const_iterator crbegin() const noexcept
    { return const_reverse_iterator(data()); }

    ///@copydoc rend()
    constexpr const_iterator crend() const noexcept
    { return const_reverse_iterator(data() + _size); }
    ///@}

    ///@name Capacity
    ///@{
    //
    ///@brief Number of elements in container
    constexpr size_type size() const noexcept
    { return _size; }

    ///@brief Maximum number of elements in container
    constexpr size_type max_size() const noexcept
    { return _size; }

    ///@brief Checks whether container is empty
    [[nodiscard]]
    constexpr bool empty() const noexcept
    { return size() == 0; }
    ///@}


    ///@name Element Access
    ///@{
    //
    ///@brief Access specified element
    constexpr reference operator[](size_type __n) noexcept
    { return _data[__n]; }

    ///@copydoc operator[]
    constexpr const_reference operator[](size_type __n) const noexcept
    { return _data[__n]; }

    ///@brief Access specified element with bounds checking
    constexpr reference at(size_type __n)
    {
      if(__n >= _size){
        std::__throw_out_of_range_fmt(__N("dynarray::at: requested index %zu out of range"),__n);
      }
      return _data[__n];
    }

    ///@copydoc at(size_type)
    constexpr const_reference at(size_type __n) const
    {
      if(__n >= _size){
        std::__throw_out_of_range_fmt(__N("dynarray::at: requested index %zu out of range"),__n);
      }
      return _data[__n];
    }

    ///@brief Access first element
    constexpr reference front() noexcept
    { return *begin(); }

    ///@copydoc front()
    constexpr const_reference front() const noexcept
    { return *begin(); }

    ///@brief Access last element
    constexpr reference back() noexcept
    { return *(end()--); }

    ///@copydoc back()
    constexpr const_reference back() const noexcept
    { return *(end()--); }

    ///@brief direct access to underlying array
    constexpr pointer data() noexcept
    { return _data.get(); }

    ///@copydoc data()
    constexpr const_pointer data() const noexcept
    { return _data.get(); }
    ///@}
  };

  template<typename elemType>
  constexpr bool operator==(const dynarray<elemType> &lhs,const dynarray<elemType> &rhs)
  {
    if(lhs.size() != rhs.size()) return false;
    for(size_t i=0; i<lhs.size(); ++i){
      if(lhs[i] != rhs[i]) return false;
    }
    return true;
  }

  template<typename elemType>
  constexpr bool operator!=(const dynarray<elemType> &lhs,const dynarray<elemType> &rhs)
  {
    return !(lhs == rhs);
  }

  template<typename elemType>
  constexpr bool operator<(const dynarray<elemType> &lhs,const dynarray<elemType> &rhs)
  {
    for(auto lit = lhs.begin(), rit = rhs.begin();; ++lit,++rit){
      if(lit != lhs.end() && rit != rhs.end()){
        if(*lit < *rit) return true;
        if(*lit > *rit) return false;
      }
      else{
        if(rit != rhs.end()) return true;
        return false;
      }
    }
    //I'd like to know how you got here...
    return false;
  }

  template<typename elemType>
  constexpr bool operator<=(const dynarray<elemType> &lhs,const dynarray<elemType> &rhs)
  {
    return (lhs < rhs) || (lhs == rhs);
  }

  template<typename elemType>
  constexpr bool operator>(const dynarray<elemType> &lhs,const dynarray<elemType> &rhs)
  {
    return (rhs < lhs);
  }

  template<typename elemType>
  constexpr bool operator>=(const dynarray<elemType> &lhs,const dynarray<elemType> &rhs)
  {
    return !(lhs < rhs);
  }

  template<typename elemType>
  constexpr void swap(dynarray<elemType> &lhs, dynarray<elemType> &rhs)
  noexcept(noexcept(lhs.swap(rhs)))
  {
    lhs.swap(rhs);
  }
}

#endif
