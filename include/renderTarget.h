// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Render Target Infrastructure
//
//	Description: Generalized framebuffer management system that allows pipeline
//         rendering operations to target multiple framebuffers.

#ifndef IGNEOUS_RENDERTARGET
#define IGNEOUS_RENDERTARGET

#include <map>
#include <vector>

#include "vulkan/vulkan.hpp"

#include "resource.h"
#include "texture.h"

#include "renderPass.h"

namespace Igneous{

  ///@brief Render Command Destination
  ///
  ///@details Texture resource wrapper designed to provide a generic interface
  ///         for Render-To-Texture processes. Surface compositing is also
  ///         implemented using RenderTarget instances, where the target texture
  ///         is an externally-managed image resource.
  class RenderTarget{
  private:
    Device &dev;

    std::shared_ptr<BaseTexture> targetTexture;      ///<Texture which will be drawn to
    vk::ImageViewCreateInfo _ivci;  ///<View creation metadata

    vk::Extent2D extents;           ///<Dimensions (extents) of this texture

  public:
    RenderTarget(
      Device &_d,
      vk::Format format,
      vk::Extent2D extents
    );
    RenderTarget(
      Device &_d,
      vk::Format format,
      vk::Extent2D extents,
      vk::Image &rawImage
    );

    //Should this be generally exposed?
    RenderTarget& SetImage(
      vk::Image &rawImage,
      vk::Extent2D exts
    );

    ///@brief Get Valid Subregion
    ///
    ///@details Attempts to create a valid rendering subregion from the provided
    ///         parameters.
    ///
    ///@attention This method will avoid defining a draw region that isn't fully
    ///           encapsulated by the texture. If there is a partial extrusion,
    ///           where some of the requested subregion lies outside the texture,
    ///           the subregion will be constrained to only fit within the visible
    ///           region by reducing its extents.\n
    ///           However, if the requested offset places the entire subregion
    ///           outside of the texture, an empty bounding box will be returned.
    vk::Rect2D GetSubregion(vk::Offset2D &offset, vk::Extent2D &extents) const;

    ///@brief Get Full Render Region
    ///
    ///@details Constructs a bounding box that encompasses the entire rendering
    ///         surface. Equivalent to GetSubregion({0,0},GetExtents())
    operator vk::Rect2D() const;

    ///@brief Get Texture Extents
    ///
    ///@details Accessor for the extents of the encapsulated texture.
    vk::Extent2D GetExtents() const;

    BaseTexture& GetTexture(Device &owner);
  };

  ///@brief Surface Framebuffer
  ///
  ///@details Render operation interface that encapsulates framebuffer usage
  ///         requirements. Provides an interface for manipulating framebuffer
  ///         attachments, and configuring the associated render region.
  class RenderBuffer{
  private:
    std::shared_ptr<RenderPassMeta> rpm;          ///<Render pass metadata
    std::map<std::string,uint32_t> attNameIndex;  ///<Attachment identifier mapping

    ///Framebuffer attachment configuration
    std::map<uint32_t,std::pair<BaseTexture::view,vk::ClearValue>> fbAttachments;

    vk::Rect2D renderArea;                        ///<Framebuffer render subregion

    //There's a reason for this.
    std::shared_ptr<vk::Framebuffer> framebuffer; ///<Framebuffer resource

    std::vector<vk::ClearValue> _rpBlanking;      ///@internal Attachment blanking reference

  public:

    RenderBuffer(std::shared_ptr<RenderPassMeta> rpmeta);

    ///@brief Assign Framebuffer Attachment
    ///
    ///@details Assigns a reference to the provided texture resource to the
    ///         attachment with the specified identifier.
    ///
    ///@param[in] id Identifier for the attachment
    ///
    ///@param[in] attachmentSource Texture to be used as the attachment referent
    RenderBuffer& SetAttachment(std::string id, BaseTexture &attachmentSource);

    ///@brief Assign Attachment Blanking Value
    ///
    ///@details Assigns the provided clear value to the attachment with the
    ///         specified identifier.
    ///
    ///@param[in] id Identifier for the attachment
    ///
    ///@param[in] value Blanking value used to clear the attachment between frames
    RenderBuffer& SetClearValue(std::string id, vk::ClearValue value);

    ///@brief Set Framebuffer Area
    ///
    ///@details Sets the render area of this framebuffer to the provided value.
    ///
    ///@param[in] area New bounding box of the framebuffer's render area
    ///
    ///@attention Make sure the render area does not exceed the bounds of any
    ///           render targets, as doing so will produce unexpected behaviour.
    RenderBuffer& SetRenderArea(vk::Rect2D const &area);

    ///@brief Set Framebuffer Dimensions
    ///
    ///@details Sets the extents of this framebuffer to the provided value.
    ///
    ///@param[in] extents New extents of the framebuffer's render area
    ///
    ///@attention Make sure the render area does not exceed the bounds of any
    ///           render targets, as doing so will produce unexpected behaviour.
    RenderBuffer& SetExtents(vk::Extent2D const &extents);

    ///@brief Set Framebuffer Dimensions
    ///
    ///@details Sets the extents of this framebuffer to the provided value.
    ///
    ///@param[in] width New width of the framebuffer
    ///
    ///@param[in] height New height of the framebuffer
    ///
    ///@attention Make sure the render area does not exceed the bounds of any
    ///           render targets, as doing so will produce unexpected behaviour.
    RenderBuffer& SetExtents(uint32_t const width, uint32_t const height);

    ///@brief Set Framebuffer Offset
    ///
    ///@details Sets the offset of this framebuffer to the provided value.
    ///
    ///@param[in] offset New offset of the framebuffer's render area
    ///
    ///@attention Make sure the render area does not exceed the bounds of any
    ///           render targets, as doing so will produce unexpected behaviour.
    RenderBuffer& SetRenderOffset(vk::Offset2D const &offset);

    ///@brief Build Framebuffer
    ///
    ///@details Constructs a framebuffer instance from the current attachment
    ///         data state.
    ///
    ///@param[in] dev Logical device on which to construct this framebuffer
    ///
    ///@attention After building, this framebuffer may only be used by pipelines
    ///           built on the same logical device. Executing build with a
    ///           different device will destroy the current framebuffer, and
    ///           create a new framebuffer on the new device.
    void Build(Device &dev);

    ///@internal Creates a vk::RenderPassBeginInfo for a built framebuffer.
    vk::RenderPassBeginInfo StartPass();

    ///@brief Framebuffer State Reporting
    ///
    ///@details Reports whether the framebuffer is currently in a valid, usable
    ///         state (i.e. built)
    operator bool() const;
  };
}

#endif
