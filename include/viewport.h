// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Viewport Configuration
//
//	Description: Surface viewport configuration structures

#ifndef IGNEOUS_VIEWPORT
#define IGNEOUS_VIEWPORT

#include "vulkan/vulkan.hpp"

namespace Igneous{

  ///@brief Viewport configuration
  ///
  ///@details The ViewportConfiguration class provides a set of procedures for
  ///         configuring the viewports and scissors used by a surface.
  class ViewportConfiguration{
  private:
    std::vector<vk::Viewport> viewports;
    std::vector<vk::Rect2D> scissors;

  public:
    ///@brief Add viewport definition to configuration
    void AddViewport(vk::Extent2D position, vk::Extent2D dimensions, vk::Extent2D depth);

    ///@brief Add scissor definition to configuration
    void AddScissor(vk::Offset2D offset, vk::Extent2D extents);

    void RecreateViewport(size_t vpIndex, vk::Extent2D position, vk::Extent2D dims, vk::Extent2D depth);
    void SetViewports(vk::CommandBuffer &buffer);

    void RecreateScissor(size_t scIndex, vk::Offset2D offset, vk::Extent2D extents);
    void SetScissors(vk::CommandBuffer &buffer);

    ///@brief Create Viewport Configuration creation info struct from configured
    ///       elements.
    vk::PipelineViewportStateCreateInfo CreateInfo();
  };

}

#endif
