// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Pipeline Configuration Infrastructure
//
//	Description: Pipeline configuration infrastructure for the Igneous Framework.
//         Contains classes used for the creation of pipelines, and configuring
//         individual pipeline behaviours prior to construction.

#ifndef IGNEOUS_PIPELINE_CONFIGURATION
#define IGNEOUS_PIPELINE_CONFIGURATION

#include <vector>
#include <memory>

#include "vulkan/vulkan.hpp"

#include "Conduit/shader.h"
#include "Conduit/dsmeta.h"

#include "viewport.h"
#include "resource.h"
#include "renderPass.h"

namespace Igneous{

  ///@brief Dynamic State configuration
  ///
  ///@details Provides an interface for managing the pipeline state elements
  ///         which will be provided before every rendering operation.
  class DynamicStateConfiguration{
  private:
    std::vector<vk::DynamicState> dynamicStates;

  public:
    ///@brief Configure a dynamic state element
    void AddDynamicState(vk::DynamicState state);

    ///@brief Create Dynamic State creation info structure from configured
    ///       elements.
    vk::PipelineDynamicStateCreateInfo CreateInfo();
  };

  ///@brief Color Blend Fixed-function configuration
  ///
  ///@details The ColorBlendState class provides an interface for configuring the
  ///         Color Blend fixed-function portion of a graphics pipeline.
	class ColorBlendState{
	private:
		std::vector<vk::PipelineColorBlendAttachmentState> colorAttaches;

	public:
		uint32_t logicEnable;   ///< Whether logic operations are enabled for this blend state
		vk::LogicOp logicOp;    ///< Logic operation applied to color components

    ///@brief Add blend attachment
    ///
    ///@details Adds a blend attachment to the configuration. Blend attachments
    ///         are used to configure how colours are blended between shader
    ///         fragment outputs and the associated colour attachments.
    ///
    ///@param[in] enabled Enables blending for the corresponding colour attachment.
    ///           If disabled, the corresponding shader fragment colour will be
    ///           passed through unmodified to the colour attachment
    ///
    ///@param[in] sourceColorBlend Specifies the influence of the colour components
    ///           from the fragment source.
    ///
    ///@param[in] destColorBlend Specifies the influence of the colour components
    ///           from the colour attachment.
    ///
    ///@param[in] colorBlendOp Specifies the blend operation to be used when
    ///           calculating the final colour to be written to the colour
    ///           attachment.
    ///
    ///@param[in] sourceAlphaBlend Specifies the influence of the alpha component
    ///           from the fragment source.
    ///
    ///@param[in] destAlphaBlend Specifies the influence of the alpha component
    ///           from the colour attachment.
    ///
    ///@param[in] alphaBlendOp Specifies the blend operation to be used when
    ///           calculating the final alpha value to be written to the colour
    ///           attachment.
    ///
    ///@param[in] writeMask Bitmask specifying the components that may be written.
    ///
    ///@todo Simplify interface, rather than pure parameter forwarding
		void AddAttachment(vk::Bool32 enabled,
			vk::BlendFactor sourceColorBlend,
			vk::BlendFactor destColorBlend,
			vk::BlendOp colorBlendOp,
			vk::BlendFactor sourceAlphaBlend,
			vk::BlendFactor destAlphaBlend,
			vk::BlendOp alphaBlendOp,
			vk::ColorComponentFlags writeMask);

    ///@brief Create Color Blend State creation info structure from configured
    ///       attachments.
		vk::PipelineColorBlendStateCreateInfo CreateInfo();
	};

  ///@brief Vertex Input Fixed-function configuration
  ///
  ///@details The VertexInputState class provides an interface for configuring the
  ///         value bindings to be used when providing data to the shaders used
  ///         in a constructed pipeline.
	class VertexInputState{
	private:

    ///@internal Creates a vertex attribute.
    ///
    ///@note This function modifies the index and offset parameters, as it is only
    ///       intended to be used by the fold-expression in SetAttributes.
		template<typename Tp>
		static vk::VertexInputAttributeDescription _makeAttr(uint32_t binding, size_t &index, vk::Format format, size_t &attrOffset){
			attrOffset += sizeof(Tp);
			return vk::VertexInputAttributeDescription{
				static_cast<uint32_t>(index++),
				binding,
				format,
				static_cast<uint32_t>(attrOffset-sizeof(Tp))
			};
		};

	public:
		std::vector<vk::VertexInputBindingDescription> bindings;
		std::vector<vk::VertexInputAttributeDescription> attributes;

    ///@brief Add input binding
    ///
    ///@details Adds a vertex input binding to the state manager, using the size
    ///         of the passed type to create the binding. The type itself is only
    ///         used to determine the expected size of the binding: no value is
    ///         passed during binding creation.
    ///
    ///@tparam Ts Type that should be expected to be used when writing values to
    ///         this binding.
    ///
    ///@param[in] isInstanced Whether vertex attributes are addressed by the vertex
    ///           or instance index when used in draw operations.
		template<typename Ts>
		void AddBinding(bool isInstanced=false){
			auto bind = this->bindings.emplace_back(
				this->bindings.size(),
				sizeof(Ts),
				vk::VertexInputRate::eVertex);
			if(isInstanced) bind.setInputRate(vk::VertexInputRate::eInstance);
		};

    ///@brief Set binding attributes
    ///
    ///@details Sets the attributes to be provided by the specified binding,
    ///         according to the supplied types and formats.
    ///
    ///@tparam Tm... List of types that should be considered individual components
    ///             of the input binding. Each attribute created has the same size
    ///             as the identically-indexed type passed as an argument.
    ///
    ///@param[in] bindingIndex Binding which will provide the supplied attributes.
    ///
    ///@param[in] formats Array of Vulkan formats, where each element corresponds
    ///           to the identically-indexed template parameter type.
    ///
    ///@note The types passed as template parameters are only used for sizing
    ///       requirements. The types should be the constituent members in the
    ///       struct-type specified when creating the binding, if applicable.
    ///
    ///@attention The order of types and formats is important. Make sure the
    ///           order is consistent with what the vertex shader expects as per
    ///           it's input layout. Incorrect ordering may work, but not behave
    ///           as expected.
		template<typename... Tm>
		void SetAttributes(uint32_t bindingIndex, std::array<vk::Format,sizeof...(Tm)> formats){
			size_t index = 0;
			size_t attrOffset = 0;
			auto vAtt = {
				VertexInputState::_makeAttr<Tm>(bindingIndex,index,formats[index],attrOffset)...
			};
			for(auto &v : vAtt){
				this->attributes.push_back(std::move(v));
			}
		};

    ///@brief Create Vertex Input State creation info structure from configured
    ///       bindings.
		vk::PipelineVertexInputStateCreateInfo CreateInfo();
	};

  ///@brief Primitive Configuration Class
  ///
  ///@details The PrimitiveConfiguration class provides an interface to
  ///         the vertex primitive specification process. Provides the Input
  ///         Assembly State configuration used by a PipelineConfiguration instance.
  ///
  ///@note The downside to this approach is that there may be multiple
  ///       instances of a single type of primitive, leading to unnecessary
  ///       memory allocations.
  ///
  ///@todo This approach defers topology decisions to derived classes. While this
  ///       works, it requires polymorphism to implement the CreateInfo method.
  ///       While the templated PipelineConfiguration::SetPrimitive method works,
  ///       there's probably a better process that doesn't require templates.
  class PrimitiveConfiguration{
  protected:
    vk::PrimitiveTopology topology;
  public:

    virtual ~PrimitiveConfiguration();

    bool primitiveRestartEnable;

    virtual vk::PipelineInputAssemblyStateCreateInfo CreateInfo();

  };

  class Point final : public PrimitiveConfiguration{
    Point();
  };

  class Line final : public PrimitiveConfiguration{
  public:
    enum class Ordering{
      List,
      Strip
    };

    Ordering _layout;
    bool withAdjacency;

    Line(Ordering layout);

    //Overriden to account for adjacency requirements
    virtual vk::PipelineInputAssemblyStateCreateInfo CreateInfo() override;
  };

  class Triangle final : public PrimitiveConfiguration{
  public:
    enum class Ordering{
      List,
      Strip,
      Fan
    };

    Ordering _layout;
    bool withAdjacency;

    Triangle(Ordering layout);

    //Overriden to account for adjacency requirements
    virtual vk::PipelineInputAssemblyStateCreateInfo CreateInfo() override;
  };

  class Patch final : public PrimitiveConfiguration{
    Patch();
  };

  ///@brief Tessellation Configuration Class
  ///
  ///@details The Tessellation Configuration class provides an interface to the
  ///         mesh tessellation patching system.
  ///
  ///@note This class exists to shield developers from changes to the tessellation
  ///       configuration process. While there is currently only a single configurable
  ///       value, new flags could potentially be introduced in the Vulkan spec.
  class TessellationConfiguration{
  public:
    uint32_t patchControlPoints;

    vk::PipelineTessellationStateCreateInfo CreateInfo();
  };

  ///@brief Depth and Stencil test configurations
  ///
  ///@details The DepthStencilConfiguration class combines the configurations for
  ///         depth tests and stencil tests into a single interface.
  ///
  ///@note Depth and Stencil tests are already part of a single interface in
  ///       Vulkan, but this class attempts to make each process easier to
  ///       configure.
  ///
  ///@todo The interface needs to \em actually simplify the configuration. Right
  ///       now, it's just parameter forwarding.
  class DepthStencilConfiguration{
  public:
    ///@name Depth Testing Configuration
    ///@{
    bool enableDepthTest;
    bool enableDepthWrite;
    vk::CompareOp comparator;
    ///@}

    ///@name Depth Bounds Testing Configuration
    ///@{
    bool enableBoundsTest;
    std::array<float,2> depthBounds;
    ///@}

    ///@name Stencil Testing Configuration
    ///@{
    bool enableStencilTest;
    vk::StencilOpState frontStencil;
    vk::StencilOpState backStencil;
    ///@}

    vk::PipelineDepthStencilStateCreateInfo CreateInfo();
  };

  ///@brief Rasterization Configuration Class
  ///
  ///@details The Rasterization Configuration class provides an interface to the
  ///         configuration of processes used during rasterization and fragment
  ///         operations.
  ///
  ///@note This class combines two underlying Vulkan creation structs that cover
  ///       similar components of the graphics pipeline: RasterizationState and
  ///       MultisampleState.
  ///@todo Multisample-specific components should be better abstracted. The
  ///       behaviour required by the sample mask system introduces complications,
  ///       but it should still be possible to make the interface easier to use.
  class RasterizationConfiguration{
  private:
    vk::CullModeFlags cullFlags;
    vk::FrontFace frontFaceWinding;

  public:
    ///@name Rasterization Configuration
    ///@{
    bool enableDepthClamp;          ///<Enable Depth Range Clamping
    bool enableRasterizerDiscard;   ///<Enable primitive discard prior to rasterization stage

    vk::PolygonMode mode;           ///<Primitive rendering mode (Fill,Line,Point)
    ///@}

    ///@name Face Culling
    ///@{
    bool cullFrontFace;             ///<Whether front faces should be culled
    bool cullBackFace;              ///<Whether back faces should be culled
    bool frontFaceIsCCW;            ///<Specifies whether primitive winding is counter-clockwise (anti-clockwise)
    ///@}

    ///@name Depth Bias
    ///@{
    bool enableDepthBias;           ///<Enable depth bias for fragment results
    float depthBiasConstantFactor;   ///<Scaling factor for fragment depth bias
    float depthBiasClamp;            ///<Extrema value for depth bias (max/min)
    float depthBiasSlopeFactor;      ///<Scaling factor for fragment slope
    ///@}

    float lineWidth;                 ///<Width of rasterized line segments

    ///@name Multisample-specific components
    ///@{
    bool enableSampleShading;       ///<Whether sample shading is enabled
    float minSampleShading;          ///<Minimum fractional shading

    vk::SampleCountFlagBits sampleCount;  ///<Number of rasterization samples (Primitive Antialiasing)
    vk::SampleMask* pMask;          ///<Bitmask of samples to be used

    bool enableAlphaToCoverage;     ///<Enable implicit setting of fragment coverage from alpha component of first colour output
    bool enableAlphaToOne;          ///<Enable override of fragment first colour alpha component with one (opaque override)
    ///@}

    vk::PipelineRasterizationStateCreateInfo CreateRasterInfo();
    vk::PipelineMultisampleStateCreateInfo CreateMSInfo();
  };

  ///@brief	Pipeline Configuration Class
  ///
  ///@details	The PipelineConfiguration hierarchy is designed to make user-defined
  ///					pipelines simple to define, by abstracting the internal creation
  ///					semantics of the Vulkan API behind a generalized interface.
  ///
  class PipelineConfiguration{
  private:
    std::vector<vk::ShaderModule> shaders;
    std::map<std::string,Shader::_PCBlockDef> pc_block_defs;
    std::set<Shader::SetBinding,Shader::BindingComp> shaderBindings;
    std::vector<DescriptorLayoutMeta> layout_metas;

  protected:
    std::vector<vk::PipelineShaderStageCreateInfo> shaderStages;

    void _CompileShaders(vk::Device const &dev);
    void _CleanupShaders(vk::Device const &dev);

    void _ConfigurePushConstants(PushConstantLayout &layout);
    void _ConfigureBindings(Device &dev);

    void _ConfigureLayout(
      vk::Device const &dev,
      vk::PipelineLayout &layout
    );

  public:
    ///@brief Shader stage configuration
    ///
    ///@details Set of shader stages to be used by configured pipelines. Pairs
    ///         consist of a shader name and entry point.
    ///
    ///@note Shader names are defined by the "name" parameter to the
    ///			GraphicsPipeline::DefineShader static function
    std::vector<std::pair<std::string,std::string>> stages;

    ///@brief Compile pipeline
    ///
    ///@details Builds a pipeline instance, using this configuration instance to
    ///         customize the fixed function stages of the pipeline.
    ///
    ///         This method generates the pipeline layout for the configured
    ///         pipeline, and initializes the push constant block layout
    ///
    ///@param[in] dev Igneous Logical Device to use for pipeline creation
    ///
    ///@param[out] layout External storage for the constructed pipeline layout
    ///
    ///@param[out] pc_block External storage for the pipeline's push constant
    ///             block
    virtual vk::Pipeline CreatePipeline(
      Device &dev,
      vk::PipelineLayout &layout,
      PushConstantLayout &pc_block
    ) =0;

    ///@brief Assign Descriptor Set Layouts
    ///
    ///@details Assigns the internal set of descriptor set layouts to the
    ///         externally-managed vector.
    ///
    ///@param[out] layout_meta External storage for the pipeline's descriptor
    ///             set layout metadata
    void _AssignLayouts(std::vector<DescriptorLayoutMeta> &layout_meta);
  };

  class ComputePipelineConfiguration final : public PipelineConfiguration{
  public:
    virtual vk::Pipeline CreatePipeline(
      Device &dev,
      vk::PipelineLayout &layout,
      PushConstantLayout &pc_block
    ) override;
  };

	///@brief	Graphics Pipeline Configuration Class
	///
	///@details	The GraphicsPipelineConfiguration class is designed to make user-defined
	///					graphics pipelines simple to define, by abstracting the internal
  ///         creation semantics of the Vulkan API.
	///
	///@note		Some configuration options require knowledge of the Vulkan-Hpp
	///					enumerated types. Future developments will provide a set of
	///					proxy classes that manage the *CreateInfo structure definitions.
	///
	class GraphicsPipelineConfiguration final : public PipelineConfiguration{
	private:
		//Internally Managed by respective meta-structs
		vk::PipelineVertexInputStateCreateInfo vertex;
		vk::PipelineColorBlendStateCreateInfo color;
		vk::PipelineViewportStateCreateInfo vp;
		vk::PipelineDynamicStateCreateInfo ds;
    vk::PipelineInputAssemblyStateCreateInfo inputAssembly;
    vk::PipelineTessellationStateCreateInfo tess;
    vk::PipelineRasterizationStateCreateInfo rasterConfig;
    vk::PipelineMultisampleStateCreateInfo multisampling;

    ///@todo The fact that a user-configurable struct needs to be internally
    ///       managed should be a big hint that something isn't right...
    std::unique_ptr<PrimitiveConfiguration> primitiveConfig;

    uint32_t viewports;
    uint32_t scissors;

	public:

		//mutators:
		//	- AddViewport
		//	- AddScissor
		//	- AddDynamicState
		DynamicStateConfiguration dynamicStates;

		//mutators:
		//	- AddBinding
		//	- SetAttributes
		VertexInputState vertexInput;

		//options:
		//	- logicEnable
		//	- logicOp
		//
		//mutators:
		//	- AddAttachment
		ColorBlendState colorBlend;

		TessellationConfiguration tesselation;

    ///@todo Custom class to abstract Vulkan internals
		///options:
		///	- flags
		///	- depthTestEnable (bool)
		///	- depthWriteEnable (bool)
		///	- depthCompareOp (vk::CompareOp)
		///	- depthBoundsTestEnable (bool)
		///	- stencilTestEnable (bool)
		///	- front (vk::StencilOpState)
		///	- back (vk::StencilOpState)
		///	- minDepthBounds (float)
		///	- maxDepthBounds (float)
		vk::PipelineDepthStencilStateCreateInfo depthStencil;

    RasterizationConfiguration rasterization;

		///@brief Index buffer size configuration
		///
		///@details Specifies whether the index buffer is limited to a 16-bit value
		///			    (65.535 unique indices). If true, treat index buffer as a uint16_t.
		bool index16;

    std::vector<vk::ClearValue> blanking;

    std::shared_ptr<RenderPassMeta> rpm;

		vk::IndexType GetIndexType();

    //... You sure there's no other way?
    template<typename Primitive, typename... Args, typename = std::enable_if_t<std::is_base_of<PrimitiveConfiguration,Primitive>::value>>
    void SetPrimitive(Args... cArgs){
      this->primitiveConfig.reset();
      this->primitiveConfig = std::make_unique<Primitive>(cArgs...);
    }

    void SetViewportConfiguration(
      uint32_t viewportCount,
      uint32_t scissorCount
    );

    virtual vk::Pipeline CreatePipeline(
      Device &dev,
      vk::PipelineLayout &layout,
      PushConstantLayout &pc_block
    ) override;
	};

};

#endif
