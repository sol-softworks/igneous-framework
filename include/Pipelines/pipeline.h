// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Pipeline Infrastructure
//
//	Description: Core Pipeline infrastructure for the Igneous Framework. Contains
//				 classes used for the management of pipeline objects and behaviours.

#ifndef IGNEOUS_PIPELINE
#define IGNEOUS_PIPELINE

#include <vector>
#include <map>

#include "vulkan/vulkan.hpp"

#include "resource.h"

#include "miniLog.h"

#include "Pipelines/pipeline_configuration.h"
#include "Conduit/pushconstant.h"
#include "Obsidian/material.h"
#include "Conduit/dsmeta.h"
#include "renderPass.h"
#include "renderTarget.h"

#include "drawcmd.h"

namespace Igneous{

	///@brief Pipeline Management Class
	///
	///@details Pipeline base class for custom pipeline creation.
	///
	///@details	This class drastically simplifies the process of custom
	///					pipeline implementation by standardizing the creation process.
	///					Most of the configuration process is exposed through the
	///					PipelineConfiguration struct instance. The values from the config
	///					instance are used during the pipeline compilation stage to create
	///					the pipeline.
	class Pipeline{
	private:

		//eGraphics, eCompute, eRayTracingNV
		vk::PipelineBindPoint pipelineType;

	protected:
		Device &_d;
		vk::PipelineCache cache;
		vk::PipelineLayout layout;
		vk::Pipeline pipeline;

		Device::TaskQueue::_Task executionTask;

		PushConstantLayout pc_block;
		std::vector<DescriptorLayoutMeta> layouts;

		uint32_t _subID;								//Subpass ID

	public:

		Pipeline(
			Device &dev,
			vk::PipelineBindPoint bindPoint
		);
		~Pipeline();

		//Add final specifier?
		///@brief Compiles Pipeline for use
		///
		///@details Finalizes pipeline configuration, and makes the pipeline usable
		///					for render operations. This routine must be re-run when most
		///					pipeline configuration options are modified.
		///
		///@param[in] rpmeta Render Pass metadata instance
		virtual void CompilePipeline(PipelineConfiguration &config);

		///@brief Start Pipeline Execution
		///
		///@details Initializes a Pipeline execution task for processing commands.
		virtual void FrameStart();

		///@brief Finish Pipeline Execution
		///
		///@details Finalizes Pipeline execution task, and submits the task to the
		///					central device queue manager.
		virtual void FrameEnd();

		///@brief Assign Materials to Current Decriptor Layout
		///
		///@details Assigns the values stored in each of the provided materials to
		///					their associated location in the pipeline descriptor layout.
		///
		///@param[in] materials Vector of materials which should be bound to the
		///						layout at this stage of pipeline execution.
		///
		///@warning Each provided Material must have been created by this pipeline.
		///				Invalid materials may result in unexpected behaviour.
		///
		///@note The provided materials should provide information for different
		///				layout positions. If duplicate positions are included, only the last
		///				material in the list of duplicates will be applied for that position.
		virtual void SetMaterials(
			std::vector<std::weak_ptr<Material>> const &materials
		);

		///@brief Retrieve Push Constant Reference
		///
		///@details Generates a PushConstantReference instance for the push constant
		///					block with the specified identifier.
		///
		///@tparam storedType Host-side type of the managed push constant. Both primitive
    ///         and user-defined types may be used.
		///
		///@param[in] id Block identifier
		///
		///@warning The specified type must match the type used to create the push
		///					constant block. Failure to do so will result in a
		///					std::bad_optional_access exception.
		template<typename pcType>
		PushConstantReference<pcType> GetPushConstant(
			std::string_view id
		){
			return this->pc_block.GetValue<pcType>(id).value();
		}

		///@brief Generate Material Resource
		///
		///@details Creates a new Material instance for the specified descriptor
		///					layout. Instances of this Material may be reused by multiple
		///					objects, but are only usable with the pipeline instance from
		///					which they were created.
		///
		///@returns std::shared_ptr<Material> to generated Material.
		///
		///@param[in] baseLayout Numerical ID of layout, as exposed by shader resources.
		///
		///@note baseLayout is consistent with the layout numbers outlined in shaders.
		virtual std::shared_ptr<Material> CreateMaterial(uint32_t baseLayout);

		//Overriden by derived classes
		virtual void PreConfigurePipeline();	///<Pre-configuration hook
	};
}

#endif
