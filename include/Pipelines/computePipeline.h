// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Graphics Pipeline Infrastructure
//
//	Description: Graphics pipeline infrastructure for the Igneous Framework.
//         Specializes the behaviour of Pipeline for graphics rendering tasks.

#ifndef IGNEOUS_PIPELINE_COMPUTE
#define IGNEOUS_PIPELINE_COMPUTE

#include "pipeline.h"

namespace Igneous{

  class ComputePipeline final : public Pipeline{

  public:
    ComputePipeline(Device &dev);
    //~ComputePipeline();

    ///@brief Start Pipeline Execution
    ///
    ///@details Initializes a Pipeline execution task for processing rendering
    ///					commands. The provided framebuffer will be used as the target
    ///					for all commands during this frame.
    ///
    ///@param[in] framebuffer Framebuffer to use for rendering composition
    virtual void FrameStart() override;
    virtual void FrameEnd() override;

    ///@brief Dispatch global workgroup
    ///
    ///@details Queues a global workgroup to execute the compute pipeline's
    ///         associated
    void Dispatch(
      uint32_t groupX,
      uint32_t groupY,
      uint32_t groupZ
    );
  };
}

#endif
