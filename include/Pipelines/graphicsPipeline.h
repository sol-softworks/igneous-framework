// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Graphics Pipeline Infrastructure
//
//	Description: Graphics pipeline infrastructure for the Igneous Framework.
//         Specializes the behaviour of Pipeline for graphics rendering tasks.

#ifndef IGNEOUS_PIPELINE_GFX
#define IGNEOUS_PIPELINE_GFX

#include "pipeline.h"

//#include "surface.h"
#include "viewport.h"

namespace Igneous{

  class GraphicsPipeline final : public Pipeline{

  public:

    GraphicsPipeline(Device &dev);
    //~GraphicsPipeline();

    ///@brief Start Pipeline Execution
    ///
    ///@details Initializes a Pipeline execution task for processing rendering
    ///					commands. The provided framebuffer will be used as the target
    ///					for all commands during this frame.
    ///
    ///@param[in] framebuffer Framebuffer to use for rendering composition
    virtual void FrameStart(
      RenderBuffer &framebuffer
    );
    virtual void FrameEnd() override;

    ///@brief Configure Viewports / Scissors
    ///
    ///@details Assigns the viewports and scissors defined in the provided
    ///         configuration to the render task.
    ///
    ///@param[in] config Viewport configuration struct from which to assign the
    ///           viewport and scissor definitions
    virtual void ConfigureViewport(
      ViewportConfiguration &config
    );

    ///@brief Record mesh draw command
    ///
    ///@details Adds a draw command to the render task. The command may be reused
    ///					in later invocations, as it is not consumed.\n
    ///					See the documentation for BaseDrawCommand for details on how to
    ///					use draw commands.
    virtual void RecordDraw(BaseDrawCommand &cmd);

    virtual void AssignBuffers();
    virtual void AssignBuffers(
      VulkanBuffer &VBO,
      VulkanBuffer &IBO,
      GraphicsPipelineConfiguration *config
    );

    static void ConfigureRenderPass(RenderPassGenerator &rpgen);
    static std::unique_ptr<GraphicsPipelineConfiguration> CreateConfiguration();
  };
}

#endif
