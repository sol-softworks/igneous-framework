// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Texture Resources
//
//	Description: Vulkan Texture abstraction layer

#ifndef IGNEOUS_TEXTURE_INT
#define IGNEOUS_TEXTURE_INT

#include <array>
#include <vector>
#include <tuple>
#include <unordered_map>
#include <memory>

#include "vulkan/vulkan.hpp"

#include "resource.h"
#include "managedTexture.h"

#include "miniLog.h"

namespace Igneous{

	///@brief Texture Resource
	///
	///@details Stores pixel data relevant to textures stored either as files
	///		or compiled into a binary asset format.
	template<uint32_t texDimensions>
	struct Texture{
	protected:
		typedef typename std::shared_ptr<Texture> _Tex;

	private:

		vk::ImageType _baseType;

		vk::Format _baseFormat;
		vk::ImageSubresourceRange _srRange;
		vk::ImageUsageFlags _usageFlags;
		vk::SampleCountFlagBits _samples;
		vk::Extent3D _dims;
		uint32_t _mipLevels;

		vk::ImageCreateInfo _ici;
		vk::ImageViewCreateInfo _ivci;
		vk::ImageLayout _currentLayout;

		std::vector<uint32_t> data;

		///@brief Data modification bitfield
		///
		///@details Bitfield representing whether image resource creation members have
		///         been modified since the last resource compilation. Implicitly
		///         convertible to bool, to simplify usage.
		///
		///@internal This data structure implements a more intelligent form of "dirty
		///           flag" state monitoring, where modifications are tracked at the
		///           per-field level.
		///           Explicitly occupies a single byte, whereas a bool's size is
		///           implementation defined, and may be larger.
		struct Modified{
			uint8_t format : 1;
			uint8_t srRange : 1;
			uint8_t uFlags : 1;
			uint8_t samples : 1;
			uint8_t dims : 1;
			uint8_t mips : 1;
			uint8_t data : 1;
			uint8_t layout : 1;

			operator bool() const{
				return
					this->format |
					this->srRange |
					this->uFlags |
					this->samples |
					this->dims |
					this->mips |
					this->data |
					this->layout;
			}
			void reset(){
				this->format = 0;
				this->srRange = 0;
				this->uFlags = 0;
				this->samples = 0;
				this->dims = 0;
				this->mips = 0;
				this->data = 0;
				this->layout = 0;
			}
		};

		Modified _dirty;				///<Does this texture need to be rebuilt?

		vk::ImageLayout _layout;	///<Current layout of this texture

		//In lieu of resource sharing across devices...
		std::map<std::reference_wrapper<Device>,ManagedTexture,std::less<Device>> _instances;

		Texture(vk::Extent3D extents, uint32_t mipLevels, uint32_t layers){
			vk::ImageType it = vk::ImageType::e2D;

			if constexpr(texDimensions == 1){
				it = vk::ImageType::e1D;
			}
			else if constexpr(texDimensions == 3){
				it = vk::ImageType::e3D;
			}

			this->_dims = extents;
			this->_mipLevels = mipLevels;

			this->_ici = {
				{},
				it,
				vk::Format::eUndefined,
				this->_dims,
				this->_mipLevels,
				//Textured Array Configuration
				1,
				vk::SampleCountFlagBits::e1,
				vk::ImageTiling::eOptimal,
				{},
				vk::SharingMode::eExclusive,
				0,
				nullptr,
				vk::ImageLayout::eUndefined
			};

			vk::ImageViewType vt = vk::ImageViewType::e1D;
			//Non-arrayed textures
			if(layers == 1){
				//1D is the default, so no need to re-check for 1D texture
				if(this->_ici.imageType == vk::ImageType::e2D) vt = vk::ImageViewType::e2D;
				else if(this->_ici.imageType == vk::ImageType::e3D) vt = vk::ImageViewType::e3D;
			}
			//Texture Arrays
			else{
				if(this->_ici.flags & vk::ImageCreateFlagBits::eCubeCompatible){
					if(layers == 6) vt = vk::ImageViewType::eCube;
					else vt = vk::ImageViewType::eCubeArray;
				}
				else{
					if(this->_ici.imageType == vk::ImageType::e1D) vt = vk::ImageViewType::e1DArray;
					else if(this->_ici.imageType == vk::ImageType::e2D) vt = vk::ImageViewType::e2DArray;
				}
			}

			this->_ivci = {
				{},
				nullptr,
				vt,
				vk::Format::eUndefined,
				{
          vk::ComponentSwizzle::eIdentity,
          vk::ComponentSwizzle::eIdentity,
          vk::ComponentSwizzle::eIdentity,
          vk::ComponentSwizzle::eIdentity,
        },
				vk::ImageSubresourceRange{{},0,mipLevels,0,layers}
			};
		}

		void _rebuild(){
			if(this->_dirty){
				this->_ici.format = this->_baseFormat;
				this->_ivci.format = this->_baseFormat;
				this->_ici.extent = this->_dims;
				this->_ici.samples = this->_samples;
				this->_ici.usage = this->_usageFlags;
				this->_ici.mipLevels = this->_mipLevels;
				this->_ivci.subresourceRange = this->_srRange;

				for(auto &tex : this->_instances){
					tex.second._rebuild(this->_ici);
					if(this->data.size() > 0){
						tex.second._setData(this->_ici.extent,this->data);
						this->_currentLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
					}
				}

				this->_dirty.reset();
			}
		}

	public:

		~Texture(){
			this->_instances.clear();
		}

		///@brief Create new texture
		///
		///@details Instantiates a new texture, creating a std::shared_ptr reference
		///					that may be used to alter the texture data at a later point.
		///
		///@param[in] texName Name to associate with this texture
		///
		///@param[in] pixelData Vector of individual pixel data to initialize the texture
		///
		///@param[in] dimensions Dimensions of the texture
		///
		///@param[in] mipLevels MIP levels that should be available for this texture
		///
		///@param[in] layers Number of texture layers in this texture
		static _Tex Create(std::string texName, std::array<uint32_t,texDimensions> dimensions, uint32_t mipLevels, uint32_t layers){
			//Extents from provided dimensions
			vk::Extent3D ex = {1,1,1};
			ex.setWidth(dimensions[0]);
			if constexpr(texDimensions > 1) ex.setHeight(dimensions[1]);
			if constexpr(texDimensions > 2) ex.setDepth(dimensions[2]);

			auto ret = std::shared_ptr<Texture>(new Texture{ex,mipLevels,layers});
			//common baseFormat: vk::Format::eR8G8B8A8Unorm
			//common aspectFlags: vk::ImageAspectFlagBits::eColor
			//common samples: vk::SampleCountFlagBits::e1;
			ret->SetFormat(vk::Format::eR8G8B8A8Unorm)
					.SetResources({vk::ImageAspectFlagBits::eColor,0,1,0,1})
					.SetSampleCount(vk::SampleCountFlagBits::e1);
			return ret;
		}

		///@brief Set texture pixel data
		///
		///@details Assign the provided pixel data to this texture, starting at the
		///					first pixel coordinate, and proceeding sequentially.
		///
		///@param[in] newData Vector of 32-bit integral pixel data to be assigned to
		///						the texture.
		///
		///@attention If the size of newData exceeds the linear size of the texture,
		///						only the pixels that fit within the image dimensions will be
		///						written.
		Texture& SetPixels(std::vector<uint32_t> newData){
			if(newData.size() <= this->data.size()){
				std::copy(newData.begin(),newData.end(),this->data.begin());
			}
			else{
				//std::copy(newData.begin(),newData.begin()+this->data.size()-1,this->data.begin());
				this->data.swap(newData);
			}
			this->_dirty.data = 1;
			return *this;
		}
		//TODO: Implement block-based pixel writing system?

		///@brief Set texture size
		///
		///@details Set the size of the texture along all dimensions. The new dimensions
		///					take effect immediately.
		///
		///@param[in] newDim Array of size texDimensions containing the new dimensions
		///						of the texture.
		Texture& SetSize(std::array<uint32_t,texDimensions> newDim){
			//Dirty checking makes this very painful...
			vk::Extent3D ex = {1,1,1};
			ex.setWidth(newDim[0]);
			if constexpr(texDimensions > 1) ex.setHeight(newDim[1]);
			if constexpr(texDimensions > 2) ex.setDepth(newDim[2]);

			if(this->_dims != ex){
				if(this->_ici.extent != ex) this->_dirty.dims = 1;
				else this->_dirty.dims = 0;
				this->_dims = ex;
			}
			return *this;
		}

		///@brief Set texture MIP level
		///
		///@details Set the MIP levels provided by this texture
		///
		///@param[in] mips MIP levels available for this texture.
		Texture& SetMipLevel(uint32_t mips){
			if(this->_mipLevels != mips){
				if(this->_ici.mipLevels != mips) this->_dirty.mips = 1;
				else this->_dirty.mips = 0;
				this->_mipLevels = mips;
			}
			return *this;
		}

		///@brief Create Image backing objects
    ///
    ///@details Convenience routine that sets all necessary parameters for a
    ///					texture to the specified values. Generally, this call will be
    ///					handled internally by the Texture class.
    ///
    ///@param[in] type Type of image (1D,2D,3D,etc.)
    ///
    ///@param[in] viewType Type of image view (1D,2D,3D,etc.)
    ///
    ///@param[in] dimensions Dimensions of the texture
    ///
    ///@param[in] mips MIP level used by this image
    ///
    ///@param[in] imageFormat Format used by the image
    ///
    ///@param[in] aspectFlags Aspects used by the image
    ///
    ///@param[in] samples Number of samples used by the image
    ///
    ///@note The `type` and `viewType` parameters need to be of similar "type",
    ///				in that a 2D type must also use a 2D view type. This is automatically
    ///				handled by the Texture class. A future iteration of the API may
    ///				opt for a simplified interface that codifies this requirement
    ///				automatically.
    ///
    ///@deprecated _createImage simply forwards the provided parameters to the
    ///       relevant Set_X_() routines, which provide more legible code. As
    ///       such, they should be used in favour of this method.
    [[deprecated(" Use the Set_X_() routines, as this method simply wraps them all into a single call ")]]
    void _createImage(vk::ImageType type, vk::ImageViewType viewType, vk::Extent3D dimensions, uint32_t mips, vk::Format imageFormat, vk::ImageAspectFlagBits aspectFlags, vk::SampleCountFlagBits samples);

    //Mutator API
    Texture& SetFormat(vk::Format format){
			if(this->_baseFormat != format){
				if(this->_ici.format != format) this->_dirty.format = 1;
				else this->_dirty.format = 0;
				this->_baseFormat = format;
			}
			return *this;
		}

    Texture& SetResources(vk::ImageSubresourceRange srRange){
			if(this->_srRange != srRange){
				if(this->_ivci.subresourceRange != srRange) this->_dirty.srRange = 1;
				else this->_dirty.format = 0;
				this->_srRange = srRange;
			}
			return *this;
		}

    Texture& SetUsageFlags(vk::ImageUsageFlags flags){
			if(this->_usageFlags != flags){
				if(this->_ici.usage != flags) this->_dirty.uFlags = 1;
				else this->_dirty.uFlags = 0;
				this->_usageFlags = flags;
			}
			return *this;
		}

    Texture& SetSampleCount(vk::SampleCountFlagBits count){
			if(this->_samples != count){
				if(this->_ici.samples != count) this->_dirty.samples = 1;
				else this->_dirty.uFlags = 0;
				this->_samples = count;
			}
			return *this;
		}

    Texture& SetExtents(vk::Extent3D extents){
			if(this->_dims != extents){
				if(this->_ici.extent != extents) this->_dirty.dims = 1;
				else this->_dirty.dims = 0;
				this->_dims = extents;
			}
			return *this;
		}

		Texture& SetLayout(vk::CommandBuffer &buffer, vk::ImageLayout newLayout){
			for(auto &inst : this->_instances){
				inst.second._transLayout(buffer,this->_layout,newLayout);
			}
			this->_currentLayout = newLayout;
			return *this;
		}

		//Since a Texture can be created on multiple devices, any attempts to use a
		//			image need to know the "device-path" for the correct instance. By
		//			design, almost any system that needs the backing image will also know
		//			the necessary device (e.g. Pipelines).
		ManagedTexture& GetTexture(Device &owner){
			//Flush all changes
			this->_rebuild();
			//				  			(std::pair<*std::pair<&Device,ManagedTexture>,bool>)
			//														||			*std::pair<&Device,ManagedTexture>
			//														||									||
			//														\/								  \/
			auto ret = this->_instances.try_emplace(owner,owner,this->_ivci).first;
			ret->second._rebuild(this->_ici);
			if(this->data.size() > 0) ret->second._setData(this->_ici.extent,this->data);
			return ret->second;
		}
	};
}

#endif
