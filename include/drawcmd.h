// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Draw Command
//
//	Description: Igneous Framework Property draw command structure

#ifndef IGNEOUS_DRAWCMD
#define IGNEOUS_DRAWCMD

#include "vulkan/vulkan.hpp"
#include "resource.h"
#include "buffer.h"

namespace Igneous{

  ///@brief Draw Command Interface
  ///
  ///@details Abstract class interface specifying the common functionality exposed
  ///         by all draw command variants.
  struct BaseDrawCommand{

    ///@brief Single-instance Draw
    ///
    ///@details Draws an object, using the buffers bound by the pipeline that
    ///         started the render task.
    virtual void Draw(vk::CommandBuffer &buffer) =0;

    ///@brief Instanced Draw
    ///
    ///@details Draws an object multiple times, with each instance using the
    ///         buffers bound by the pipeline that started the render task.
    virtual void DrawInstanced(vk::CommandBuffer &buffer, uint32_t instances, uint32_t firstInstanceID) =0;
  };

  ///@brief Vertex-based Draw Command
  ///
  ///@details Draw command that uses explicit vertex definitions to define the
  ///         draw calls.
  struct VertexDrawCommand : public BaseDrawCommand{
    uint32_t vertexCount;
    uint32_t vertexOffset;

    VertexDrawCommand(uint32_t vCount, uint32_t vOffset);

    virtual void Draw(vk::CommandBuffer &buffer) override;
    virtual void DrawInstanced(vk::CommandBuffer &buffer, uint32_t instances, uint32_t firstInstanceID) override;
  };

  ///@brief Index-based Draw Command
  ///
  ///@details Draw command that uses index-based references into a VBO to define
  ///         the draw calls.
  struct IndexedDrawCommand : public BaseDrawCommand{
    uint32_t indexCount;
    int32_t vertexOffset;
    uint32_t indexOffset;

    IndexedDrawCommand(uint32_t iCount, int32_t vOffset, uint32_t iOffset);

    virtual void Draw(vk::CommandBuffer &buffer) override;
    virtual void DrawInstanced(vk::CommandBuffer &buffer, uint32_t instances, uint32_t firstInstanceID) override;
  };

  ///@brief Vertex-based Indirect Draw Command
  ///
  ///@details Draw command that uses explicit vertex definitions, defined in an
  ///         auxiliary buffer, to define draw commands.
  ///
  ///@note Indirect draws allow the draw to be constructed on the device, which
  ///       can potentially reduce transfer bandwidth for large, repetitive draw
  ///       operations.
  struct IndirectDrawCommand : public BaseDrawCommand{
    VulkanBuffer commandSets;

    IndirectDrawCommand(Device &dev);

    virtual void Draw(vk::CommandBuffer &buffer) override;
    //Multi-Draw Indirect = Instanced Indirect Drawing
    virtual void DrawInstanced(vk::CommandBuffer &buffer, uint32_t instances, uint32_t offset) override;

    ///@brief Add Draw Command
    ///
    ///@details Adds a draw command to the auxiliary buffer.
    ///
    ///@param[in] cmd Command which defines the parameters to be used in the draw
    ///           call.
    ///
    ///@todo Currently a stub.
    void AddCommand(VertexDrawCommand &cmd);

    ///@brief Add Instanced Draw Command
    ///
    ///@details Adds an instanced draw command to the auxiliary buffer.
    ///
    ///@param[in] cmd Command which defines the parameters to be used in the draw
    ///           call.
    ///
    ///@todo Currently a stub.
    void AddCommandInstanced(VertexDrawCommand &cmd, uint32_t instances, uint32_t firstInstanceID);
  };

  ///@brief Index-based Indirect Draw Command
  ///
  ///@details Draw command that uses index-based references, defined in an
  ///         auxiliary buffer, to define draw commands.
  ///
  ///@note Indirect draws allow the draw to be constructed on the device, which
  ///       can potentially reduce transfer bandwidth for large, repetitive draw
  ///       operations.
  struct IndirectIndexedDrawCommand : public BaseDrawCommand{
    VulkanBuffer commandSets;

    IndirectIndexedDrawCommand(Device &dev);

    virtual void Draw(vk::CommandBuffer &buffer) override;
    //Multi-Draw Indirect = Instanced Indirect Drawing
    virtual void DrawInstanced(vk::CommandBuffer &buffer, uint32_t instances, uint32_t offset) override;

    ///@brief Add Draw Command
    ///
    ///@details Adds a draw command to the auxiliary buffer.
    ///
    ///@param[in] cmd Command which defines the parameters to be used in the draw
    ///           call.
    ///
    ///@todo Currently a stub.
    void AddCommand(IndexedDrawCommand &cmd);

    ///@brief Add Instanced Draw Command
    ///
    ///@details Adds an instanced draw command to the auxiliary buffer.
    ///
    ///@param[in] cmd Command which defines the parameters to be used in the draw
    ///           call.
    ///
    ///@todo Currently a stub.
    void AddCommandInstanced(IndexedDrawCommand &cmd, uint32_t instances, uint32_t firstInstanceID);
  };
}

#endif
