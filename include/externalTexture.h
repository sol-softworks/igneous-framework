// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Managed Texture Infrastructure
//
//	Description: Infrastructure for Igneous Framework Textures that reference an
//         externally-managed texture resource.

#ifndef IGNEOUS_TEXTURE_EXTERNAL
#define IGNEOUS_TEXTURE_EXTERNAL

#include "baseTexture.h"

namespace Igneous{

  ///@brief Texture with externally-managed image
  ///
  ///@details Interface for working with image resources that cannot be created
  ///         directly, such as those managed by a surface swapchain.
  class ExternalTexture final : public BaseTexture{

  public:

    ExternalTexture(Device &dev, vk::ImageViewCreateInfo &ivci, vk::Image &img);

    ///@brief Set image resource
    ///
    ///@details Assigns the provided backend image resource to the texture. The
    ///         previous image, if one exists, is not explicitly destroyed by
    ///         this method.
    ///
    ///@param[in] img Vulkan image resource to assign to this texture
    ///
    ///@note Previously assigned images are not destroyed by this method. An
    ///       external system will destroy the old image, if necessary. There
    ///       should be very few instances where images need to be explicitly
    ///       destroyed.
    void _setImage(vk::Image &img);
  };
}

#endif
