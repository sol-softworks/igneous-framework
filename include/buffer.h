// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Buffer Metadata
//
//	Description: Igneous Framework VulkanBuffer class and functionality intended
//				to provide a more intuitive interface to working with buffers. Abstracts
//        Vulkan internals behind methods that more clearly define their intended
//        usage.

#ifndef IGNEOUS_BUFFER
#define IGNEOUS_BUFFER

#include <vector>

#include "vulkan/vulkan.hpp"

#include "resource.h"

namespace Igneous{
  class VulkanBuffer{
  private:
    Device::mem_ptr bufferMemory;
    Device &device;
    vk::Buffer buffer;

  public:
    vk::BufferUsageFlags flags;
    vk::DeviceSize size;
    vk::MemoryPropertyFlags properties;

  public:
    VulkanBuffer(
      Device &device,
      vk::DeviceSize size,
      vk::BufferUsageFlags flags,
      vk::MemoryPropertyFlags properties
    ) :
      device(device),
      flags(flags),
      size(size),
      properties(properties)
      //_allocated(false)
    {
      this->Resize(size);
    }
    ~VulkanBuffer(){
      static_cast<vk::Device>(device).destroyBuffer(this->buffer,nullptr);
      device.free(this->bufferMemory);
    }

    //TODO: REPORT WHEN DATA IS LARGER THAN BUFFER!!!
    ///@brief Assign data vector to buffer
    ///
    ///@details Assigns the data stored in the provided vector to the managed
    ///         buffer. If the total size of the data exceeds that of the buffer,
    ///         the buffer is automatically resized to accommodate the data.
    ///
    ///@tparam eType Element type, deduced from passed data.
    ///
    ///@param[in] data Vector of values to be uploaded to the buffer.
    ///
    ///@note The automatic resizing process is not intended to remain in place,
    ///       as it leads to a potential issue with inadvertently exhausting
    ///       device memory if excessive reallocations occur. Instead, invoke
    ///       Resize() prior to assignment.
    template<typename eType>
    void Assign(std::vector<eType> data){
      size_t dSize = data.size()*sizeof(eType);
      if(dSize > this->size){
        this->Resize(dSize);
      }
      this->device.memcpy(this->bufferMemory,data.data(),dSize);
    }

    ///@brief Assign data structure to buffer
    ///
    ///@details Assigns the data stored in the provided data structure to the
    ///         managed buffer. If the size of the structure exceeds that of the
    ///         buffer, the buffer is automatically resized to accommodate the
    ///         data.
    ///
    ///@tparam eType Element type, deduced from passed data.
    ///
    ///@param[in] data Data structure with values to be uploaded to the buffer.
    ///
    ///@note The automatic resizing process is not intended to remain in place,
    ///       as it leads to a potential issue with inadvertently exhausting
    ///       device memory if excessive reallocations occur. Instead, invoke
    ///       Resize() prior to assignment.
    template<typename eType>
    void Assign(eType &data){
      size_t dSize = sizeof(eType);
      if(dSize > this->size){
        this->Resize(dSize);
      }
      this->device.memcpy(this->bufferMemory,&data,dSize);
    }

    void _debugReadValue(size_t index){
      std::vector<uint32_t> data(this->size);
      this->device.memcpy(data.data(),this->bufferMemory,this->size);
      ConsoleLog(LogType::DEBUG,"[VB] Buffer value at index %u: %x\n",index,data[index]);
    }

    void _debugReadRange(size_t start, size_t length){
      std::vector<uint32_t> data(this->size);
      this->device.memcpy(data.data(),this->bufferMemory,this->size);
      ConsoleLog(LogType::DEBUG,"[VB] Buffer values:\n");
      for(size_t i=start; i<start+length; ++i){
        ConsoleLog(LogType::DEBUG,"[VB]   - index %u: %x\n",i,data[i]);
      }
    }

    ///@brief Resize managed buffer
    ///
    ///@details Resize the internally managed buffer to accommodate the specified
    ///         bytecount.
    ///
    ///@param newSize New size of the buffer.
    ///
    ///@attention newSize is a value represented in bytes. If manually invoking
    ///           this method, ensure the newSize reflects your intent: i.e. it's
    ///           not just vector.size(); it's vector.size() * sizeof(dataType)
    void Resize(vk::DeviceSize newSize){
      if(this->bufferMemory){
        //Are there actually cases where Buffers will be shared across threads?
        static_cast<vk::Device>(this->device).destroyBuffer(this->buffer,nullptr);
        device.free(this->bufferMemory);
      }
      this->size = newSize;
      this->buffer = static_cast<vk::Device>(this->device).createBuffer(
        {vk::BufferCreateFlags(),newSize,this->flags,vk::SharingMode::eExclusive,0,nullptr},
        nullptr);
      this->bufferMemory = this->device.balloc(this->buffer,this->properties);
    }

    ///@brief Internal Buffer Accessor (Implicit-cast)
    ///
    ///@details Allows direct access to the internal buffer. Intended for use by
    ///         low-level Vulkan functions that require a buffer.
    operator vk::Buffer() const{
      return this->buffer;
    }

    operator const vk::Buffer*() const{
      return &this->buffer;
    }
  };
}

#endif
