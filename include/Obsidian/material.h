// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Material System (Obsidian)
//
//	Description: Igneous Framework Material classes and functionality for simplified
//				management of Vulkan textures. Allows multiple textures to be treated
//				as a single entity for application on mesh objects.

#ifndef IGNEOUS_MATERIAL
#define IGNEOUS_MATERIAL

#include <memory>
#include <map>
#include <string>

#include "Obsidian/matproperties.h"
#include "Conduit/dsmeta.h"

namespace Igneous{

	///@brief Obsidian Material Class
	///
	///@details Obsidian interface to Descriptor Set binding management. Materials
	///					function as wrappers around the DescriptorSetMeta class, with
	///					individual bindings exposed as MaterialProperty instances.
	class Material{

	protected:

		std::weak_ptr<DescriptorSetMeta> dsm;			///<Descriptor Set for this material.

	public:
		std::map<std::string,std::unique_ptr<Property>> properties;		///<Properties exposed by this material.
		Material(DescriptorLayoutMeta &setLayout);
		~Material();

		///@brief Retrieve Material Descriptor Set
		///
		///@details Prepares the Material's descriptor set for binding during
		///					pipeline execution. If necessary, the internal DescriptorSetMeta
		///					updates binding data prior to returning the descriptor set.
		///
		///@returns vk::DescriptorSet Set provided by this material.
		vk::DescriptorSet PrepareDescriptorSet();

		///@brief Retrieve Casted Property Wrapper
		///
		///@details Convenience wrapper that attempts to cast the requested property
		///					to the specified type.
		///
		///@tparam propType Property type to attempt to cast a property into.
		///
		///@param[in] id Identifier of property to retrieve.
		///
		///@warning If the requested property is not of the requested type, a nullptr
		///					will be returned.
		template<typename propType, typename = std::enable_if_t<std::is_base_of<Property,propType>::value>>
		propType* GetProperty(std::string id){
			try{
				auto &prop = this->properties.at(id);
				return prop->as<propType>();
			}
			catch(std::out_of_range &e){
				ConsoleLog(LogType::ERR,"[IG:OBS] - No property with id %s found.\n",id.c_str());
				return nullptr;
			}
		}

		///@brief Retrieve Texture Property Wrapper
		///
		///@details Convenience wrapper that attempts to cast the requested property
		///					to a TextureProperty.
		///
		///@param[in] id Identifier of property to retrieve.
		///
		///@warning If the requested property is not a texture or sampler resource,
		///					a nullptr will be returned.
		TextureProperty* GetTexture(std::string id);

		///@brief Retrieve Buffer Property Wrapper
		///
		///@details Convenience wrapper that attempts to cast the requested property
		///					to a BufferProperty.
		///
		///@param[in] id Identifier of property to retrieve.
		///
		///@warning If the requested property is not a buffer resource a nullptr will
		///					be returned.
		BufferProperty* GetBuffer(std::string id);
	};
}

#endif
