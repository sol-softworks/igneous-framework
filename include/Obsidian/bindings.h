// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Descriptor Set Metadata Infrastructure
//
//	Description: Descriptor Set Metadata infrastructure for the Igneous Framework.
//         Stores relevant descriptor set information stored in SPIRV source, to
//         facilitate intelligent creation of related descriptor set instances.
//
//         This system has been extended to provide metadata required by the
//         Igneous Material System (Obsidian) to easily generate Materials based
//         on the properties used in a GraphicsPipeline

#ifndef IGNEOUS_OBSIDIAN_BINDINGS
#define IGNEOUS_OBSIDIAN_BINDINGS

#include "vulkan/vulkan.hpp"
#include "texture_internal.h"
#include "samplerMeta.h"
#include "buffer.h"

namespace Igneous{

  ///@brief Binding Update Manager
  ///
  ///@details Binding update structure allowing bindings to specify their data
  ///         update requirements. A reference to this struct is stored by each
  ///         binding, along with an integer that defines their binding index.
  class BindUpdates{
  private:
    uint32_t updateBits;    ///<Binding update declarations (bitfield)

  public:
    ///@brief Mark binding for update
    ///
    ///@details Requests that the binding with the specified index be updated
    ///         during the next update pass.
    ///
    ///@param[in] index Index of the binding to be updated
    void set(uint32_t index){
      this->updateBits |= (1 << index);
    }

    ///@brief Unmark binding for update
    ///
    ///@details Requests that the binding with the specified index be ignored
    ///         during the next update pass.
    ///
    ///@param[in] index Index of the binding to be ignored
    void clear(uint32_t index){
      this->updateBits &= (-1 ^ (1 << index));
    }

    ///@brief Clear binding updates
    ///
    ///@details Resets the binding update structure, marking all bindings as
    ///         ignored during the next update cycle
    void reset(){
      this->updateBits = 0;
    }

    ///@brief Determine update state
    ///
    ///@details Checks whether the binding with the specified index is marked for
    ///         update
    ///
    ///@param[in] index Index of the binding to be checked
    bool isSet(uint32_t index){
      return (this->updateBits & (1 << index));
    }

    ///@brief Determine overall update state
    ///
    ///@details Checks whether any monitored binding has queued updates.
    operator bool(){return updateBits;}
  };

  ///@brief Descriptor Set Binding Base Class
  ///
  ///@details Provides a common interface to descriptor set binding data management.
  ///         Binding data updates are implemented such that only populated values
  ///         are included in an update.
  ///
  ///@note BaseBinding only contains the highly-generic interface used by
  ///       DescriptorSetMeta for bindings, as the overall update process is
  ///       independent of the internal mechanisms required for each binding.
  struct BaseBinding{

  protected:
    vk::DescriptorSetLayoutBinding &binding;      ///<DescriptorLayoutMeta binding definition

  public:
    BaseBinding(vk::DescriptorSetLayoutBinding &_binding);
    virtual ~BaseBinding();

    ///@brief Generate Binding Update Information
    ///
    ///@details Produces an update structure used to specify the values to be
    ///         updated, including their new values.
    ///
    ///@note The base implementation only populates the binding type and destination
    ///       with specific values. Derived bindings are responsible for setting
    ///       the remaining fields to appropriate values.
    ///
    ///@internal In derived classes, invoke BaseBinding's implementation prior
    ///           to any binding-specific behaviour, to ensure consistent
    ///           initialization behaviour.
    virtual vk::WriteDescriptorSet CreateWriteInfo();

    ///@brief Layout Binding Accessor (Implicit cast)
    virtual operator vk::DescriptorSetLayoutBinding() const;

    ///@brief Binding Type Accessor (Implicit cast)
    virtual operator vk::DescriptorType() const;

    //Temporary friends
    friend struct DescriptorSetMeta;
  };

  ///@name Bind Data Types
  ///@{

  ///@brief Image Resource Generic Binding Data
  ///
  ///@details Common resource data used by all bindings with image-related usage.
  ///
  ///@internal diInfos exists to persist update information long enough to allow
  ///           the late update process used in Obsidian to reduce driver overhead.
  struct ImageBindingData{
  private:
    Device* _dev;
    std::vector<vk::DescriptorImageInfo> diInfos;
    BindUpdates &_updates;      ///<Reference to binding update manager
    const size_t _bindIndex;    ///<Binding Index for the update manager

  protected:
    std::vector<std::shared_ptr<vk::ImageView>> views;
    std::vector<std::shared_ptr<vk::Sampler>> samplers;

    ///@brief Assign Data to Update Structure
    ///
    ///@details Provides update information for the data managed by this binding.
    ///         Invoked automatically by binding, if an update is required.
    virtual void SetBindData(vk::WriteDescriptorSet &writeInfo){
      this->diInfos.clear();
      for(size_t i = 0; i<this->size; ++i){
        if(this->samplers.at(i) || this->views.at(i)){
          diInfos.emplace_back(
            vk::DescriptorImageInfo{
              (this->samplers[i]) ? *this->samplers[i] : nullptr,
              (this->views[i]) ? *this->views[i] : nullptr,
              vk::ImageLayout::eGeneral
            }
          );
        }
      }
      writeInfo.descriptorCount = diInfos.size();
      writeInfo.pImageInfo = diInfos.data();
    }

  public:
    const size_t size;        ///<Maximum number of elements in the referenced binding.

    ///@brief Assign Texture to Binding Data
    ///
    ///@details Generates a new ImageView for the provided Texture resource,
    ///         storing the view internally for usage by this descriptor binding.
    ///
    ///@tparam texSize Extent of texture resource. Deduced based on provided
    ///         Igneous::Texture
    ///
    ///@param[in] newTex Igneous::Texture resource to use for this binding
    ///
    ///@param[in] index Array index to which this image will be assigned. Must
    ///           be in range [0,descriptorCount)
    template<uint32_t texSize>
    void SetTexture(Texture<texSize> &newTex, uint32_t index){
      if(index >= this->size){
        return;
      }
      this->views.at(index).reset();
      auto newElem = newTex.GetTexture(*this->_dev).CreateView();
      this->views.at(index).swap(newElem);
      this->_updates.set(this->_bindIndex);
    }

    ///@brief Assign Sampler to Binding Data
    ///
    ///@details Assigns the provided sampler to the Texture resource at the
    ///         specified index.
    ///
    ///@param[in] newSamp Igneous::SamplerMeta resource to use for this binding
    ///
    ///@param[in] index Array index to which this sampler will be assigned. Must
    ///           be in range [0,descriptorCount)
    void SetSampler(std::shared_ptr<SamplerMeta> &newSamp, uint32_t index = 0){
      if(index >= this->size){
        return;
      }
      this->samplers.at(index).reset();
      auto newElem = newSamp->GetSampler(*this->_dev);
      this->samplers.at(index).swap(newElem);
      this->_updates.set(this->_bindIndex);
    }

    ImageBindingData(size_t size, BindUpdates &_updater, size_t bindIndex);

    friend struct DescriptorSetMeta;
  };

  ///@brief Buffer Resource Generic Binding Data
  ///
  ///@details Common resource data used by all bindings with buffer-related usage.
  ///
  ///@internal dbInfos exists to persist update information long enough to allow
  ///           the late update process used in Obsidian to reduce driver overhead.
  struct BufferBindingData{
  private:
    Device* _dev;
    std::vector<vk::DescriptorBufferInfo> dbInfos;

    BindUpdates &_updates;      ///<Reference to binding update manager
    const size_t _bindIndex;    ///<Binding Index for the update manager

  protected:
    std::vector<std::unique_ptr<VulkanBuffer>> buffers;

    ///@brief Assign Data to Update Structure
    ///
    ///@details Provides update information for the data managed by this binding.
    ///         Invoked automatically by Binding, if an update is required.
    virtual void SetBindData(vk::WriteDescriptorSet &writeInfo){
      this->dbInfos.clear();
      for(size_t i = 0; i<this->size; ++i){
        if(this->buffers.at(i)){
          dbInfos.emplace_back(
            vk::DescriptorBufferInfo{
              *this->buffers[i],
              0,
              this->buffers[i]->size
            }
          );
        }
      }
      writeInfo.descriptorCount = dbInfos.size();
      writeInfo.pBufferInfo = dbInfos.data();
    }

  public:
    const size_t size;        ///<Maximum number of elements in the referenced binding.

    ///@brief Assign Texture to Binding Data
    ///
    ///@details Assigns the data contained in the provided user-defined data
    ///         structure to the buffer located at the specified index. If the
    ///         buffer doesn't exist, this method will attempt to create one, and
    ///         assign the contents to the new buffer.
    ///
    ///@tparam contentType User-designated type which defines data to be
    ///         transferred into a buffer
    ///
    ///@param[in] contents User-defined data structure which contains the data
    ///           to be copied to the specified buffer
    ///
    ///@param[in] index Array index to which this data will be assigned. Must
    ///           be in range [0,descriptorCount)
    template<typename contentType>
    void SetData(contentType &contents, uint32_t index = 0){
      if(index < this->size){
        if(auto &elem = this->buffers.at(index)) elem->Assign(contents);
        else{
          this->CreateBuffer<contentType>(index);
          this->buffers.at(index)->Assign(contents);
        }
      }
    }

    ///@brief Assign Texture to Binding Data
    ///
    ///@details Generates a new buffer, with sizing information determined by the
    ///         size of a user-specified data structure. If a buffer already
    ///         exists at the specified index, resize the buffer.
    ///
    ///@tparam contentType User-designated type which defines data to be
    ///         transferred into a buffer. Dictates the allocation size of the
    ///         buffer.
    ///
    ///@param[in] index Array index to which the new buffer will be assigned.
    ///           Must be in range [0,descriptorCount)
    template<typename contentType>
    void CreateBuffer(uint32_t index = 0){
      if(index >= this->size){
        return;
      }
      if(!this->buffers[index]){
        this->buffers[index] = std::make_unique<VulkanBuffer>(
          *this->_dev,
          sizeof(contentType),
          vk::BufferUsageFlags{vk::BufferUsageFlagBits::eUniformBuffer},
          vk::MemoryPropertyFlags{vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent}
        );
      }
      else{
        //VB is resizable, so just reuse the existing buffer
        this->buffers[index]->Resize(sizeof(contentType));
      }
      this->_updates.set(this->_bindIndex);
    }

    BufferBindingData(size_t size, BindUpdates &_updater, size_t bindIndex);

    friend struct DescriptorSetMeta;
  };

  ///@}

  ///@brief Static-size Description Binding
  ///
  ///@details Base class for Igneous binding classes that do not use dynamic size
  ///         information. Most bindings fit into this category, but custom
  ///         bindings may be defined that require specialized behaviour.
  ///
  ///@tparam dataType BindData type used by this binding instance. Provided class
  ///         must expose a SetBindData method, and use a BindUpdates manager
  ///         reference as part of its construction.
  ///
  ///@internal Unless absolutely necessary, use this class for new bind types.
  template<typename dataType>
  struct StaticSizeBinding : public BaseBinding, public dataType{

    StaticSizeBinding(
      vk::DescriptorSetLayoutBinding &_binding,
      BindUpdates &updater
    ) : BaseBinding(_binding),
    dataType(_binding.descriptorCount,updater,_binding.binding)
    {

    }

    ///@brief Generate Binding Update Information
    ///
    ///@details Produces an update structure used to specify the values to be
    ///         updated, including their new values. If a binding does not need
    ///         to be updated, the descriptorCount field of the returned struct
    ///         will be zero (0).
    virtual vk::WriteDescriptorSet CreateWriteInfo(){
      auto writeInfo = BaseBinding::CreateWriteInfo();
      this->SetBindData(writeInfo);
      return writeInfo;
    }
  };


  struct CombinedSampleImageBinding :
    public StaticSizeBinding<ImageBindingData>
  {
    CombinedSampleImageBinding(
      vk::DescriptorSetLayoutBinding &_binding,
      BindUpdates &_updater
    ) : StaticSizeBinding<ImageBindingData>(_binding,_updater){

    }

    static const vk::DescriptorType Type = vk::DescriptorType::eCombinedImageSampler;
  };

  struct SampledImageBinding :
    public StaticSizeBinding<ImageBindingData>
  {
    SampledImageBinding(
      vk::DescriptorSetLayoutBinding &_binding,
      BindUpdates &_updater
    ) : StaticSizeBinding<ImageBindingData>(_binding,_updater){

    }

    static const vk::DescriptorType Type = vk::DescriptorType::eSampledImage;
  };

  struct SamplerBinding :
    public StaticSizeBinding<ImageBindingData>
  {
    SamplerBinding(
      vk::DescriptorSetLayoutBinding &_binding,
      BindUpdates &_updater
    ) : StaticSizeBinding<ImageBindingData>(_binding,_updater){

    }

    static const vk::DescriptorType Type = vk::DescriptorType::eSampler;
  };

  struct BufferBinding :
    public StaticSizeBinding<BufferBindingData>
  {

  public:
    BufferBinding(
      vk::DescriptorSetLayoutBinding &_binding,
      BindUpdates &_updater
    ) : StaticSizeBinding<BufferBindingData>(_binding,_updater){

    }

    static const vk::DescriptorType Type = vk::DescriptorType::eUniformBuffer;
  };
}

#endif
