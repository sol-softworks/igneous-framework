// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Material Properties
//
//	Description: Obsidian Property-derived classes, which provide convenient
//         interfaces for modifying various descriptor type values stored in
//         associated Materials.

#ifndef IGNEOUS_OBSIDIAN_PROPERTIES
#define IGNEOUS_OBSIDIAN_PROPERTIES

#include "property.h"

#include "Conduit/dsmeta.h"
#include "resource.h"
#include "baseTexture.h"
#include "samplerMeta.h"
#include "texture_internal.h"

namespace Igneous{

  ///@brief Material Property Base
  ///
  ///@details Base type for Obsidian properties. Stores a weak_ptr reference to
  ///         the descriptor set binding which the associated property implements.
  ///         Derived types are responsible for casting the binding to the
  ///         appropriate type when assigning values to the binding, as BaseBinding
  ///         doesn't have an interface for data assignment.
  class MaterialProperty : public Property{
  protected:
    std::weak_ptr<BaseBinding> _binding;    ///<Binding referenced by this property

  public:

    MaterialProperty(
      std::string name,
      std::shared_ptr<BaseBinding> binding
    ) : Property(name),
      _binding(binding)
    {}

    virtual ~MaterialProperty(){}
  };

  class TextureProperty : public MaterialProperty{

  public:

    TextureProperty(
      std::string name,
      std::shared_ptr<BaseBinding> binding
    ) : MaterialProperty(name,binding){
    }

    ~TextureProperty(){}

    ///@brief Assign Sampler to Property
    ///
    ///@details Assigns the provided sampler to the image resource located at
    ///         the specified index in the binding data.
    ///
    ///@param[in] index Array element which should use the provided sampler.
    ///
    ///@param[in] newSampler SamplerMeta which should be used to construct a
    ///           sampler for the associated image.
    ///
    ///@note Descriptor Layout Bindings are (generally) fixed-size concepts.
    ///       Attempting to address an element outside the range defined by the
    ///       binding is an invalid operation, <b>and will be silently ignored.</b>
    virtual void SetSampler(uint32_t index, std::shared_ptr<SamplerMeta> newSampler){
      //In principle, this should always work.
      if(auto casted = dynamic_cast<ImageBindingData*>(this->_binding.lock().get())){
        casted->SetSampler(newSampler, index);
      }
    }

    ///@overload
    ///@details Convenience implementation that swaps the order of parameters.
    virtual void SetSampler(std::shared_ptr<SamplerMeta> newSampler, uint32_t index = 0){
      this->SetSampler(index, newSampler);
    }

    ///@brief Assign Texture to Property
    ///
    ///@details Constructs a new ImageView for the provided Texture resource, and
    ///         assigns the new view to the image resource located at the
    ///         specified index in the binding data.
    ///
    ///@param[in] index Array element which should use the generate image view.
    ///
    ///@param[in] newTex Texture resource to be assigned to the binding.
    ///
    ///@note Descriptor Layout Bindings are (generally) fixed-size concepts.
    ///       Attempting to address an element outside the range defined by the
    ///       binding is an invalid operation, <b>and will be silently ignored.</b>
    template<uint32_t texSize>
    void SetTexture(uint32_t index, Texture<texSize> &newTex){
      //In principle, this should always work.
      if(auto casted = dynamic_cast<ImageBindingData*>(this->_binding.lock().get())){
        casted->SetTexture(newTex, index);
      }
    }

    ///@overload
    ///@details Convenience implementation that swaps the order of parameters.
    template<uint32_t texSize>
    void SetTexture(Texture<texSize> &newTex, uint32_t index = 0){
      this->SetTexture(index, newTex);
    }
  };

  class BufferProperty : public MaterialProperty{

  public:
    BufferProperty(
      std::string name,
      std::shared_ptr<BaseBinding> binding
    ) : MaterialProperty(name,binding)
    {

    }

    ///@brief Assign Contents to Buffer
    ///
    ///@details Assign the contents of the provided structure to the buffer
    ///         at the specified index in the binding.
    ///
    ///@tparam contentType User-defined data type for the provided data. Deduced
    ///         from the passed contents.
    ///
    ///@param[in] newContents Structure containing data to be copied into the
    ///           buffer.
    ///
    ///@param[in] index Array index for the buffer which will have its contents
    ///           assigned to the provided value.
    ///
    ///@note Descriptor Layout Bindings are (generally) fixed-size concepts.
    ///       Attempting to address an element outside the range defined by the
    ///       binding is an invalid operation, <b>and will be silently ignored.</b>
    ///
    ///@attention Buffers are internally managed as VulkanBuffer instances, which
    ///           assign data to the buffer through a low-level memcpy operation.
    ///           Make sure the provided data type is both the correct size, and
    ///           has an identical memory layout to the intended buffer usage.
    ///
    ///@warning This method does not perform lazy-initialization of buffers. You
    ///         are responsible for creating the buffer prior to calling this
    ///         function.
    ///
    ///@internal Re. warning: Maybe assign SHOULD perform lazy-init? It'd reduce
    ///           overall interface complexity, and avoid the nullptr issue with
    ///           invalid buffers.
    template<typename contentType>
    void Assign(contentType &newContents, uint32_t index = 0){
      //In principle, this should always work.
      if(auto casted = dynamic_cast<BufferBindingData*>(this->_binding.lock().get())){
        casted->SetData(newContents, index);
      }
    }

    ///@brief Create New Buffer
    ///
    ///@details Allocates a new VulkanBuffer instance at the specified index,
    ///         using a data structure type for sizing information.
    ///
    ///@tparam contentType Type used to determine sizing requirements for the
    ///         buffer allocation.
    ///
    ///@param[in] index Array index for the buffer which will be allocated.
    ///
    ///@note Descriptor Layout Bindings are (generally) fixed-size concepts.
    ///       Attempting to address an element outside the range defined by the
    ///       binding is an invalid operation, <b>and will be silently ignored.</b>
    template<typename contentType>
    void Create(uint32_t index = 0){
      //In principle, this should always work.
      if(auto casted = dynamic_cast<BufferBindingData*>(this->_binding.lock().get())){
        ConsoleLog(LogType::DEBUG,"[Property] Sizeof content: %u\n",sizeof(contentType));
        casted->CreateBuffer<contentType>(index);
      }
    }

    /*
    Are there instances where Materials should be able to resize a uniform buffer?
    virtual void Resize(uint32_t newSize){
      this->value->Resize(newSize);
    }
    */
  };
};

#endif
