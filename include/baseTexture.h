// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework BaseTexture Infrastructure
//
//	Description: Core Texture infrastructure for the Igneous Framework. Contains
//				 classes used for the management of image resources.

#ifndef IGNEOUS_BASETEXTURE
#define IGNEOUS_BASETEXTURE

#include "vulkan/vulkan.hpp"
#include "resource.h"

namespace Igneous{

  ///@brief Vulkan Image resource management struct
  ///
  ///@details Struct used to centralize the management of Vulkan resources
  ///					required for all image-based constructs.
  ///
  ///					Most applications will not need to manually store instances of
  ///					this class, as they are managed internally by the pipeline
  ///					infrastructure.
  ///
  struct BaseTexture{
  private:
    std::forward_list<std::weak_ptr<vk::ImageView>> _views;

    //... Necessary due to CreateView()
    vk::ImageViewCreateInfo &_ivci;

  protected:
    Device &dev;
    vk::Image _img;

  public:

    typedef std::shared_ptr<vk::ImageView> view;

    BaseTexture(
      Device &dev,
      vk::ImageViewCreateInfo &ivci
    );

    virtual ~BaseTexture();

    ///@brief Transition Image Layout
    ///
    ///@details Transitions the layout of the backing image. This is required
    ///					when writing data to the image, but more generally whenever
    ///					there is a need to transition the image to a more suitable
    ///					layout for a particular use case.
    ///
    ///@param[in] buf Command buffer which will record the transition.
    ///
    ///@param[in] oldLayout Current layout used by the image
    ///
    ///@param[in] newLayout Target layout for the image
    ///
    ///@internal Only useful for self-managed resources
    virtual void _transLayout(
      vk::CommandBuffer &buf,
      vk::ImageLayout oldLayout,
      vk::ImageLayout newLayout
    );

    ///@brief Rebuild Constructed Views
    ///
    ///@details Rebuild all image views created from the backing resources. This
    ///         may be used to force reconstruction of views without requiring
    ///         the backing image resource to be rebuilt.
    virtual void _rebuildViews();

    ///@brief Set image data
    ///
    ///@details Sets the texel data for the backing image. An internal staging
    ///					buffer is used to handle the transfer, and ensure appropriate
    ///					operation under most circumstances.
    ///
    ///@param[in] pixelData Data to be assigned to the image buffer.
    ///
    ///@internal This method can go either way: self-managed resources definitely
    ///           need this method, but it's possible externally-managed resources
    ///           can also use this. If so, this stays in BT.
    virtual void _setData(
      vk::Extent3D &extent,
      std::vector<uint32_t> &pixelData
    );

    ///@brief Output pixel data
    ///
    ///@details Prints pixel values at the specified range to console.
    ///
    ///@param range Extents which define the region from which to read values.
    ///
    ///@param offset Offset into image region.
    ///
    ///@note As implemented, this routine is only useful for internal debugging.
    ///       Separate functions will need to be implemented to allow more broadly
    ///       useful reads from texture resources.
    virtual void _debugReadData(
      vk::Extent3D const &range,
      vk::Offset3D const &offset
    );

    //Image View API
    ///@brief Create New ImageView for Texture
    ///
    ///@details Creates a new ImageView resource that works with the managed
    ///         Image resource.
    ///
    ///@return std::shared_ptr<vk::ImageView> to generated view.
    virtual view CreateView();
  };
}

#endif
