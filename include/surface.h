// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Surface Management System
//
//	Description: Vulkan Surface abstraction layer.

#ifndef IGNEOUS_SURFACE
#define IGNEOUS_SURFACE

#include <vector>

#include <functional>
#include <optional>

#include "vulkan/vulkan.hpp"

#include "viewport.h"
#include "Pipelines/graphicsPipeline.h"
#include "resource.h"
#include "renderTarget.h"
#include "renderPass.h"

namespace Igneous{

	class WindowSurface{

		vk::SurfaceKHR surface;
		vk::SwapchainKHR swapchain;

		std::vector<RenderTarget> backbuffer;
		std::vector<RenderBuffer> framebuffers;

		std::shared_ptr<RenderPassMeta> rpm;

		Device &_dev;
		vk::SurfaceCapabilitiesKHR capabilities;
		uint32_t swapchainSize;

		vk::Format format;
		vk::ColorSpaceKHR colorSpace;

		vk::Extent2D fbDims;
		ViewportConfiguration viewports;

		uint32_t activeSwapchainImage;

	public:
		WindowSurface(Device &dev, std::function<void(void*,vk::SurfaceKHR&)> surfGen, void* genData);
		~WindowSurface();

		void resizeFramebuffer(int width, int height);

		void SetBufferAttachmentUniform(
			std::string id,
			BaseTexture &attachSource
		);
		void SetBufferAttachment(
			uint32_t bufferIndex,
			std::string id,
			BaseTexture &attachSource
		);

		void SetAttachmentClearValueUniform(
			std::string id,
			vk::ClearValue value
		);
		void SetAttachmentClearValue(
			uint32_t bufferIndex,
			std::string id,
			vk::ClearValue value
		);

		void SetRenderPass(std::shared_ptr<RenderPassMeta> rpmeta);

		std::optional<RenderBuffer> Prepare();
		void Present();

		void Synchronize();

		void AddSurfaceAttachment(RenderPassGenerator &rpg);
		void ConfigureViewports(std::shared_ptr<GraphicsPipeline> &pipeline);
	};
}

#endif
