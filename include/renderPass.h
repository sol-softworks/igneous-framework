// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Render Pass Infrastructure
//
//	Description: Core infrastructure for RenderPass management. Contains
//				 classes used for the creation and management of data related to
//         rendering tasks.

#ifndef IGNEOUS_RENDERPASS
#define IGNEOUS_RENDERPASS

#include <vector>
#include <map>
#include <utility>

#include "vulkan/vulkan.hpp"

#include "baseTexture.h"

namespace Igneous{

  ///@brief Render Pass Instance Metadata
  ///
  ///@details Metadata interface for managing the attachments associated with
  ///         a render pass. Also performs initialization of entities that provide
  ///         attachment data.
  class RenderPassMeta{
  private:
    vk::RenderPass rp;

    ///@todo Implement attachment validation system, which wraps the full dataset
    ///       of <Name,Index,Description> into a single struct that verifies
    ///       whether an assigned attachment value fulfills all requirements.
    std::map<std::string, uint32_t> _attach_map;
    std::vector<vk::AttachmentDescription> _attach_defs;

  public:
    RenderPassMeta(
      vk::RenderPass &pass,
      std::vector<vk::AttachmentDescription> &attachment_definitions,
      std::map<std::string,uint32_t> &id_mapping
    );

    ///@brief Initialize Attachment Map
    ///
    ///@details Initializes the provided map with default-initialized (empty)
    ///         BaseTexture::view instances, using the AttachName component of
    ///         AttachID as keys.
    ///
    ///@param[in,out] atts Map of attachments to be initialized.
    ///
    ///@param[in,out] id_mapping Map of attachment name / index
    void InitializeAttachments(
      std::map<uint32_t,std::pair<BaseTexture::view,vk::ClearValue>> &atts,
      std::map<std::string,uint32_t> &id_mapping
    );

    operator vk::RenderPass() const;

    friend struct RenderPassDeleter;
  };

  ///@brief	Internal RP Factory
  ///
  ///@details	Class used to simplify the creation of RenderPass entities for
  ///					pipeline configuration. Provides a more intuitive interface for
  ///         defining attachments, subpasses, and dependencies.
  class RenderPassGenerator{
  private:
    struct Subpass{
      std::vector<vk::AttachmentReference> _inputs;
      std::vector<vk::AttachmentReference> _colors;
      std::vector<vk::AttachmentReference> _resolves;
      std::vector<uint32_t> _preserves;
      vk::AttachmentReference _stencil;

      Subpass(
        const std::vector<vk::AttachmentReference> &inputAttachments,
        const std::vector<vk::AttachmentReference> &colorAttachments,
        const std::vector<vk::AttachmentReference> &resolveAttachments,
        const std::vector<uint32_t> &preserveAttachments,
        const vk::AttachmentReference &depthStencil
      );
      operator vk::SubpassDescription() const;
    };

    std::map<std::string,uint32_t> id_mapping;
    std::vector<vk::AttachmentDescription> attachments;
    std::vector<Subpass> subpasses;
    std::vector<vk::SubpassDependency> spDepends;

  public:

    ///@brief Attachment Operation Configuration structure
    ///
    ///@details Utility structure intended to simplify configuration of attachments.
    struct AttachmentOperationConfiguration{
      bool loadEnabled;					///<Load operations enabled?
      bool loadPreserve;				///<Preserve existing attachment contents?
      bool writeEnabled;				///<Write operations enabled?
    };

    ///@brief Add attachment to render pass
    ///
    ///@details Creates a new attachment associated with this render pass, which
    ///					may be later referenced when designing the pipeline which uses
    ///					this render pass.
    ///
    ///@param[in] id Human-readable identifier for this attachment
    ///
    ///@param[in] attachmentFormat Vulkan data format used by this attachment
    ///
    ///@param[in] sampleCount Number of samples used in attachment
    ///
    ///@param[in] colorConfig Configuration for color operations
    ///
    ///@param[in] stencilConfig Configuration for color operations
    ///
    ///@param[in] initialLayout Initial image layout of attachment at start of
    ///						render pass
    ///
    ///@param[in] finalLayout Final image layout of attachment at end of render
    ///						pass
    void AddAttachment(
      std::string id,
      vk::Format attachmentFormat,
      uint8_t sampleCount,
      AttachmentOperationConfiguration colorConfig,
      AttachmentOperationConfiguration stencilConfig,
      vk::ImageLayout initialLayout,
      vk::ImageLayout finalLayout
    );

    ///@brief Add subpass definition to render pass
    ///
    ///@details Creates a subpass, using the specified attachments, in the same
    ///         order as specified, to define how data proceeds through the subpass.
    ///
    ///@param[in] inputAttachments Input-type attachments to be used as part of
    ///						the fragment shader input pipeline.
    ///
    ///@param[in] colorAttachments Colour-type attachments to be used as part of
    ///						the shader output pipeline.
    ///
    ///@param[in] resolveAttachments Colour-type attachments to be used in
    ///						multisample resolve operations for the corresponding color
    ///						attachment.
    ///
    ///@param[in] preserveAttachments Attachments which are not used in this
    ///						subpass, but whose contents must be preserved for later subpasses
    ///
    ///@param[in] hasDepthStencil Whether this subpass uses a depth stencil
    ///
    ///@param[in] depthStencilID Identifier of the depth stencil attachment.
    ///
    ///@attention The order of attachments is important, as it defines how data
    ///						progresses through the pipeline.
    uint32_t AddSubpass(
      const std::vector<std::string> &inputAttachments,
      const std::vector<std::string> &colorAttachments,
      const std::vector<std::string> &resolveAttachments,
      const std::vector<std::string> &preserveAttachments,
      bool hasDepthStencil,
      const std::string depthStencilID
    );

    ///@brief Add subpass dependency to render pass
    ///
    ///@details Creates a subpass execution dependency declaration. This allows
    ///					for control over the ordered execution of subpasses in the render
    ///					pass.
    ///
    ///					Dependencies can be more easily visualized in this case as
    ///					injecting an intermediate all-preserve subpass between the subpasses
    ///					in question. Execution is synchronized such that the "source"
    ///					subpass executes prior to the "destination" subpass. Memory access
    ///					is limited in that all writes during the "source" subpass are
    ///					made available and visible before the "destination" subpass
    ///					attempts to either read or write the same locations.
    ///
    ///					The purpose of dependencies is to avoid data hazards. They are
    ///					not mandatory, but are highly recommended in more complex pipelines.
    ///
    ///@param[in] sourceSubpass Subpass index which should precede this dependency
    ///						for synchronization purposes. -1 indicates no leading synchronization.
    ///
    ///@param[in] destinationSubpass Subpass index which should proceed this
    ///						subpass for synchronization purposes. -1 indicates no following
    ///						synchronization.
    ///
    ///@param[in] sourceStageMask Stages during which memory access is permitted.
    ///
    ///@param[in] destinationStageMask Stages during which memory access is
    ///						permitted.
    ///
    ///@param[in] sourceAccess Types of memory access which are permitted during
    ///						the specified source stages
    ///
    ///@param[in] destinationAccess Types of memory access which are permitted
    ///						during the specified destination stages
    ///
    ///@param[in] dependencyFlags Flags used by this dependency.
    ///
    ///@note sourceSubpass must be either earlier or identical to destinationSubpass,
    ///				to avoid possible cyclic dependencies. If the subpasses are identical,
    ///				a self-dependency is defined, which only constrains pipeline barriers
    ///				within the subpass.
    ///
    ///@todo Simplify interface, rather than pure parameter forwarding.
    void AddSubpassDependency(
      uint32_t sourceSubpass,
      uint32_t destinationSubpass,
      vk::PipelineStageFlags sourceStageMask,
      vk::PipelineStageFlags destinationStageMask,
      vk::AccessFlags sourceAccess,
      vk::AccessFlags destinationAccess,
      vk::DependencyFlags dependencyFlags
    );

    ///@brief Create render pass
    ///
    ///@details Generate the render pass, using the currently-defined sets of
    ///					subpasses and subpass dependencies.
    ///
    ///@param[in] device Logical device which will create this render pass.
    ///
    ///@returns std::shared_ptr<RenderPassMeta> instance, which contains a
    ///         fully-constructed render pass instance, as well as metadata used
    ///         for consumers of the render pass.
    ///
    ///@note The device used in the render pass must also be used to create any
    ///				pipeline which utilizes the render pass.
    std::shared_ptr<RenderPassMeta> Generate(
      Device &device
    );

    ///@brief Reset this generator
    ///
    ///@details Reset the generator by clearing out the list of attachments,
    ///					subpasses, and dependencies.
    void Reset();
  };
}
#endif
