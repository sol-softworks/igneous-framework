// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Managed Texture Infrastructure
//
//	Description: Infrastructure for Igneous Framework Textures that manage an
//         internally-created texture resource.

#ifndef IGNEOUS_TEXTURE_MANAGED
#define IGNEOUS_TEXTURE_MANAGED

#include "baseTexture.h"

namespace Igneous{

  ///@brief Texture with internally-managed image
  ///
  ///@details Interface for working with image resources that can be created
  ///         dynamically.
  class ManagedTexture final : public BaseTexture{
  private:
    Device::mem_ptr _imgMem;    ///<Device memory allocated for this texture

  public:

    ManagedTexture(Device &dev, vk::ImageViewCreateInfo &ivci);

    ~ManagedTexture();

    ///@brief Rebuild backing members
    ///
    ///@details Rebuilds backing texture resources, if recent changes have been
    ///					applied to the texture object. This method is handled automatically
    ///					by the pipeline infrastructure during normal operation, but may
    ///					be invoked manually.
    void _rebuild(vk::ImageCreateInfo &ici);
  };
}

#endif
