// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Push Constant Infrastructure
//
//	Description: Pipeline Push Constant infrastructure for the Igneous Framework.
//         Contains classes used for the management of push constants.

#ifndef IGNEOUS_PUSHCONSTANT
#define IGNEOUS_PUSHCONSTANT

#include <vector>
#include <map>
#include <memory>
#include <optional>

#include "vulkan/vulkan.hpp"

#include "util/dynarray.h"

#include "miniLog.h"

namespace Igneous{
  ///@brief Push Constant Management Class
  ///
  ///@details Manages the creation and configuration of a pipeline push constant
  ///					block. Push constants are used by pipelines to provide small,
  ///					frequently-changing values to a device.
  ///
  ///@attention Push constants are limited in their maximum size. The spec only
  ///						specifies that the block size must be "at least" 128 bytes. Do
  ///						not exceed the maximum size as reported by the PhysicalDeviceLimits
  ///						structure.
  ///
  ///@todo Provide an interface that reports when the requested block size exceeds
  ///				the "minimum" size spec, to ensure compatibility across all devices?
  struct PushConstantMeta{
  private:
    vk::PushConstantRange block;
    vk::PipelineLayout &_layout;
    vk::CommandBuffer _buffer;

  public:

    PushConstantMeta(
      vk::PipelineLayout &layout,
      vk::ShaderStageFlags stages,
      uint32_t offset,
      uint32_t size
    ) : block({stages,offset,size}), _layout(layout), _buffer(nullptr)
    {
      ConsoleLog(LogType::DEBUG,"[IG:PUSH] Offset: %u; Size: %u\n",offset,size);
    }

    virtual ~PushConstantMeta(){}

    ///@brief Write value to push constant block
    ///
    ///@details Assigns the specified value to the push constant block, so that
    ///         device-side accesses may reference the new value.
    ///
    ///@param[in] newValue Value to be assigned to the block.
    ///
    ///@note    This function is normally invoked by PushConstantValue when
    ///         Commit() is run, as PCV provides type guarantees for assigned
    ///         values. Under normal circumstances, there is no need to invoke
    ///         this method directly. It is publically exposed for situations
    ///         that require more specialized behaviour.
    ///
    ///@note    Attempting to execute a write outside of a render task is invalid,
    ///         but will not (directly) result in undefined behaviour, as this
    ///         method ensures there is a valid command buffer before writing.
    virtual void _WriteValue(void* newValue){
      if(this->_buffer){
        this->_buffer.pushConstants(
          this->_layout,
          this->block.stageFlags,
          this->block.offset,
          this->block.size,
          newValue
        );
      }
      else{
        ConsoleLog(LogType::ERR,"[IG:PUSH] Attempting to write to push constant \
block outside of valid task. Ignoring.\n");
      }
    }

    ///@brief Set Command Buffer
    ///
    ///@details Assigns the command buffer used for writing to the push constant
    ///         block. Future writes will be performed using this command buffer.
    ///
    ///@param[in] buffer Command buffer which will record value assignments.
    virtual void SetBuffer(vk::CommandBuffer &buffer){
      this->_buffer = buffer;
    }

    ///@brief Clears Command Buffer Assignment
    ///
    ///@details Removes the assigned command buffer, preventing further value
    ///         changes. Future value write attempts will be ignored until a new
    ///         buffer is assigned.
    virtual void ClearBuffer(){
      this->_buffer = nullptr;
    }

    ///@brief Get size of block
    ///
    ///@details Retrieves the size, in bytes, of the push constant block.
    ///
    ///@returns Size of the block, in bytes.
    virtual uint32_t size() const{
      return this->block.size;
    }
  };

  ///@brief Push Constant Block Interface
  ///
  ///@details Class template designed to provide type-safety guarantees to push
  ///         constant data assignment operations. Writes are internally buffered
  ///         before being assigned to the push constant block, to reduce data
  ///         transfers in complex situations.
  ///
  ///@tparam storedType Host-side type of the managed push constant. Both primitive
  ///         and user-defined types may be used.
  template<typename storedType>
  struct PushConstantValue final : public PushConstantMeta{
  private:
    storedType value;

  public:

    template<typename... TypeArgs>
    PushConstantValue(
      vk::PipelineLayout &layout,
      vk::ShaderStageFlags stages,
      uint32_t offset,
      uint32_t blockSize,
      TypeArgs... valArgs
    ) :
      PushConstantMeta(layout,stages,offset,blockSize),
      value(valArgs...)
    {

    }

    ///@brief Update Block Value
    ///
    ///@details Commits the stored value to the push constant block, making it
    ///         visible to device-side kernels.
    void Commit(){
      if constexpr(std::is_arithmetic_v<storedType>){
        this->_WriteValue(&this->value);
      }
      else{
        this->_WriteValue(this->value.data());
      }
    }

    ///@name Assignment Operators
    ///@{
    PushConstantValue& operator=(const storedType &rhs){
      this->value = rhs;
      return *this;
    }

    PushConstantValue& operator=(const PushConstantValue &rhs){
      this->value = rhs.value;
      return *this;
    }
    ///@}

    ///@name Value Retrieval Operators
    ///@{
    storedType get() const{
      return this->value;
    }

    operator storedType() const{
      return this->value;
    }
    ///@}
  };

  ///@brief Push Constant Block Interface with Reference Semantics
  ///
  ///@details Class providing a convenience interface to the functionality of
  ///         PushConstantValue. Utilizes reference semantics to make it possible
  ///         to have multiple users access the same push constant block.
  ///
  ///@tparam storedType Host-side type of the managed push constant. Both primitive
  ///         and user-defined types may be used.
  template<typename storedType>
  struct PushConstantReference{
  private:
    PushConstantValue<storedType> &_value;

  public:
    PushConstantReference(
      PushConstantValue<storedType> &value
    ) : _value(value){

    }

    PushConstantReference() : _value(nullptr){}


    void Commit(){
      this->_value.Commit();
    }

    ///@name Assignment Operators
    ///@{
    PushConstantReference& operator=(const storedType &rhs){
      this->_value = rhs;
      return *this;
    }

    PushConstantReference& operator=(const PushConstantReference &rhs){
      this->_value = rhs;
      return *this;
    }
    ///@}

    ///@name Value Retrieval Operators
    ///@{
    operator storedType() const{
      return this->_value;
    }

    storedType get() const{
      return this->_value;
    }
    ///@}
  };

  ///@brief Push Constant Layout Metadata
  ///
  ///@details Metadata structure centralizing the pipeline push constant blocks.
  ///         Provides the means to add additional blocks during pipeline creation,
  ///         and ensure block modifiers have consistent behaviour when created.
  struct PushConstantLayout{

  private:
    //PCL is specific to each pipeline, so having a reference to the owning
    //		layout should reduce "parameter fatigue"
    vk::PipelineLayout &_layout;
    vk::CommandBuffer _buffer;

  public:

    PushConstantLayout(
      vk::PipelineLayout &layout
    ) : _layout(layout){}

    ///@brief Ranges added to the block
    ///
    ///@details All value ranges added to the push constant block. Managed by the
    ///					PushConstantLayout::AddRange method.
    ///
    ///@attention Do not modify this struct directly. Use the AddRange method,
    ///						as it will ensure the ranges are properly added to the block.
    std::map<std::string,std::unique_ptr<PushConstantMeta>, std::less<>> blocks;

    //eVertex, eTessellationControl, eTesselationEvaluation,
    //	eGeometry, eFragment, eCompute, eAllGraphics, eAll
    ///@brief Add Push Constant Range (Array)
    ///
    ///@details Adds an arrayed range (sized-type) to the push constant layout.
    ///
    ///@tparam blockType Type that will store the data to be modified.
    ///
    ///@param[in] name Name used to identify this range
    ///
    ///@param[in] stage Stage for which this range applies.
    ///
    ///@param[in] offset Offset of this range in the layout
    ///
    ///@param[in] count Number of blockType elements in the array
    ///
    ///@note blockType is only used to determine the appropriate sizing for the
    ///				range. Any type of identical size may be used to update the element,
    ///				but it's better to use the same type for updates.
    ///
    ///@note stage can consist of multiple stages.
    template<typename blockType>
    void AddArrayRange(
      std::string name,
      vk::ShaderStageFlags stages,
      uint32_t offset,
      size_t count
    ){
      ConsoleLog(LogType::DEBUG,"[VK] Adding Array Range %s\n",name.c_str());
      ConsoleLog(LogType::DEBUG,"[VK]   - offset: %i\n",offset);
      ConsoleLog(LogType::DEBUG,"[IG:PC] - Adding %s array Push Constant block\n",typeid(blockType).name());
      this->blocks.try_emplace(
        name,
        std::make_unique<PushConstantValue<util::dynarray<blockType>>>(
          this->_layout,
          stages,
          offset,
          sizeof(blockType)*count,
          count
        )
      );
    }

    ///@brief Add Push Constant Range (Single value)
    ///
    ///@details Adds a single-value range (sized-type) to the push constant layout.
    ///
    ///@tparam blockType Type that will store the data to be modified.
    ///
    ///@param[in] name Name used to identify this range
    ///
    ///@param[in] stage Stage for which this range applies.
    ///
    ///@param[in] offset Offset of this range in the layout
    ///
    ///@note blockType is only used to determine the appropriate sizing for the
    ///				range. Any type of identical size may be used to update the element,
    ///				but it's better to use the same type for updates.
    ///
    ///@note stage can consist of multiple stages.
    template<typename blockType>
    void AddRange(
      std::string name,
      vk::ShaderStageFlags stage,
      uint32_t offset
    ){
      ConsoleLog(LogType::DEBUG,"[IG:PC] - Adding %s Push Constant block\n",typeid(blockType).name());
      this->blocks.try_emplace(
        name,
        std::make_unique<PushConstantValue<blockType>>(
          this->_layout,
          stage,
          offset,
          sizeof(blockType)
        )
      );
    }

    ///@brief Assign Command Buffer
    ///
    ///@details Assigns the command buffer used for modifying push constant
    ///         values. The provided buffer will be used by all blocks when
    ///         committing new values.
    ///
    ///@param[in] buffer Command buffer used to record push constant value changes.
    void SetCommandBuffer(vk::CommandBuffer &buffer){
      this->_buffer = buffer;
      for(auto &block : this->blocks){
        block.second->SetBuffer(this->_buffer);
      }
    }

    ///@brief Clears Command Buffer Assignment
    ///
    ///@details Removes the assigned command buffer, preventing further value
    ///         changes to the managed blocks.
    void ClearCommandBuffer(){
      this->_buffer = nullptr;
      for(auto &block : this->blocks){
        block.second->ClearBuffer();
      }
    }

    ///@brief Get Push Constant Block Accessor
    ///
    ///@details Generates a PushConstantReference entity that enables manipulation
    ///         of push constant block values.
    ///
    ///@tparam storedType Host-side type of the managed push constant. Both primitive
    ///         and user-defined types may be used.
    ///
    ///@param[in] pc_id Block identifier
    ///
    ///@warning The specified type must be the same as that stored in the block.
    ///         A discrepancy between the two types will result in an invalid
    ///         return value, which will manifest as a std::bad_optional_access
    ///         exception.
    ///
    ///@todo    Either find a way to simplify retrieving a push constant reference,
    ///         or raise a more informative exception.
    template<typename storedType>
    std::optional<PushConstantReference<storedType>> GetValue(
      std::string_view pc_id
    ){
      auto blockIter = this->blocks.find(pc_id);
      if(blockIter != this->blocks.end()){
        auto block = dynamic_cast<PushConstantValue<storedType>*>(blockIter->second.get());
        if(block){
          if(this->_buffer) block->SetBuffer(this->_buffer);
          return PushConstantReference<storedType>(*block);
        }
        else{
          //Wrong type specified. It doesn't make sense to try and work with
          //		size-compatible conversions in this case, so just report an error.
          ConsoleLog(LogType::ERR,"[VK] Attempting to read push constant %s value \
  with size-incompatible type.\n",
          pc_id.data()
          );
        }
      }
      return {};
    }
  };
}

#endif
