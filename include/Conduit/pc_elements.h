// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Push Constant Infrastructure
//
//	Description: Pipeline Push Constant element definitions for Conduit.
//         Contains classes used by Conduit to categorize push constant block
//         elements into generalized structures.

#ifndef IGNEOUS_CONDUIT_PCTYPES
#define IGNEOUS_CONDUIT_PCTYPES

#include "glm/detail/qualifier.hpp"

#include "util/dynarray.h"

#include "Conduit/pushconstant.h"

namespace Igneous{

  ///@brief Alias template for Scalar elements
  template<typename primType>
  using PushConstantScalar = PushConstantReference<primType>;

  ///@brief Alias template for Array elements
  template<typename primType>
  using PushConstantArray = PushConstantReference<util::dynarray<primType>>;

  ///@brief Alias template for 2-element vectors
  template<typename primType>
  using PushConstantVec2 = PushConstantReference<glm::tvec2<primType>>;

  ///@brief Alias template for 3-element vectors
  template<typename primType>
  using PushConstantVec3 = PushConstantReference<glm::tvec3<primType>>;

  ///@brief Alias template for 4-element vectors
  template<typename primType>
  using PushConstantVec4 = PushConstantReference<glm::tvec4<primType>>;

  ///@brief Alias template for NxM matrices
  template<size_t N, size_t M, typename primType>
  using PushConstantMatNxM = PushConstantReference<glm::mat<N,M,primType>>;
}

#endif
