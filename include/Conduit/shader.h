// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Shader Infrastructure
//
//	Description: Shader infrastructure for the Igneous Framework. Contains
//				 classes used for shader source reflection and metadata storage.

#ifndef IGNEOUS_SHADER
#define IGNEOUS_SHADER

#include <vector>
#include <map>
#include <set>
#include <filesystem>

#include "spirv_common.hpp"

#include "Conduit/pushconstant.h"

#include "vulkan/vulkan.hpp"

namespace Igneous{

  ///@brief Shader metadata struct
  ///
  ///@details Class providing a list of all relevant information that is
  ///					available for a particular shader.
  ///
  ///@todo GLSL shaders can (technically) have multiple entry points and stages
  ///				in a single shader source. Entry points are already enabled; it's
  ///				the "combined vert/frag" shader compilation that's introducing
  ///				problems. Not really an Igneous problem: just a "why won't
  ///				glslangValidator compile this" problem. It might already work, but
  ///				it hasn't been tested.
  ///
  ///@todo Further integration with SPIRV-Cross is possible. There are several
  ///       other shader elements that are not extracted that can further enhance
  ///       Conduit's generation capabilities.
  struct Shader{

    ///@brief Push Constant Range metadata
    struct _PCBlockDef{
      spirv_cross::SPIRType type;   ///<Block base type (array element type)
      uint64_t size;                ///<Total block size
      uint64_t count;               ///<Number of elements in block (<1: arrayed)
      uint64_t offset;              ///<Offset of block
      vk::ShaderStageFlags stages;  ///<Stages which reference this block

      vk::PushConstantRange Define();
    };

    ///@brief Descriptor Set Binding metadata
    struct SetBinding{
      uint32_t layoutIndex;
      uint32_t bindingIndex;
      std::string name;
      vk::DescriptorType type;
      uint32_t dimensionality;
      vk::ShaderStageFlags stages;

      SetBinding& operator|=(const vk::ShaderStageFlagBits &stage){
        this->stages |= stage;
        return *this;
      }

      SetBinding& operator|=(const vk::ShaderStageFlags &stages){
        this->stages |= stages;
        return *this;
      }
    };

    ///@brief SetBinding comparison function object
    struct BindingComp{

      using is_transparent = void;

      bool operator()(SetBinding const &lhs, SetBinding const &rhs) const{
        return lhs.layoutIndex < rhs.layoutIndex ||
              (lhs.layoutIndex == rhs.layoutIndex && lhs.bindingIndex < rhs.bindingIndex);
      }

      bool operator()(std::pair<uint32_t,uint32_t> const &lhs, SetBinding const &rhs) const{
        return lhs.first < rhs.layoutIndex ||
              (lhs.first == rhs.layoutIndex && lhs.second < rhs.bindingIndex);
      }

      bool operator()(SetBinding const &lhs, std::pair<uint32_t,uint32_t> const &rhs) const{
        return lhs.layoutIndex < rhs.first ||
              (lhs.layoutIndex == rhs.first && lhs.bindingIndex < rhs.second);
      }
    };

    std::set<SetBinding,BindingComp> shaderBindings;            ///<Binding definitions from all stages
    std::map<std::string,vk::ShaderStageFlagBits> entry_points; ///<Entry points: Name and stage
    std::map<std::string,_PCBlockDef> pc_blocks;                ///<Push Constant block definitions from all stages

    std::vector<uint32_t> shader;			///<SPIR-V bytecode

    Shader(
      std::vector<uint32_t> &shaderCode
    );

    ~Shader();

    ///@brief Add push constant definitions to external map
    ///
    ///@details Provides the provided external map with the set of push constant
    ///         available within this shader. If a block already exists in the
    ///         map, it's definition is updated to include the stages referenced
    ///         in this shader.
    ///
    ///@param[in] pc_defs External map of push constant definitions to be updated
    ///           with the available blocks
    void AddPushConstants(std::map<std::string,_PCBlockDef> &pc_defs);

    ///@brief Add binding definitions to external map
    ///
    ///@details Provides the provided external map with the set of descriptor set
    ///         bindings available within this shader. If a binding already
    ///         exists in the map, it's definition is updated to include the
    ///         stages referenced in this shader.
    ///
    ///@param[in] bind_defs External map of descriptor set binding definitions
    ///           to be updated with the available bindings
    void AddBindingDefs(std::set<Shader::SetBinding,Shader::BindingComp> &bind_defs);

    ///@brief Build shader module
    ///
    ///@details Builds this shader module for use with the provided logical device.
    ///
    ///@param[in] device Logical device which will create the module instance
    vk::ShaderModule Build(vk::Device device);

    ///@brief Destroy shader module instance
    ///
    ///@details Destroys a shader module instance that was created previously on
    ///         the specified device
    ///
    ///@param[in] device Logical device which created the module instance
    ///
    ///@param[in] module Shader module instance to be destroyed
    ///
    ///@attention The provided shader module MUST have been created by the
    ///           provided logical device. Attempting to destroy a module with a
    ///           different logical device will result in undefined behaviour.
    static void Destroy(vk::Device device, vk::ShaderModule module);

    ///@brief Add shared shader
    ///
    ///@details Adds a shader to the internal list of known shaders. The shader
    ///					may then be referenced by using the supplied name when configuring
    ///					a pipeline.
    ///
    ///@param[in] shaderPath Path to the shader source.
    ///
    ///@param[in] (optional) name Name to assign to this shader
    ///
    ///@note If a custom name is not provided, this method generates one for the
    ///       shader by taking only the base filename without a .spv extension (stem).
    ///       (e.g. a file named "shader.vert.spv" becomes "shader.vert", while
    ///       a file named "shader.vert" remains the same)
    ///
    static void Define(
      std::filesystem::path shaderPath,
      std::string name=""
    );
  };

  ///@brief Program-wide list of known shaders
  ///
  ///@details Map containing all shaders that are accessible to pipelines, with
  ///					a human-readable identifier used to differentiate between shaders.\n
  ///         \n
  ///					The reason for a globally-accessible list is to reduce the amount
  ///					of duplication when shaders are shared between multiple pipelines.
  ///					Instead of each pipeline having a copy of the bytecode for itself,
  ///					the shader is compiled into a pipeline-agnostic module usable by
  ///					any pipeline.
  extern std::map<std::string,Shader> sharedShaders;
}

#endif
