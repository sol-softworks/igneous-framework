// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Descriptor Set Metadata Infrastructure
//
//	Description: Descriptor Set Metadata infrastructure for the Igneous Framework.
//         Stores relevant descriptor set information stored in SPIRV source, to
//         facilitate intelligent creation of related descriptor set instances.
//
//         This system has been extended to provide metadata required by the
//         Igneous Material System (Obsidian) to easily generate Materials based
//         on the properties used in a GraphicsPipeline

#ifndef IGNEOUS_DSMETA
#define IGNEOUS_DSMETA

#include <vector>
#include <map>

#include "vulkan/vulkan.hpp"

#include "resource.h"
#include "Obsidian/bindings.h"

namespace Igneous{

  struct DescriptorSetMeta;
  class Material;


  struct DescriptorLayoutMeta{
  public:
    //Internal typedefs, to clarify the intent of map elements
    using BindingCount = uint32_t;
    using BindingSID = std::string;
    using BindingNID = uint32_t;
    using bindID = std::pair<BindingNID,BindingSID>;

  private:
    Device &_dev;
    vk::DescriptorSetLayout layout;
    std::set<std::shared_ptr<DescriptorSetMeta>> _instances;

    std::map<bindID,vk::DescriptorSetLayoutBinding> layout_bindings;

    vk::DescriptorSet _AllocateDS();

  public:

    DescriptorLayoutMeta(Device &dev);
    ~DescriptorLayoutMeta();

    //Enable new binding on existing descriptor set
    [[deprecated("Use the more featureful template-based implementation")]]
    void AddLayoutBinding(std::string bindID, std::tuple<vk::DescriptorType,vk::ShaderStageFlagBits,uint32_t> binding);

    template<typename bindType, typename = std::enable_if_t<std::is_base_of<BaseBinding,bindType>::value>>
    void DefineBinding(bindID bindKey, vk::ShaderStageFlags stages, BindingCount count){

      this->layout_bindings.try_emplace(
        bindKey,
        vk::DescriptorSetLayoutBinding{
          bindKey.first,
          bindType::Type,
          count,
          stages,
          nullptr
        }
      );
    }

    void CompileLayout();

    std::weak_ptr<DescriptorSetMeta> CreateSet();
    void DestroySet(std::weak_ptr<DescriptorSetMeta> oldSet);

    operator vk::DescriptorSetLayout() const;
  };

  struct DescriptorSetMeta{
  private:
    Device &_dev;

    std::vector<vk::WriteDescriptorSet> _updates;
    vk::DescriptorSet ds;

    std::map<std::string,std::shared_ptr<BaseBinding>> bindings;

  public:

    ///@todo DSM entity resources should be managed by the associated DSL.
    ///       DSMDeleter handles DS release, but DS creation still needs to be
    ///       handled.
    ///       (The reason for this complexity is to remove _dev from DSM)
    /*
    struct DSMDeleter{
      Device &_dev;
      void operator()(DescriptorSetMeta* dsm) const{
        this->_dev.FreeDescriptors(dsm->ds);
      }
    };
    */

    BindUpdates _bindUpdater;

    DescriptorSetMeta(Device &dev, DescriptorLayoutMeta &dsl);
    ~DescriptorSetMeta();

    template<typename bindType, typename = std::enable_if_t<std::is_base_of<BaseBinding,bindType>::value>>
    void AddBinding(std::string bindIdentifier, vk::DescriptorSetLayoutBinding &bindDef){
      auto newBind = this->bindings.try_emplace(
        bindIdentifier,
        std::make_shared<bindType>(
          bindDef,
          this->_bindUpdater
        )
      );

      //Here's a hint that something's not right here...
      if(auto cast = dynamic_cast<ImageBindingData*>(newBind.first->second.get())){
        cast->_dev = &this->_dev;
      }
      if(auto cast = dynamic_cast<BufferBindingData*>(newBind.first->second.get())){
        cast->_dev = &this->_dev;
      }
    }

    //(Default) BaseBinding pseudo-overload.
    void AddBinding(std::string bindIdentifier, vk::DescriptorSetLayoutBinding &bindDef){
      this->bindings.try_emplace(
        bindIdentifier,
        std::make_shared<BaseBinding>(
          bindDef
        )
      );
    }

    void Update();

    operator vk::DescriptorSet() const;

    //Temporary friends
    friend class Material;
    friend class DSMDeleter;
  };
}

#endif
