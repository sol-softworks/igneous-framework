// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Sampler Metadata
//
//	Description: Igneous Framework Sampler metadata structure, used to provide
//        a simplified interface to the configuration of Vulkan sampler objects.

#ifndef IGNEOUS_SAMPLER
#define IGNEOUS_SAMPLER

#include <map>

#include "vulkan/vulkan.hpp"
#include "resource.h"

namespace Igneous{

  ///@brief Sampler metadata class
  ///
  ///@details Class providing access to the sampler configuration process through
  ///					a series of generalized mutators and utility structs.
  ///
  ///					Samplers are used to define how texels are accessed and retrieved,
  ///					and are independent of textures -- a sampler may be used by
  ///					multiple textures simultaneously. SamplerMeta maintains the
  ///					configuration for a sampler, so that it may be later modified.
  struct SamplerMeta{

    ///@brief Texture Addressing Configuration
    ///
    ///@details Utility struct defining how to handle situations where a texel
    ///					coordinate beyond the valid range is accessed. Each component
    ///					can be either Tiled, Mirrored, or Clamped.
    ///
    ///@todo Vulkan has a few addressing modes not used by Igneous, yet.
    ///				ClampToBorder and MirrorClampToEdge should be permitted, but
    ///				finding a good way to abstract the internal enums is difficult.
    struct TilingConfig{
      vk::SamplerAddressMode u,v,w;

      //FFS, ANY other way
      static TilingConfig TiledUV();
      static TilingConfig TiledUVW();
      static TilingConfig MirroredUV();
      static TilingConfig MirroredUVW();
      static TilingConfig ClampedUV();
      static TilingConfig ClampedUVW();
    };

    ///@brief Texture anisotropic sampling configuration
    ///
    ///@details Utility struct defining whether anisotropic sampling should be
    ///					used for this sampler, and to what extent.
    struct AnisotropyConfig{
      vk::Bool32 enable;
      float max;
    };

    ///@brief Texture mag/minification filter configuration
    ///
    ///@details Utility struct defining the filters to apply on magnification and
    ///					minification operations when sampling textures.
    struct TexelFilter{
      vk::Filter magnify,minify;

      //FFS, ANY other way
      static TexelFilter Nearest();
      static TexelFilter Linear();
      static TexelFilter CubicIMG();
    };

    ///@brief LoD Configuration
    ///
    ///@details Utility struct specifying the minimum and maximum levels used
    ///					in the Level-of-Detail parameterization for sampling operations.
    struct LODConfig{
      float min,max;
    };

  private:
    TilingConfig tiling;
    AnisotropyConfig anisotropy;
    TexelFilter filters;
    LODConfig lod;

    vk::SamplerCreateInfo _sci;

    ///@brief Data modification bitfield
		///
		///@details Bitfield representing whether image resource creation members have
		///         been modified since the last resource compilation. Implicitly
		///         convertible to bool, to simplify usage.
		///
		///@internal This data structure implements a more intelligent form of "dirty
		///           flag" state monitoring, where modifications are tracked at the
		///           per-field level.
		///           Explicitly occupies a single byte, whereas a bool's size is
		///           implementation defined, and may be larger.
		struct Modified{
			uint8_t tiling : 1;
			uint8_t anisotropy : 1;
			uint8_t filters : 1;
			uint8_t lod : 1;

			operator bool() const{
				return
					this->tiling |
					this->anisotropy |
					this->filters |
					this->lod;
			}
			void reset(){
        this->tiling = 0;
        this->anisotropy = 0;
        this->filters = 0;
        this->lod = 0;
			}
		};

		Modified _dirty;				///<Does this texture need to be rebuilt?

    std::map<std::reference_wrapper<Device>,std::shared_ptr<vk::Sampler>,std::less<Device>> _instances;

    void _rebuild();

  public:

    SamplerMeta(TilingConfig tilecfg, AnisotropyConfig anisocfg, TexelFilter texcfg, LODConfig lodcfg);
    ~SamplerMeta();

    std::shared_ptr<vk::Sampler> GetSampler(Device& owner);
    void SetTiling(TilingConfig newTiling);
    void SetAnisotropy(AnisotropyConfig newAniso);
    void SetLOD(LODConfig newLOD);
  };
}

#endif
