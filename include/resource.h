// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Device Resources
//
//	Description: Vulkan Physical / Logical Device abstraction layer.

#ifndef IGNEOUS_RESOURCE
#define IGNEOUS_RESOURCE

#include <array>
#include <vector>

//Holy shit, it's a dinosaur
#include <forward_list>
#include <set>
//Some day, when I'm less lazy...
//#include <unordered_set>

#include "vulkan/vulkan.hpp"

//VERY TEMPORARY
#include "miniLog.h"

///@brief Igneous Framework namespace
///@todo Create internal (impl) namespace for framework-managed data
namespace Igneous{

	class Device;
	class PD;

	extern vk::AllocationCallbacks *allocators;

	///@brief Vulkan Resource Deleter
	///
	///@details Unified deletion struct for low-level Vulkan resources managed by
	///					std::shared_ptr instances. This struct provides templatized call
	///					operators for Vulkan resource destruction, using a single reference
	///					to the logical device used to create the resources.\n
	///
	///@note This struct is created automatically by Device instances, as only one
	///				instance needs to exist per Device. It is an internal implementation
	///				detail.
	struct ResourceManager{
		vk::Device &dev;

		ResourceManager(vk::Device &dev) : dev(dev){}

		///@brief Resource Destruction Call Operator
		///
		///@details Call operator function template that destroys the provided
		///					resource. Only valid for types that have an overload for the
		///					vk::Device::destroy() method.
		///
		///@tparam resType Type of Vulkan resource being destroyed
		///
		///@param[in] res Pointer to resource being destroyed.
		template<class resType>
		void operator()(resType *res){
			this->dev.destroy(*res,Igneous::allocators);
			delete res;
		}

		///@brief Memory Free Call Operator
		///
		///@details Call operator for freeing device memory.
		///
		///@param[in] res Pointer to memory to be freed.
		void operator()(vk::DeviceMemory* res){
			this->dev.freeMemory(*res);
			delete res;
		}
	};

	//TODO: Relocate into separate translation unit
	void InitAPI(std::vector<const char*> vkExtensionList);
	void ReleaseAPI();
	PD& GetPhysicalDevice(size_t index);
	vk::PipelineCache GetPipelineCache(vk::Device const &dev);

	///@brief Logical Device Interface
	///
	///@details Class designed to simplify the interface between logical device
	///					management and interaction. Most operations in Vulkan, and
	///					consequently Igneous, require a logical device instance. This class
	///					aggregates common procedures into a simplified interface, while
	///					still providing control over specific requirements for a particular
	///					sequence.
	class Device{

		static const uint8_t comLimit = 2;

		///@brief Reference to physical device which created this logical device
		vk::PhysicalDevice &phys;

		///@brief Vulkan Logical Device
		vk::Device &logicalDevice;

		///@brief Semaphores owned and utilized for operations performed by this device
		std::array<vk::Semaphore,Device::comLimit> semaphore;

		//Device Descriptor Management

		///@brief Descripor pool used by this device
		vk::DescriptorPool descPool;

		//Internal Task Queue Management Submodule
		///@brief Igneous Task Queue Manager
		///
		///@details Manages the device command queue through sequences of Task instances.
		class TaskQueue{
		private:

			///@brief Internal command pool from which tasks are allocated
			vk::CommandPool _pool;

			///@brief Device queue that processes tasks
			vk::Queue queue;

			///@brief Synchronization fence
			vk::Fence _fence;

			///@brief Back-reference to device which owns this queue (needed for
			///				creating new Tasks / cleanup operations)
			vk::Device &_d;

			class Task{
			private:
				//TODO: Enable multiple buffers per task?
				///@brief Internal command buffer
				///
				///@details Vulkan Command Buffer used to record commands for later
				///					execution. All processes in Vulkan require a
				vk::CommandBuffer buffer;

				///@brief Task State
				///
				///@details Possible states:
				///	0: Available (Not in use)
				///	1: In Use (Modified)
				///	2: Finalized (Pre-submit)
				///	3: Submitted
				///	4: Finished (Requires reset)
				uint8_t state;

			public:

				Task(vk::CommandBuffer buf);
				~Task();

				///@brief Mark task as ready for execution
				///
				///@details When a process is finished adding operations to a task, this
				///					method finalizes the Task, and prepares it for eventual
				///					execution within the TaskQueue. The Task is not executed
				///					until the SubmitTasks method is invoked on the owning TaskQueue.
				void Submit();

				operator vk::CommandBuffer() const;
				operator VkCommandBuffer() const;

				friend class TaskQueue;
			};

			///@brief List of tasks associated with this queue, regardless of state.
			std::vector<std::shared_ptr<Task>> tasks;

			//TODO: Store smart pointers in Device, to explicitly destroy
			void _destroy();

		public:

			typedef std::weak_ptr<Task> _Task;

			TaskQueue(vk::Device &dev, uint32_t queueFamily, uint32_t queueIndex, uint32_t initBufferCount);
			~TaskQueue();

			//Fence Management
			bool QueueFinished();
			void ResetQueue();

			//Task Management
			_Task StartTask();
			void ResetTasks();
			void SubmitTasks(std::vector<vk::PipelineStageFlags> stageFlags, std::vector<vk::Semaphore> waitSem, std::vector<vk::Semaphore> sigSem);

			//Queue Extensions
			///@todo Task mixins might help avoid collecting role-specific functions
			///				in the general interface.
			void PresentKHR(std::vector<vk::Semaphore> waitSem, vk::SwapchainKHR &chain, uint32_t imageIndex);

			friend class Device;
		};
		//I have a dream, that there will be many TaskQueues
		//std::vector<TaskQueue> taskLists;
		TaskQueue taskList;

		///@brief Memory Allocations performed by this device
		///
		///@details Internal list of allocated device memory instances. Centralized
		///					to ensure all memory allocations are freed when the device is
		///					destroyed.
		std::forward_list<std::unique_ptr<vk::DeviceMemory,ResourceManager>> memoryAllocations;

		//Utility References
		vk::PhysicalDeviceMemoryProperties memProps;		//Memory Properties from parent PD

	public:
		///Device Memory pointer type
		typedef vk::DeviceMemory* mem_ptr;

		ResourceManager _manager;

		Device(vk::PhysicalDevice &pd, vk::Device &dev);
		~Device();

		Device(Device &&b);

		///@name Device Memory Management
		///@{
		mem_ptr malloc(uint32_t size, uint32_t typeIndex);	//Raw-Memory Allocation
		mem_ptr balloc(vk::Buffer &buf, vk::MemoryPropertyFlags props);	//Buffer Allocation
		mem_ptr ialloc(vk::Image &img, vk::MemoryPropertyFlags props);	//Image Allocation

		void free(mem_ptr memory);	//Free memory block
		void memcpy(mem_ptr memory, void* buffer, size_t size);	//Assign buffer data
		void memcpy(void* buffer, mem_ptr memory, size_t size); //Read memory into buffer
		///@}

		///@name Device Descriptor Management
		///@{
		vk::DescriptorSet AddDescriptorSet(vk::DescriptorSetLayout layout);
		void FreeDescriptors(vk::DescriptorSet &oldSet);				//Free specified descriptor(s)
		///@}

		///@name Device Command Management
		///@{
		//<Line intentionally left blank for Doxygen>
		///@brief Begin recording task
		///
		///@details Request a new task for recording commands for future execution.
		TaskQueue::_Task StartTask();

		///@brief Execute queued tasks
		///
		///@details Execute all tasks currently queued, using the provided synchronization
		///					parameters
		///@param[in] stageFlags: Vector of vk::PipelineStageFlags specifying the
		///						stage at which the corresponding semaphore wait occurs
		///@param[in] waitSem: Vector of vk::Semaphore wait semaphores that must be
		///						signaled before the queue may continue execution.
		///@param[in] sigSem: Vector of vk::Semaphore signal semaphores that will be
		///						emitted once the queue has finished executing all commands.
		void ExecuteTasks(std::vector<vk::PipelineStageFlags> stageFlags, std::vector<vk::Semaphore> waitSem, std::vector<vk::Semaphore> sigSem);

		///@brief Check current queue execution state
		///
		///@details Verify whether the queue has finished executing all staged commands.
		///
		///@return bool reporting whether queue has finished execution
		bool CheckTaskQueueStatus();

		//Plural, for future development
		//Also, helpful brief doc...
		///@brief Synchronize queues
		///
		///@details Synchronizes queues by blocking execution until all internal
		///					fences have been signaled.
		void SynchronizeQueues();
		///@}

		operator vk::Device() const;
		operator VkDevice() const;

		//Temporary Friends
		friend class WindowSurface;
		friend class Pipeline;
	};

	class PD{
		vk::PhysicalDevice phys;
		vk::QueueFamilyProperties queueProps;
		std::vector<std::unique_ptr<vk::Device>> logicalDevices;

	public:

		//Yes, this should be public.
		vk::PhysicalDeviceFeatures features;		///< Struct defining features available on this physical device (Status / Enable / Disable properties)

		PD(vk::PhysicalDevice &pd);
		~PD();

		PD(PD &&b);

		///@brief Add logical device
		///
		///@details Creates a new logical device, using the specified queue creation
		///					information and device extensions.
		///
		///@param[in] dqciArray Vector of vk::DeviceQueueCreateInfo structs defining
		///						how the device should create it's internal queues.
		///@param[in] devExt Vector of C-style strings containing the Vulkan extensions
		///						required for this logical device.
		///
		///@note dqciArray may be modified in a future revision of Igneous, in favour
		///				of a simplified interface that shifts implementation into TaskQueue.
		std::unique_ptr<Device> AddDevice(std::vector<vk::DeviceQueueCreateInfo> dqciArray, std::vector<const char*> devExt);
	};
}

#endif
