// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Logging Utility
//
//	Description: Logfile / Console output processes.
//
//	Note: More robust (external) implementation still in development. Primarily
//				included to simplify Igneous development.

#ifndef SS_LOGGING
#define SS_LOGGING

#include <string>

enum LogType{
	DEBUG,
	INFO,
	WARN,
	ERR,
	FATAL
};

std::string FormatMsg(LogType type, std::string msg);

void ConsoleLog(LogType type, std::string msg);
void FileLog(LogType type, std::string file, std::string msg);

template<typename ...msgArgs>
void ConsoleLog(LogType type, std::string msg, msgArgs... args){
	auto fmt = FormatMsg(type,msg);
	printf(fmt.c_str(),args...);
}

template<typename ...msgArgs>
void FileLog(LogType type, std::string file, std::string msg, msgArgs... args){
	if(auto out = std::fopen(file.c_str(),"a")){
		auto fmt = FormatMsg(type,msg);
		fprintf(out,fmt,args...);
	}
}

#endif
