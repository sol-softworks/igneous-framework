// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Texture Resources
//
//	Description: Vulkan Texture Implementations (1D,2D,3D implementations)

#ifndef IGNEOUS_TEXTURE
#define IGNEOUS_TEXTURE

//Yep. Just the one.
#include "texture_internal.h"

namespace Igneous{
	//Template Instantiations for Textures
	class Texture1D : public Texture<1>{
	public:
		using Texture<1>::Create;
	};
	
	class Texture2D : public Texture<2>{
	public:
		using Texture<2>::Create;
	};

	class Texture3D : public Texture<3>{
	public:
		using Texture<3>::Create;
	};
}

#endif
