// SPDX-License-Identifier: LGPL-3.0-or-later
//Copyright (C) 2019  Sol Softworks
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published
//		by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//PROJECT: Igneous Framework
//
//	Development Team: Sol Softworks
//
//	Subject: Igneous Framework Property Resource
//
//	Description: Igneous Framework Property data structure types, used to provide
//        a common interface for accessing and storing multi-type members in a
//        single vector. Primarily used by the Material system to generalize
//        shader inputs in a way that subverts the need to create derivative
//        classes.

#ifndef IGNEOUS_PROPERTY
#define IGNEOUS_PROPERTY

#include <memory>
#include <type_traits>

#include "miniLog.h"

class Property{
protected:
  std::string name;

public:
  Property(std::string id) : name(id){}
  virtual ~Property(){}

  void operator()(){}
  void operator()(void*){}
  Property& operator=(const void* val){return *this;}

  ///@brief Type-safe Property Conversion
  ///
  ///@details Performs a type-safe conversion for a derived property. Uses SFINAE
  ///         to guarantee that only valid types are attempted.
  ///
  ///@tparam derivedType Derived property type to attempt to cast this property
  ///         into.
  ///
  ///@warning This method will return a nullptr if the requested downcast fails,
  ///         such as when the actual derived property type is not derivedType.
  ///         Make sure to verify pointer validity before attempting to dereference
  ///         the returned value.
  template<typename derivedType, typename = std::enable_if_t<std::is_base_of<Property,derivedType>::value>>
  derivedType* as(){
    derivedType* ret = nullptr;
    if(dynamic_cast<derivedType*>(this) != nullptr){
      ret = dynamic_cast<derivedType*>(this);
    }
    return ret;
  }

  ///@brief Property Name Accessor
  ///
  ///@details Returns the name of the property.
  ///
  ///@returns std::string Name of property.
  ///
  ///@note This method exists primarily to ensure Property is a polymorphic type,
  ///       but may be useful for property introspection.
  virtual std::string id() const{return this->name;}

  ///@brief Property Name Mutator
  ///
  ///@details Sets the name of the property to the provided string.
  ///
  ///@param[in] newName Name to be assigned to the property.
  ///
  ///@note This method exists primarily to ensure Property is a polymorphic type.
  ///       Whether this particular method is actually useful is up for debate.
  virtual void id(std::string newName){this->name = newName;}
};

template<typename storedType>
class ValuedProperty : public Property{
  storedType value;

  //Verification method: Override to provide value checking on set
  virtual bool _isValid(storedType val) =0;

public:
  //Accessor methods
  storedType& operator()(){return this->value;}
  void operator()(storedType val){this->value = val;}
  ValuedProperty& operator=(const storedType &val){
    if(this->value != val){
      this->value = val;
    }
    return *this;
  }
};

template<typename storedType>
class RangedProperty : public ValuedProperty<storedType>{
  storedType minValue;
  storedType maxValue;

  virtual bool _isValid(storedType val) override{return (minValue <= val && maxValue >= val);}
};

#endif
